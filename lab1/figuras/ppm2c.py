# ppm2c.py - Converte uma imagem ppm com dados em binario para uma váriavel const uint8_t[] de C

def convert_ppm (filename, cabecalho_separado=False):
    ppm_file = open(filename, 'rb')
    split_name = filename.split('.')
    output_file = open(split_name[0] + ".c", 'w')
    
    output_file.write("const uint8_t img_" + split_name[0] +  "[] = {") 

    char = ppm_file.read(3)
    
    while char:   
        #output_file.write("0x")
        avg = (char[0]+char[1]+char[2])//3
        output_file.write(str(avg))
        output_file.write(", ")
        char = ppm_file.read(3)

    output_file.write("};")

    ppm_file.close()
    output_file.close()
