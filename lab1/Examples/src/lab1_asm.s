; Transforma as imagens

#define NADA_PRESSIONADO -1
#define ESQUERDA_PRESSIONADO  0
#define DIREITA_PRESSIONADO 1
#define CIMA_PRESSIONADO 2
#define BAIXO_PRESSIONADO 3
#define CENTRO_PRESSIONADO 4
#define N_INPUTS 5
#define N_CYCLES_TO_PRESS 1

#define VEL_MAX 3
#define DELAY_DELTA 100
#define DELAY_MIN 10

#define SENTIDO_HORIZONTAL 1
#define SENTIDO_VERTICAL 0
#define HORIZ_VERT_MASK 1

#define SENTIDO_POSITIVO 0
#define SENTIDO_NEGATIVO 2
#define POS_NEG_MASK 2

#define HEIGHT_MASK 0x00FF
#define WIDTH_MASK 0xFF00
#define HEIGHT_SHIFT 0
#define WIDTH_SHIFT 8

#define NULL_PTR 0x0000
 
    PUBLIC transforma_imagem
    SECTION .text : CODE (2)
    THUMB

; R0: Ponteiro para posiçao da imagem
; R1: Transformaçao a ser feita
; R2: Tamanho da imagem (largura<<8 + altura)
; R3: Ponteiro para estado da imagem (sentido, veloidade ou inversao dependendo do que deve ser feito)
transforma_imagem:
    push {r4-r7}

    cmp r1, #NADA_PRESSIONADO
    beq movimenta_imagem
    cmp r1, #ESQUERDA_PRESSIONADO
    beq troca_positivo_negativo
    cmp r1, #DIREITA_PRESSIONADO
    beq troca_horizontal_vertical
    cmp r1, #CIMA_PRESSIONADO
    beq acelera
    cmp r1, #BAIXO_PRESSIONADO
    beq desacelera
    cmp r1, #CENTRO_PRESSIONADO
    beq inverte_imagem
    b transforma_imagem_fim

movimenta_imagem:
    ; R3: &Sentido atual
    ldr r4, [r3]
    cmp r4, #SENTIDO_HORIZONTAL + SENTIDO_POSITIVO
    beq move_direita
    cmp r4, #SENTIDO_HORIZONTAL + SENTIDO_NEGATIVO
    beq move_esquerda
    cmp r4, #SENTIDO_VERTICAL + SENTIDO_POSITIVO
    beq move_baixo
    cmp r4, #SENTIDO_VERTICAL + SENTIDO_NEGATIVO
    beq move_cima
    b transforma_imagem_fim

move_direita:
    ldr r7, [r0] ; r7 <= pos_horizontal
    and r2, r2, #WIDTH_MASK
    lsr r2, r2, #WIDTH_SHIFT ; r2 <= largura
    cmp r7, #0
    itte le
    suble r2, #1
    movle r7, r2 // Se ta no canto da a volta
    subgt r7, #1
    str r7, [r0]
    b transforma_imagem_fim
  
move_esquerda:
    ldr r7, [r0] ; r7 <= pos_horizontal
    and r2, r2, #WIDTH_MASK
    lsr r2, r2, #WIDTH_SHIFT ; r2 <= largura
    sub r2, r2, #1
    cmp r7, r2
    ite ge
    movge r7, #0 // Se ta no canto da a volta
    addlt r7, #1
    str r7, [r0]
    b transforma_imagem_fim
  
move_baixo:
    ldr r7, [r0] ; r7 <= pos_vertial
    and r2, r2, #HEIGHT_MASK
    lsr r2, r2, #HEIGHT_SHIFT ; r2 <= altura
    cmp r7, #0
    itte le
    suble r2, #1
    movle r7, r2 // Se ta no canto da a volta
    subgt r7, #1
    str r7, [r0]
    b transforma_imagem_fim
  
move_cima:
    ldr r7, [r0] ; r7 <= pos_vertial
    and r2, r2, #HEIGHT_MASK
    lsr r2, r2, #HEIGHT_SHIFT ; r2 <= altura
    sub r2, r2, #1
    cmp r7, r2
    ite ge
    movge r7, #0 // Se ta no canto da a volta
    addlt r7, #1
    str r7, [r0]
    b transforma_imagem_fim
 
troca_positivo_negativo:
    ldr r4,[r3]
    mov r5, r4
    and r5, r5, #HORIZ_VERT_MASK ; r5 <= horizontal ou vertial
    and r4, r4, #POS_NEG_MASK ; r4 <= positivo ou negativo
    cmp r4, #SENTIDO_POSITIVO
    ite eq
    moveq r4, #SENTIDO_NEGATIVO ; inverte sentido
    movne r4, #SENTIDO_POSITIVO
    orr r4, r4, r5 ; reonstitui sentido
    str r4, [r3]
    b transforma_imagem_fim
    
troca_horizontal_vertical:
    ldr r4,[r3]
    mov r5, r4
    and r5, r5, #HORIZ_VERT_MASK ; r5 <= horizontal ou vertial
    and r4, r4, #POS_NEG_MASK ; r4 <= positivo ou negativo
    cmp r5, #SENTIDO_HORIZONTAL
    ite eq
    moveq r5, #SENTIDO_VERTICAL ; inverte sentido
    movne r5, #SENTIDO_HORIZONTAL
    orr r4, r4, r5 ; reonstitui sentido
    str r4, [r3]
    b transforma_imagem_fim
 
inverte_imagem:
    ldr r4,[r3]
    cmp r4, #0 ; r4 = inversao
    ite eq
    moveq r4, #1
    movne r4, #0
    str r4, [r3]
    b transforma_imagem_fim
  
 desacelera:
    ldr r4,[r3]
    cmp r4, #0 ; r4 = veloidade
    beq transforma_imagem_fim ; Se clip em 0
    sub r4, r4, #1 ; derementa veloidade
    str r4, [r3]
    b transforma_imagem_fim
  
 acelera:
    ldr r4,[r3]
    cmp r4, #VEL_MAX ; r4 = veloidade
    beq transforma_imagem_fim ; Se clip em VEL_MAX
    add r4, r4, #1 ; inrementa veloidade
    str r4, [r3]
    b transforma_imagem_fim
  
  
transforma_imagem_fim:
  pop {r4-r7}
  bx lr
  
  END
