/*============================================================================
 *           LPCXpresso 1343 + Embedded Artists Development Board
 *---------------------------------------------------------------------------*
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *===========================================================================*/
#include "mcu_regs.h"
#include "type.h"
#include "uart.h"
#include "stdio.h"
#include "timer32.h"
#include "gpio.h"
#include "ssp.h"
#include "i2c.h"
#include "oled.h"
#include "light.h"
#include "pca9532.h"
#include "joystick.h"
#include <stdlib.h>

#include "lab1.h"
   
static uint32_t msTicks = 0;

static uint32_t getTicks(void)
{
    return msTicks;
}

int main (void)
{
    SystemCoreClockUpdate();
    GPIOInit();
    init_timer32(0, 10);
    UARTInit(115200);
    UARTSendString((uint8_t*)"OLED - Peripherals\r\n");
    I2CInit( (uint32_t)I2CMASTER, 0 );
    SSPInit();
    oled_init();
    light_init();
    
    /* setup sys Tick. Elapsed time is e.g. needed by temperature sensor */
    SysTick_Config(SystemCoreClock / 1000);
    if ( !(SysTick->CTRL & (1<<SysTick_CTRL_CLKSOURCE_Msk)) )
    {
      /* When external reference clock is used(CLKSOURCE in
      Systick Control and register bit 2 is set to 0), the
      SYSTICKCLKDIV must be a non-zero value and 2.5 times
      faster than the reference clock.
      When core clock, or system AHB clock, is used(CLKSOURCE
      in Systick Control and register bit 2 is set to 1), the
      SYSTICKCLKDIV has no effect to the SYSTICK frequency. See
      more on Systick clock and status register in Cortex-M3
      technical Reference Manual. */
      LPC_SYSCON->SYSTICKCLKDIV = 0x08;
    }
    
    // Example of OLED display
    light_enable();
    light_setRange(LIGHT_RANGE_4000);
    oled_clearScreen(OLED_COLOR_WHITE);


    uint32_t entradas_passadas[N_INPUTS] = {0};
    int32_t entrada_escolhida = NADA_PRESSIONADO;
    uint32_t velocidade = 0;
    uint32_t inversao = 0;
    uint32_t pos_horizontal = 0;
    uint32_t pos_vertical = 0;
    uint32_t sentido_atual = SENTIDO_POSITIVO | SENTIDO_HORIZONTAL;
    uint32_t tamanho_img = (img_width << WIDTH_SHIFT) | (img_height << HEIGHT_SHIFT);
    while(1)
    {
        // Le entradas
        entrada_escolhida = checa_entradas (entradas_passadas);

        // Transforma imagem
        // Nada apertado -> movimenta imagem
        if (entrada_escolhida == NADA_PRESSIONADO && ((sentido_atual & HORIZ_VERT_MASK) == SENTIDO_HORIZONTAL))
            transforma_imagem(&pos_horizontal, entrada_escolhida, tamanho_img, &sentido_atual);
        else if (entrada_escolhida == NADA_PRESSIONADO && ((sentido_atual & HORIZ_VERT_MASK) == SENTIDO_VERTICAL))
            transforma_imagem(&pos_vertical, entrada_escolhida, tamanho_img, &sentido_atual);
        
        // Esquerda apertado -> Muda sentido (positivo <-> negativo)
        else if (entrada_escolhida == ESQUERDA_PRESSIONADO)
            transforma_imagem((uint32_t*)NULL_PTR, entrada_escolhida, tamanho_img, &sentido_atual);
        
        // Direita apertado -> Muda sentido (vertial <-> horizontal)
        else if (entrada_escolhida == DIREITA_PRESSIONADO)
            transforma_imagem((uint32_t*)NULL_PTR, entrada_escolhida, tamanho_img, &sentido_atual);
        
        // Cima pressionado -> Aumenta veloCidade
        else if (entrada_escolhida == CIMA_PRESSIONADO)
            transforma_imagem((uint32_t*)NULL_PTR, entrada_escolhida, tamanho_img, &velocidade);
        
        // Baixo pressionado -> Diminui veloidade
        else if (entrada_escolhida == BAIXO_PRESSIONADO)
            transforma_imagem((uint32_t*)NULL_PTR, entrada_escolhida, tamanho_img, &velocidade);
        
        // centro pressionado -> Inverte cores
        else if (entrada_escolhida == CENTRO_PRESSIONADO)
            transforma_imagem((uint32_t*)NULL_PTR, entrada_escolhida, tamanho_img, &inversao);

        
        // Exibe imagens
        if ((sentido_atual & HORIZ_VERT_MASK) == SENTIDO_HORIZONTAL)
            exibe_img(img_carro, img_width, img_height, SENTIDO_HORIZONTAL, pos_horizontal, inversao);
        else
            exibe_img(img_aviao, img_width, img_height, SENTIDO_VERTICAL, pos_vertical, inversao);

        // Espera (depende da velocidade)
        delay32Ms(0, (VEL_MAX - velocidade)*DELAY_DELTA + DELAY_MIN);
    }
}

void exibe_img(const uint8_t *imagem, const uint32_t width, const uint32_t height, uint32_t direcao, uint32_t deslocamento, uint32_t inverte)
{
    uint8_t y, x;
    oled_color_t cor_pixel;
    if (!direcao) // Printa linha por linha, movimento vertical
    {
        for (y = 0; y < height; y++)
        {
            for (x = 0; x < width; x++)
            {
              if ((imagem[width*((y+deslocamento)%height) + x] <= 250) != inverte)
                cor_pixel = OLED_COLOR_BLACK;
              else
                cor_pixel = OLED_COLOR_WHITE;
              oled_putPixel(x, y, cor_pixel);
            }
        }
    }
    else // Printa coluna por coluna, moviemnto horizontal
    {
        for (x = 0; x < width; x++)
        {
            for (y = 0; y < height; y++)
            {
              if ((imagem[width*y + ((x+deslocamento)%width)] <= 250) != inverte)
                cor_pixel = OLED_COLOR_BLACK;
              else
                cor_pixel = OLED_COLOR_WHITE;
              oled_putPixel(x, y, cor_pixel);
            }
        }
    }
}



int8_t checa_entradas (uint32_t *entradas_passadas)
{
  // if not pressed, reset counter
  // joystick
  uint8_t joystick = joystick_read();
  if (joystick & JOYSTICK_LEFT)
  {
    entradas_passadas[ESQUERDA_PRESSIONADO]++;
  }
  else
  {
    entradas_passadas[ESQUERDA_PRESSIONADO] = 0;
  }

  if (joystick & JOYSTICK_RIGHT)
  {
    entradas_passadas[DIREITA_PRESSIONADO]++;
  }
  else
  {
    entradas_passadas[DIREITA_PRESSIONADO] = 0;
  }

  if (joystick & JOYSTICK_UP)
  {
    entradas_passadas[CIMA_PRESSIONADO]++;
  }
  else
  {
    entradas_passadas[CIMA_PRESSIONADO] = 0;
  }

  if (joystick & JOYSTICK_DOWN)
  {
    entradas_passadas[BAIXO_PRESSIONADO]++;
  }
  else
  {
    entradas_passadas[BAIXO_PRESSIONADO] = 0;;
  }

  if (joystick & JOYSTICK_CENTER)
  {
    entradas_passadas[CENTRO_PRESSIONADO]++;
  }
  else
  {
    entradas_passadas[CENTRO_PRESSIONADO] = 0;
  }

  for (int8_t i = 0; i < N_INPUTS; i++)
  {
    if (entradas_passadas[i] == N_CYCLES_TO_PRESS)
    {
      return i;
    }
  }
  return NADA_PRESSIONADO;
}
