#ifndef primos_h
#define primos_h

#include "cmsis_os.h"

osThreadId init_primos_thread (osThreadId _scheduler_id);

void primos_thread (const void * arg);

uint32_t my_sqrt (uint32_t op);

uint8_t e_primo (uint32_t num);

#endif // primos_h
