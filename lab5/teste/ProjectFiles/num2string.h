#ifndef num2string_h
#define num2string_h

#include "stdint.h"

void intToString(int64_t value, char * pBuf, uint32_t len, uint32_t base, uint8_t zeros);
void floatToString(float value, char *pBuf, uint32_t len, uint32_t base, uint8_t zeros, uint8_t precision);

#endif // num2string_h
