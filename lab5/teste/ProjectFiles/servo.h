#ifndef servo_h
#define servo_h

#include <stdint.h>

#define PERIOD_US 20000

typedef enum {servo_mao_e, servo_cotovelo_e, servo_ombro_e, servo_torso_e} servo_name_t;

void init_servo(uint32_t clk);

void write_servo(servo_name_t servo, uint16_t duty_cycle);

#endif //servo_h
