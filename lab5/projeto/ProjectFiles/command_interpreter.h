#ifndef command_interpreter_h
#define command_interpreter_h

#include "cmsis_os.h"

typedef struct
{
	uint8_t obj;
	uint8_t next_pos;
} cmd_t;

osThreadId init_command_interpreter_thread (osThreadId _scheduler_id, osMessageQId _uart_msg_q_id, osMailQId _cmd_mail_q_id);
void command_interpreter_thread (const void * arg);
	
#endif // command_interpreter_h
