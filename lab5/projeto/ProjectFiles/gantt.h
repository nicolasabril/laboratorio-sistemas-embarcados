#ifndef gantt_h
#define gantt_h

#include "cmsis_os.h"

// Se definido, envia o diagrama de gantt pela uart
#define SEND_GANTT
#define GANTT_MAIL_Q_LEN 160

typedef struct 
{
	uint32_t time_ms;
	char thread_num;
	char start_or_end;
} gantt_mail_t;

// Inicializa a thread que envia o diagrama de gantt
osThreadId init_gantt_thread (osThreadId _scheduler_id);

// Função executada pela thread que envia o diagrama de gantt
void gantt_thread (const void * arg);

// Cria o pedido para o inicio/fim de uma entry do diagrama de gantt
// tick - numero representando o tempo que vai ser enviado
// thread_num - Numero da thread
// start_or_end - 's' se e o inicio de uma execucao e 'e' se e o final
void gantt_enqueue (uint32_t time_ms, uint8_t thread_num, char start_or_end);

// Envia uma linha do diagrama
void send_gantt (gantt_mail_t mail);

#endif // gantt_h
