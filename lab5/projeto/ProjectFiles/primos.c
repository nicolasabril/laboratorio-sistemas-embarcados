#include "primos.h"
#include "cmsis_os.h"

osThreadDef(primos_thread, osPriorityLow, 1, 0);
osThreadId static scheduler_id;

osThreadId init_primos_thread (osThreadId _scheduler_id)
{
	scheduler_id = _scheduler_id;
	
    return osThreadCreate(osThread(primos_thread), NULL);
}
void primos_thread (const void * arg)
{
	uint32_t ultimo_primo = 3;
	uint32_t num_para_testar;
	
	while (1)
	{
		// Espera o scheduler ativar a thread
		osSignalWait(1, osWaitForever);
		
		// Se e o ultimo primo, da a volta
		if (ultimo_primo == 4294967291U)
		{
			ultimo_primo = 3;
		}
		else
		{
			// Calcula o proximo primo
			num_para_testar = ultimo_primo + 2;
			while (!e_primo(num_para_testar))
				num_para_testar += 2;
			ultimo_primo = num_para_testar;
		}
		
		osSignalSet(scheduler_id, 0x08);
	}
}

// Autor: Craig McQueen (https://stackoverflow.com/questions/1100090/looking-for-an-efficient-integer-square-root-algorithm-for-arm-thumb2)
// Licensa: CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
uint32_t my_sqrt (uint32_t op)
{
  uint32_t res = 0;
  uint32_t one = (uint32_t)1 << 30; // The second-to-top bit is set: use 1u << 14 for uint16_t type; use 1uL<<30 for uint32_t type
  // "one" starts at the highest power of four <= than the argument.
  while (one > op)
  {
    one >>= 2;
  }
  while (one != 0)
  {
    if (op >= res + one)
    {
      op = op - (res + one);
      res = res +  2 * one;
    }
    res >>= 1;
    one >>= 2;
  }
  return res;
}

// Lista com os primeiros primos (quanto mais primos, mais rapido)
uint32_t const static primeiros_primos[] =
{
2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251
};

uint8_t e_primo (uint32_t num)
{
  uint32_t testador; // Numero a ser testado contra a chave, para ver se a chave e multipla de n
  uint32_t i; // Iterador da lista
  uint32_t raiz_num = my_sqrt(num); // Raiz quadrada da chave a ser testada, para saber quando parar o teste
		
  // Testa a chave contra uma lista de primos
  for (i = 0; i<sizeof(primeiros_primos)/sizeof(uint32_t); i++) 
  {
    if(!(num % primeiros_primos[i])) // Se e um multiplo de algum numero na lista, nao e primo
    {
      return 0;
    }
    if(raiz_num <= primeiros_primos[i]) // Se nao e multiplo de nunguem ate a raiz, tem que ser primo
    {
      return 1;
    }
  }
  // Como nao cabem mais primos na lista, vai testando um por um (dois por dois, porque so importam os impares)
  for (testador = primeiros_primos[sizeof(primeiros_primos)/sizeof(uint32_t)-1]+2 ; testador < raiz_num; testador += 2)
  {
    if(!(num % testador)) // Se e um multiplo do numero, nao e primo
    {
      return 0;
    }
  }
  return 1; // Se nao foi pego nos dois testes, tem que ser um primos
}
