#ifndef fibonacci_h
#define fibonacci_h
#include "cmsis_os.h"

osThreadId init_fibonacci_thread (osThreadId _scheduler_id);
void fibonacci_thread (const void * arg);
	
#endif // fibonacci_h
