#include "gantt.h"
#include "uart.h"
#include "cmsis_os.h"
#include "num2string.h"

osMailQId static gantt_mail_q_id;
osMailQDef(gantt_mail_q, GANTT_MAIL_Q_LEN, gantt_mail_t);
osThreadDef(gantt_thread, osPriorityLow, 1, 0);
osThreadId static scheduler_id;

osThreadId init_gantt_thread (osThreadId _scheduler_id)
{	
	scheduler_id = _scheduler_id;
	
    // Cria a mail queue por onde são enviados os pedidos de gantt
    gantt_mail_q_id = osMailCreate(osMailQ(gantt_mail_q), NULL);
	
    // Cria a thread que envia o gantt
    return osThreadCreate(osThread(gantt_thread), NULL);
}
void gantt_thread (const void * arg)
{
    osEvent rcv_event;

    while (1)
    {
		// Espera o scheduler ativar
		osSignalWait(1, osWaitForever);
		
        // Enquanto tem pedidos prontos na fila, envia eles
		rcv_event = osMailGet(gantt_mail_q_id, 0);
        while (rcv_event.status == osEventMail)
        {
            send_gantt(*((gantt_mail_t*)(rcv_event.value.p)));
            osMailFree(gantt_mail_q_id, (void*)rcv_event.value.p);
            // Pega um pedido que esteja pronto na fila
            rcv_event = osMailGet(gantt_mail_q_id, 0);
        }
		
		osSignalSet(scheduler_id, 0x01);
    }
}
void gantt_enqueue (uint32_t time_ms, uint8_t thread_num, char start_or_end)
{
    #ifdef SEND_GANTT
        gantt_mail_t *gantt_mail = osMailAlloc(gantt_mail_q_id, 0);
	
		// Se nao conseguiu alocar memoria (mail queue cheia)
		if(gantt_mail == NULL)
		{
			while(1)
				;
		}
		else
		{
			gantt_mail->thread_num = thread_num + '0';
			gantt_mail->time_ms = time_ms;
			gantt_mail->start_or_end = start_or_end;
			// Coloca o pedido na fila
			osMailPut(gantt_mail_q_id, gantt_mail);
		}
    #endif // SEND_GANTT
}

void send_gantt (gantt_mail_t mail)
{
    #ifdef SEND_GANTT
		char tick_buf[13];
		// Converte o tick em hexa
		intToString(mail.time_ms, tick_buf, 11, 10, 0);

		// INICIA SECAO CRITICA
		// Envia o nome da thread
		writeByteUART(mail.thread_num);

		// Envia se e comeco ou fim de uma entry
		writeByteUART(mail.start_or_end);

		// Envia o tick de inicio/fim da entry
		writeUART(tick_buf, 0);

		// Fim da linha
		writeByteUART('\n');
		// FIM SECAO CRITICA
    #endif // SEND_GANT
}
