#ifndef uart_h
#define uart_h

#include "cmsis_os.h"

//#define UART_ECHO

//#define UART_CLK (uint32_t)g_ui32SysClock
#define UART_BAUD (uint32_t)921600

// Inicializa a UART, setando interrupcao no RX
// mail_id : Id da MailQueue por onde vai ser enviado as mensagens recebidas
void initUART (osMessageQId msg_q_id, uint32_t SysClk, osThreadId _scheduler_id);

// Manda um bloco de dados de comprimento len pela UART
// data : ponteiro para um vetor de bytes a serem enviados
// len : numero de  bytes a serem enviados
void writeUART (char *data, uint32_t len);

// Manda um byte na UART
void writeByteUART (char data);

// Handler de recebimento de dados na UART
// Manda os bytes recebidos pela MailQueue especificada em initUART()
void UART0_Handler (void);

#endif //uart_h
