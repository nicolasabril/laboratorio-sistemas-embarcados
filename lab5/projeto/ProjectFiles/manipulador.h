#ifndef manipulador_h
#define manipulador_h

#include "cmsis_os.h"
#include "servo.h"

#define N_OBJECTS 2
#define N_POS 4

osThreadId init_manipulador_thread (osThreadId _scheduler_id, osMailQId _cmd_mail_q_id);

void manipulador_thread (const void * arg);

void go_to_pos (uint8_t pos);

void open_hand (void);

void close_hand (void);

void move_servo (servo_name_t servo, int32_t old_pos, int32_t new_pos, uint8_t n_steps, uint8_t step);

#endif // manipulador_h
