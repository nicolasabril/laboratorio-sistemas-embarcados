#include "fibonacci.h"
#include "cmsis_os.h"

osThreadDef(fibonacci_thread, osPriorityLow, 1, 0);
osThreadId static scheduler_id;

osThreadId init_fibonacci_thread (osThreadId _scheduler_id)
{
	scheduler_id = _scheduler_id;
	
    return osThreadCreate(osThread(fibonacci_thread), NULL);
}
void fibonacci_thread (const void * arg)
{
	uint32_t f1 = 1;
	uint32_t f2 = 1;
	uint32_t aux;
	
	while(1)
	{
		// Espera o scheduler ativar a thread
		osSignalWait(1, osWaitForever);
		
		// Calcula o proximo numero de fibonacci;
		aux = f1 + f2;
		f1 = f2;
		f2 = aux;
		
		// Avisa o scheduler que terminou a execucao
		osSignalSet(scheduler_id, 0x02);
	}
}
