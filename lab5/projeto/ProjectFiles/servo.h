#ifndef servo_h
#define servo_h

#include <stdint.h>

#define PERIOD_US 20000
#define MIN_DUTY_CYCLE_US 625
#define MAX_DUTY_CYCLE_US 2500

/*
Limites de cada motor
m�o:
  per_min(aberto): 0.625ms
  per_max(fechado): 0.882ms

cotovelo:
  per_min(ombro esticado, no chao): 0.883ms
  per_min(bra�o pra cima):  1.355ms
  per_min(bra�o dobrado): per_max
  per_max(dobrado): 1.914ms

ombro:
  per_min(dobrado): 1.14ms
  per_max(esticado): 2.52ms

tronco:
 per_min(direita): 0.625ms
 per_max(esquerda): 2.52ms
*/

typedef enum {servo_torso_e=0, servo_ombro_e, servo_cotovelo_e, servo_mao_e} servo_name_t;

void init_servo(uint32_t clk);

void write_servo(servo_name_t servo, uint16_t duty_cycle);

#endif //servo_h
