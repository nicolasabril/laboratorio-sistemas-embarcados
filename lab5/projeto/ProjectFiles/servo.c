#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

#include "servo.h"

uint32_t clock;

void init_servo(uint32_t clk)
{	
	uint32_t period_ticks;
	
	// Habilita o clock pro PWM e para as porta que vao ser usadas
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
		
	// Seleciona o modo PWM para as portas
	GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_3);
	GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	
	// Escolhe o clock do PWM
	PWMClockSet(PWM0_BASE, PWM_SYSCLK_DIV_64);
	
	// Configura os pinos como modo PWM
	GPIOPinConfigure(GPIO_PF2_M0PWM2);
	GPIOPinConfigure(GPIO_PF3_M0PWM3);
	GPIOPinConfigure(GPIO_PG0_M0PWM4);
	GPIOPinConfigure(GPIO_PG1_M0PWM5);
	
	// Configura o gerador dos PWM
	PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
	PWMGenConfigure(PWM0_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);	


	clock = clk/64;
	period_ticks = (clock/1000000) * PERIOD_US;
	PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, period_ticks);
	PWMGenPeriodSet(PWM0_BASE, PWM_GEN_2, period_ticks);
	
	PWMGenEnable (PWM0_BASE, PWM_GEN_1);
	PWMGenEnable (PWM0_BASE, PWM_GEN_2);
	
	PWMOutputInvert(PWM0_BASE, PWM_OUT_2_BIT, false);
	PWMOutputInvert(PWM0_BASE, PWM_OUT_3_BIT, false);
	PWMOutputInvert(PWM0_BASE, PWM_OUT_4_BIT, false);
	PWMOutputInvert(PWM0_BASE, PWM_OUT_5_BIT, false);
	
	PWMOutputState(PWM0_BASE, PWM_OUT_2_BIT, true);
	PWMOutputState(PWM0_BASE, PWM_OUT_3_BIT, true);
	PWMOutputState(PWM0_BASE, PWM_OUT_4_BIT, true);
	PWMOutputState(PWM0_BASE, PWM_OUT_5_BIT, true);
	
	// Vai pras posicoes iniciais
	write_servo(servo_mao_e, 0x1680);
	write_servo(servo_cotovelo_e, 0x3400U);
	write_servo(servo_ombro_e, 0x1c00u);
	write_servo(servo_torso_e , 0x2600u);
}

void write_servo(servo_name_t servo, uint16_t duty_cycle)
{
	uint32_t duty_cycle_ticks = PWMGenPeriodGet(PWM0_BASE, PWM_GEN_2)*duty_cycle/0xffff;
	
	switch (servo)
	{
		case servo_mao_e:
			PWMPulseWidthSet(PWM0_BASE, PWM_OUT_5, duty_cycle_ticks);
			break;
		case servo_cotovelo_e:
			PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, duty_cycle_ticks);
			break;
		case servo_ombro_e:
			PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, duty_cycle_ticks);
			break;
		case servo_torso_e:
			PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, duty_cycle_ticks);
			break;
	}
}
