#ifndef display_h
#define display_h

#define USE_DISPLAY 1

#include "cmsis_os.h"
#include "scheduler.h"

osThreadId init_display_thread (osThreadId _scheduler_id);

void display_thread (const void *args);

void init_display (uint32_t SysClk);

void print_menu (void);

void print_thread (tcb_t thread, uint8_t line);

void display_send_scheduler_status (uint32_t ticks, tcb_t *threads);

void print_master_fault(uint8_t tid, uint32_t tick);

void print_secondary_fault (void);

void send_secondary_fault_display(uint8_t tid, uint32_t tick, char late_or_early);

#endif //display_h
