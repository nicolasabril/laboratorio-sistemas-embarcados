#include "command_interpreter.h"
#include "cmsis_os.h"

osThreadDef(command_interpreter_thread, osPriorityLow, 1, 0);
osThreadId static scheduler_id;
osMessageQId static uart_msg_q_id;
osMailQId static cmd_mail_q_id;

osThreadId init_command_interpreter_thread (osThreadId _scheduler_id, osMessageQId _uart_msg_q_id, osMailQId _cmd_mail_q_id)
{
	scheduler_id = _scheduler_id;
	uart_msg_q_id = _uart_msg_q_id;
	cmd_mail_q_id = _cmd_mail_q_id;
	
    return osThreadCreate(osThread(command_interpreter_thread), NULL);
}
void command_interpreter_thread (const void * arg)
{
	typedef enum {obj_e, pos_e} cmd_type_t; 
	
	osEvent uart_msg_event;
	cmd_type_t next_cmd = obj_e;
	cmd_t *cmd_to_send;
	
	while (1)
	{
		// Espera o scheduler ativar a thread
		osSignalWait(1, osWaitForever);

		// Pega a mensagem na fila
		// A principio, sempre tem pelo menos uma mensagem, ja que o scheduler so chama a thread se algo tiver vindo pela uart
		uart_msg_event = osMessageGet(uart_msg_q_id, 0);
		if (uart_msg_event.status == osEventMessage)
		{
			do
			{
				// Se a mensagem indica qual objeto
				if (next_cmd == obj_e)
				{
					if (uart_msg_event.value.v >= '1' && uart_msg_event.value.v <= '2')
					{
						cmd_to_send = osMailAlloc(cmd_mail_q_id, 0);
						if (cmd_to_send != NULL)
						{
							cmd_to_send->obj = uart_msg_event.value.v - '1';
							next_cmd = pos_e;
						}
					}
				}
				// Se a mensagem indica qual a posicao para levar o objeto
				else
				{
					if (uart_msg_event.value.v >= '1' && uart_msg_event.value.v <= '3')
					{
						cmd_to_send->next_pos = uart_msg_event.value.v - '0';
						osMailPut(cmd_mail_q_id, cmd_to_send);
						next_cmd = obj_e;
					}
				}
				
				// Verifica se tem mais alguma mensagem na fila
				uart_msg_event = osMessageGet(uart_msg_q_id, 0);
			} while (uart_msg_event.status == osEventMail);
		}
		
		osSignalSet(scheduler_id, 0x20);
	}
}
