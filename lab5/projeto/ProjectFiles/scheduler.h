#ifndef scheduler_h
#define scheduler_h

#include "cmsis_os.h"
#include <stdbool.h>

#define N_THREADS 6

#define gantt_id 0
#define interpreter_id 1
#define primos_id 2
#define fibonacci_id 3
#define manipulador_id 4
#define display_id 5

#define SCHEDULER_TIMER_PERIOD_MS 2

#define SCHEDULER_PERIOD_MS 10
#define PRIMOS_PERIOD_MS 200
#define MANIPULADOR_PERIOD_MS 100
#define FIBONACCI_PERIOD_MS 1000
#define GANTT_PERIOD_MS 500
#define DISPLAY_PERIOD_MS 100

#define SCHEDULER_PERIOD_TICKS (SCHEDULER_PERIOD_MS / SCHEDULER_TIMER_PERIOD_MS)
#define PRIMOS_PERIOD_TICKS (PRIMOS_PERIOD_MS / SCHEDULER_TIMER_PERIOD_MS)
#define MANIPULADOR_PERIOD_TICKS (MANIPULADOR_PERIOD_MS / SCHEDULER_TIMER_PERIOD_MS)
#define FIBONACCI_PERIOD_TICKS (FIBONACCI_PERIOD_MS / SCHEDULER_TIMER_PERIOD_MS)
#define GANTT_PERIOD_TICKS (GANTT_PERIOD_MS / SCHEDULER_TIMER_PERIOD_MS)
#define DISPLAY_PERIOD_TICKS (DISPLAY_PERIOD_MS / SCHEDULER_TIMER_PERIOD_MS)

// Tempo esperado 4,8ms * 1,3 = 6,24. Arredondando para um multiplo de SCHEDULER_TIMER_PERIOD_MS -> 6
#define PRIMOS_DEADLINE_MS 6
#define MANIPULADOR_DEADLINE_MS 2
#define FIBONACCI_DEADLINE_MS 2
#define GANTT_DEADLINE_MS 8
#define INTERPRETER_DEADLINE_MS 2
#define DISPLAY_DEADLINE_MS 82

#define PRIMOS_DEADLINE_TICKS (PRIMOS_DEADLINE_MS / SCHEDULER_TIMER_PERIOD_MS)
#define MANIPULADOR_DEADLINE_TICKS (MANIPULADOR_DEADLINE_MS / SCHEDULER_TIMER_PERIOD_MS)
#define FIBONACCI_DEADLINE_TICKS (FIBONACCI_DEADLINE_MS / SCHEDULER_TIMER_PERIOD_MS)
#define GANTT_DEADLINE_TICKS (GANTT_DEADLINE_MS / SCHEDULER_TIMER_PERIOD_MS)
#define INTERPRETER_DEADLINE_TICKS (INTERPRETER_DEADLINE_MS / SCHEDULER_TIMER_PERIOD_MS)
#define DISPLAY_DEADLINE_TICKS (DISPLAY_DEADLINE_MS / SCHEDULER_TIMER_PERIOD_MS)

#define SCHEDULER_PRIO_LOWEST 99
#define SCHEDULER_PRIO_LOW 10
#define SCHEDULER_PRIO_BELOW_NORMAL 5
#define SCHEDULER_PRIO_NORMAL 0
#define SCHEDULER_PRIO_ABOVE_NORMAL -15
#define SCHEDULER_PRIO_HIGH -30
#define SCHEDULER_PRIO_HIGHEST -99
#define SCHEDULER_PRIO_REALTIME -100

#define SAMPLE_THREADS_METHOD 2

typedef enum {ready_e, waiting_e, running_e} thread_status_t;

typedef struct
{
	osThreadId os_tid;
	uint8_t my_tid;
	thread_status_t status;
	int8_t prio;
	uint32_t deadline;
	uint32_t start_time;
	uint32_t finish_time;
	uint32_t active_time;
	uint32_t activation_time;
	bool periodic;
	uint32_t period;
	uint32_t deadline_len;
} tcb_t;


void init_scheduler (osThreadId thread_ids[N_THREADS]);

void wake_up_scheduler(const void* args);

void scheduler (void);

void raise_master_fault (uint8_t tid, uint32_t tick);

void raise_secondary_fault (uint8_t tid, uint32_t tick, char late_or_early);

void stop_running_thread (thread_status_t new_status);

void run_thread (tcb_t *thread);

void add_thread_to_scheduler (osThreadId os_tid, uint8_t my_tid, int8_t prio, uint32_t deadline_ticks, bool periodic, uint32_t period_ticks);

void ready_thread (uint8_t thread_id);

void sample_threads (void);

#endif // scheduler_h
