#include "manipulador.h"
#include "servo.h"
#include "cmsis_os.h"
#include "command_interpreter.h"

osThreadDef(manipulador_thread, osPriorityLow, 1, 0);
osMailQId static cmd_mail_q_id;
osThreadId static scheduler_id;

osThreadId init_manipulador_thread (osThreadId _scheduler_id, osMailQId _cmd_mail_q_id)
{	
	scheduler_id = _scheduler_id;
	cmd_mail_q_id = _cmd_mail_q_id;
	
    return osThreadCreate(osThread(manipulador_thread), NULL);
}
void manipulador_thread (const void * arg)
{	
	typedef enum
	{
		waiting_e, 
		old_pos_rotate_e,
		old_pos_extend_e,
		grab_e, 
		retract_e, 
		rotate_e, 
		new_pos_high_e, 
		new_pos_low_e, 
		release_e, 
		go_back_retract_e, 
		go_back_rotate_e
	} manipulador_state_t;
		
	manipulador_state_t state = waiting_e;
	uint8_t obj_pos[N_OBJECTS] = {1, 2};
	osEvent cmd_event;
	cmd_t cmd;
	uint8_t counter = 0;
	uint8_t n_steps_state;
	const uint16_t pos[N_POS][3] =
	{
		{0x2600u, 0x1c00u, 0x3400u},
		{0x1d00u, 0x2d00u, 0x3000u},
		{0x2600u, 0x2f00u, 0x2c00u},
		{0x3200u, 0x3600u, 0x2800u}
	};
	uint16_t old_pos[3];
	
	write_servo(servo_torso_e, pos[0][servo_torso_e]);
	write_servo(servo_cotovelo_e, pos[0][servo_cotovelo_e]);
	write_servo(servo_ombro_e, pos[0][servo_ombro_e]);
	
	old_pos[servo_torso_e] = pos[0][servo_torso_e];
	old_pos[servo_ombro_e] = pos[0][servo_ombro_e];
	old_pos[servo_cotovelo_e] = pos[0][servo_cotovelo_e];
	
	open_hand();
	
	while (1)
	{
		// Espera o scheduler ativar a thread
		osSignalWait(1, osWaitForever);
		
		switch (state)
		{
			case waiting_e:
				cmd_event = osMailGet(cmd_mail_q_id, 0);
				if (cmd_event.status == osEventMail)
				{
					cmd = *((cmd_t *)(cmd_event.value.p));
					if ((cmd.obj <= 1) && (cmd.next_pos >= 1 && cmd.next_pos <= 3))
						state = old_pos_rotate_e;
					osMailFree(cmd_mail_q_id, cmd_event.value.p);
				}
				break;
			case old_pos_rotate_e:
				n_steps_state = 2;
				if (counter < n_steps_state)
				{
					move_servo(servo_torso_e, old_pos[servo_torso_e], pos[obj_pos[cmd.obj]][servo_torso_e], n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					state = old_pos_extend_e;
					old_pos[servo_torso_e] = pos[obj_pos[cmd.obj]][servo_torso_e];
				}
				break;
			case old_pos_extend_e:	
				n_steps_state = 3;
				if (counter < n_steps_state)
				{
					move_servo(servo_cotovelo_e, old_pos[servo_cotovelo_e], pos[obj_pos[cmd.obj]][servo_cotovelo_e], n_steps_state, counter+1);
					move_servo(servo_ombro_e, old_pos[servo_ombro_e], pos[obj_pos[cmd.obj]][servo_ombro_e], n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					state = grab_e;
					old_pos[servo_ombro_e] = pos[obj_pos[cmd.obj]][servo_ombro_e];
					old_pos[servo_cotovelo_e] = pos[obj_pos[cmd.obj]][servo_cotovelo_e];
				}
				break;	
			case grab_e:
				n_steps_state = 1;
				if (counter < n_steps_state)
				{
					close_hand();
					counter++;
				}
				else
				{
					counter = 0;
					state = retract_e;
				}
				break;
			case retract_e:
				n_steps_state = 5;
				if (counter < n_steps_state)
				{
					move_servo(servo_cotovelo_e, old_pos[servo_cotovelo_e], pos[0][servo_cotovelo_e], n_steps_state, counter+1);
					move_servo(servo_ombro_e, old_pos[servo_ombro_e], pos[0][servo_ombro_e], n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					state = rotate_e;
					old_pos[servo_ombro_e] = pos[0][servo_ombro_e];
					old_pos[servo_cotovelo_e] = pos[0][servo_cotovelo_e];
				}
				break;
			case rotate_e:
				n_steps_state = 2;
				if (counter < n_steps_state)
				{
					move_servo(servo_torso_e, old_pos[servo_torso_e], pos[cmd.next_pos][servo_torso_e], n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					state = new_pos_high_e;
					old_pos[servo_torso_e] = pos[cmd.next_pos][servo_torso_e];
				}
				break;
			case new_pos_high_e:
				n_steps_state = 3;
				if (counter < n_steps_state)
				{
					move_servo(servo_cotovelo_e, old_pos[servo_cotovelo_e], pos[cmd.next_pos][servo_cotovelo_e], n_steps_state, counter+1);
					move_servo(servo_ombro_e, old_pos[servo_ombro_e], pos[cmd.next_pos][servo_ombro_e] - 0x500, n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					obj_pos[cmd.obj] = cmd.next_pos;
					state = new_pos_low_e;
					old_pos[servo_ombro_e] = pos[cmd.next_pos][servo_ombro_e] - 0x500;
					old_pos[servo_cotovelo_e] = pos[cmd.next_pos][servo_cotovelo_e];
				}
				break;
			case new_pos_low_e:
				n_steps_state = 1;
				if (counter < n_steps_state)
				{
					move_servo(servo_cotovelo_e, old_pos[servo_cotovelo_e], pos[cmd.next_pos][servo_cotovelo_e], n_steps_state, counter+1);
					move_servo(servo_ombro_e, old_pos[servo_ombro_e], pos[cmd.next_pos][servo_ombro_e], n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					obj_pos[cmd.obj] = cmd.next_pos;
					state = release_e;
					old_pos[servo_ombro_e] = pos[cmd.next_pos][servo_ombro_e];
					old_pos[servo_cotovelo_e] = pos[cmd.next_pos][servo_cotovelo_e];
				}
				break;
			case release_e:
				n_steps_state = 1;
				if (counter < n_steps_state)
				{
					open_hand();
					counter++;
				}
				else
				{
					counter = 0;
					obj_pos[cmd.obj] = cmd.next_pos;
					state = go_back_retract_e;
				}
				break;
			case go_back_retract_e:
				n_steps_state = 5;
				if (counter < n_steps_state)
				{
					move_servo(servo_cotovelo_e, old_pos[servo_cotovelo_e], pos[0][servo_cotovelo_e], n_steps_state, counter+1);
					move_servo(servo_ombro_e, old_pos[servo_ombro_e], pos[0][servo_ombro_e], n_steps_state, counter+1);

					counter++;
				}
				else
				{
					counter = 0;
					state = go_back_rotate_e;
					old_pos[servo_ombro_e] = pos[0][servo_ombro_e];
					old_pos[servo_cotovelo_e] = pos[0][servo_cotovelo_e];
				}
				break;
			case go_back_rotate_e:
				n_steps_state = 2;
				if (counter < n_steps_state)
				{
					move_servo(servo_torso_e, old_pos[servo_torso_e], pos[0][servo_torso_e], n_steps_state, counter+1);
					counter++;
				}
				else
				{
					counter = 0;
					state = waiting_e;
					old_pos[servo_torso_e] = pos[0][servo_torso_e];
				}
				break;
		}
		
		osSignalSet(scheduler_id, 0x04);
	}
}

void open_hand (void)
{
	write_servo(servo_mao_e, 0x1100);
}

void close_hand (void)
{
	write_servo(servo_mao_e, 0x1700);
}

void move_servo (servo_name_t servo, int32_t old_pos, int32_t new_pos, uint8_t n_steps, uint8_t step)
{
	uint16_t step_pos = old_pos + ((new_pos - old_pos)*step)/(n_steps);
	write_servo(servo, step_pos);
}
