#include "display.h"
#include "cmsis_os.h"
#include "grlib/grlib.h"
#include "cfaf128x128x16.h"
#include "scheduler.h"
#include "num2string.h"

osThreadDef(display_thread, osPriorityLow, 1, 0);
tContext sContext;
static osThreadId scheduler_id;
static const char nums[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
static const char *thread_names[N_THREADS] = {"Gantt  ", "Interpr", "Primos ", "Fibonac", "Manipul", "Display"};
static bool draw_secondary_fault = false;

typedef struct
{
	tcb_t threads[N_THREADS];
	uint32_t tick;
	uint32_t n_faults;
	uint8_t last_fault_tid;
	uint8_t last_fault_tick;
	char last_fault_l_or_e;
} scheduler_status_t;

static scheduler_status_t status;

void init_display (uint32_t SysClk)
{
	#if USE_DISPLAY == 1
		cfaf128x128x16Init(SysClk);
		GrContextInit(&sContext, &g_sCfaf128x128x16);
		GrContextBackgroundSet(&sContext, ClrWhite);
		GrContextForegroundSet(&sContext, ClrBlack);
		GrFlush(&sContext);
		GrContextFontSet(&sContext, &g_sFontFixed6x8);
		print_menu();
	#endif // USE_DISPLAY
}

osThreadId init_display_thread (osThreadId _scheduler_id)
{
	scheduler_id = _scheduler_id;
	
	status.n_faults = 0;
	
	return osThreadCreate(osThread(display_thread), NULL);
}

void display_thread (const void *args)
{
	uint8_t i;
	char tick_buf[11];
	
	while (1)
	{
		// Espera o scheduler ativar
		osSignalWait(1, osWaitForever);
		
		#if USE_DISPLAY == 1
			// Informacoes das threads
			for (i = 0; i < N_THREADS; i++)
			{
				print_thread(status.threads[i], i);
			}
			// Ticks
			intToString(status.tick, tick_buf, 10, 10, 0);
			GrStringDraw(&sContext, tick_buf, -1, (sContext.psFont->ui8MaxWidth)*7 + 1, (sContext.psFont->ui8Height+3)*6 + 4, true);
			
			// Secondary fault
			if (draw_secondary_fault == true)
				print_secondary_fault();
		#endif // USE_DISPLAY
			
		osSignalSet(scheduler_id, 0x10);
	}
}

void print_menu (void)
{
	#if USE_DISPLAY == 1
		GrStringDraw(&sContext, "S:  P:     %:   R:    ", -1, 1,  (sContext.psFont->ui8Height+3)*0 + 4, true);
		GrStringDraw(&sContext, "S:  P:     %:   R:    ", -1, 1,  (sContext.psFont->ui8Height+3)*1 + 4, true);
		GrStringDraw(&sContext, "S:  P:     %:   R:    ", -1, 1,  (sContext.psFont->ui8Height+3)*2 + 4, true);
		GrStringDraw(&sContext, "S:  P:     %:   R:    ", -1, 1,  (sContext.psFont->ui8Height+3)*3 + 4, true);
		GrStringDraw(&sContext, "S:  P:     %:   R:    ", -1, 1,  (sContext.psFont->ui8Height+3)*4 + 4, true);
		GrStringDraw(&sContext, "S:  P:     %:   R:    ", -1, 1,  (sContext.psFont->ui8Height+3)*5 + 4, true);
		GrStringDraw(&sContext, "Ticks: 0",               -1, 1,  (sContext.psFont->ui8Height+3)*6 + 4, true);
		GrStringDraw(&sContext, "Faults occured: 0",      -1, 1,  (sContext.psFont->ui8Height+3)*7 + 4, true);
		GrStringDraw(&sContext, "Last fault",             -1, 33, (sContext.psFont->ui8Height+3)*8 + 4, true);
	#endif // USE_DISPLAY
}

void print_thread (tcb_t thread, uint8_t line)
{
	char status_char;
	char prio_buf[5];
	int32_t percent;
	char percent_buf[3];
	uint32_t relax_time;
	char relax_buf[5];
	
	// Estado "S:"
	switch (thread.status)
	{
		case ready_e:
			status_char = 'r';
			break;
		case waiting_e:
			status_char = 'w';
			break;
		case running_e:
			status_char = 'R';
			break;
	}
	GrStringDraw(&sContext, &status_char, 1, (sContext.psFont->ui8MaxWidth)*2 + 1, (sContext.psFont->ui8Height+3)*line + 4, true);
	
	// Prioridade "P:"
	intToString(thread.prio, prio_buf, 4, 10, 3);
	if (prio_buf[0] != '-')
		prio_buf[0] = '+';
	GrStringDraw(&sContext, prio_buf, -1, (sContext.psFont->ui8MaxWidth)*6 + 1, (sContext.psFont->ui8Height+3)*line + 4, true);
	
	// Porcentagem de execucao "%:"
	percent = thread.active_time*100 / thread.deadline_len;
	if (percent < 100)
	{
		intToString(percent, percent_buf, 2, 10, 2);
	}
	else
	{
		percent_buf[0] = '>';
		percent_buf[1] = '1';
		percent_buf[2] = '\0';
	}
	GrStringDraw(&sContext, percent_buf, 2, (sContext.psFont->ui8MaxWidth)*13 + 1, (sContext.psFont->ui8Height+3)*line + 4, true);
	
	// Tempo de relaxamento/atraso "R:"
	if (thread.status != waiting_e)
		relax_time = thread.deadline - status.tick;
	else
		relax_time = 0;
	intToString(relax_time, relax_buf, 3, 10, 3);
	GrStringDraw(&sContext, relax_buf, -1, (sContext.psFont->ui8MaxWidth)*18 + 1, (sContext.psFont->ui8Height+3)*line + 4, true);
}

void display_send_scheduler_status (uint32_t ticks, tcb_t *threads)
{
	uint8_t i;
	for (i = 0; i < N_THREADS; i++)
	{
		status.threads[i] = threads[i];
	}
	status.tick = ticks;
}

void print_master_fault (uint8_t tid, uint32_t tick)
{
	#if USE_DISPLAY == 1
		char tick_buf[11];
		char fault_buf[7];
	
		status.n_faults++;
	
		intToString(status.n_faults, fault_buf, 6, 10, 0);
		GrStringDraw(&sContext, fault_buf        , -1, (sContext.psFont->ui8MaxWidth)*16 + 1, (sContext.psFont->ui8Height+3)*7 + 4, true);
	
		GrStringDraw(&sContext, "Master Fault        ", -1, 1, (sContext.psFont->ui8Height+3)*9 + 4, true);
		GrStringDraw(&sContext, &(nums[tid]), 1, (sContext.psFont->ui8MaxWidth)*13 + 1, (sContext.psFont->ui8Height+3)*9 + 4, true);
		intToString(tick, tick_buf, 10, 10, 0);
		GrStringDraw(&sContext, tick_buf, -1, 1, (sContext.psFont->ui8Height+3)*10 + 4, true);
	#endif // USE_DISPLAY
}

void print_secondary_fault (void)
{
	#if USE_DISPLAY == 1
		char tick_buf[11];
		char fault_buf[7];

		intToString(status.n_faults, fault_buf, 6, 10, 0);
		GrStringDraw(&sContext, fault_buf , -1, (sContext.psFont->ui8MaxWidth)*16 + 1, (sContext.psFont->ui8Height+3)*7 + 4, true);
		if (status.n_faults < 2)
			GrStringDraw(&sContext, "Secondary Fault", -1, 1, (sContext.psFont->ui8Height+3)*9 + 4, true);
		
		if (status.last_fault_l_or_e == 'L')
			GrStringDraw(&sContext, "Late", 4, (sContext.psFont->ui8MaxWidth)*16 + 1, (sContext.psFont->ui8Height+3)*9 + 4, true);
		else if (status.last_fault_l_or_e == 'E')
			GrStringDraw(&sContext, "Early", 5, (sContext.psFont->ui8MaxWidth)*16 + 1, (sContext.psFont->ui8Height+3)*9 + 4, true);
		else
			GrStringDraw(&sContext, "Error", 5, (sContext.psFont->ui8MaxWidth)*16 + 1, (sContext.psFont->ui8Height+3)*9 + 4, true);
		
		GrStringDraw(&sContext, thread_names[status.last_fault_tid], -1, (sContext.psFont->ui8MaxWidth)*0 + 1, (sContext.psFont->ui8Height+3)*10 + 4, true);
		
		GrStringDraw(&sContext, "t=", 2, (sContext.psFont->ui8MaxWidth)*8 + 1, (sContext.psFont->ui8Height+3)*10 + 4, true);
		intToString(status.last_fault_tick, tick_buf, 10, 10, 0);
		GrStringDraw(&sContext, tick_buf, -1, (sContext.psFont->ui8MaxWidth)*10 + 1, (sContext.psFont->ui8Height+3)*10 + 4, true);
	#endif // USE_DISPLAY
	draw_secondary_fault = false;
}

void send_secondary_fault_display(uint8_t tid, uint32_t tick, char late_or_early)
{
	draw_secondary_fault = true;
	status.n_faults++;
	status.last_fault_l_or_e = late_or_early;
	status.last_fault_tick = tick;
	status.last_fault_tid = tid;
}
