#include "scheduler.h"
#include "display.h"
#include "gantt.h"
#include <stdlib.h>
#include <stdbool.h>

static osThreadId scheduler_id;
static tcb_t threads[N_THREADS];
static uint32_t ticks = 0;
static uint32_t scheduler_last_activation = 0;
static tcb_t *running_thread = NULL;

void init_scheduler (osThreadId thread_ids[N_THREADS])
{
	scheduler_id = osThreadGetId();
	
	osThreadSetPriority(scheduler_id, osPriorityAboveNormal);
	
	add_thread_to_scheduler(thread_ids[gantt_id],       gantt_id,       SCHEDULER_PRIO_NORMAL,   GANTT_DEADLINE_TICKS,       true,  GANTT_PERIOD_TICKS);
	add_thread_to_scheduler(thread_ids[fibonacci_id],   fibonacci_id,   SCHEDULER_PRIO_NORMAL,   FIBONACCI_DEADLINE_TICKS,   true,  FIBONACCI_PERIOD_TICKS);
	add_thread_to_scheduler(thread_ids[manipulador_id], manipulador_id, SCHEDULER_PRIO_LOW,      MANIPULADOR_DEADLINE_TICKS, true,  MANIPULADOR_PERIOD_TICKS);
	add_thread_to_scheduler(thread_ids[primos_id],      primos_id,      SCHEDULER_PRIO_REALTIME, PRIMOS_DEADLINE_TICKS,      true,  PRIMOS_PERIOD_TICKS);
	add_thread_to_scheduler(thread_ids[display_id],     display_id,     SCHEDULER_PRIO_BELOW_NORMAL,   	 DISPLAY_DEADLINE_TICKS,     true,  DISPLAY_PERIOD_TICKS);
	add_thread_to_scheduler(thread_ids[interpreter_id], interpreter_id, SCHEDULER_PRIO_HIGH,     INTERPRETER_DEADLINE_TICKS, false, NULL);
}

void scheduler ()
{
	osEvent scheduler_event;
	int16_t highest_prio_ready;
	tcb_t *highest_prio_ready_thread;
	uint8_t i;
	
	while(1)
	{		
		// Espera o scheduler ser ativado (ou pelo timer ou porque alguma thread acabou)
		scheduler_event = osSignalWait(0, osWaitForever);
		
		// Verifica se a thread de tempo real estourou a deadline
		if ((threads[primos_id].status != waiting_e) && (ticks > threads[primos_id].deadline))
		{
			raise_master_fault(primos_id, ticks);
		}
					
		// Incrementa o contador de tempo executando
		if (running_thread != NULL)
			running_thread->active_time += ticks - scheduler_last_activation;
		
		// Se uma thread terminou a execucao, poe pra dormir e verifica a deadline 
		if (scheduler_event.value.signals & 0x3f)
		{
			// Se nao e uma thread realtime, verifica Secondary Fault
			if (running_thread->prio > SCHEDULER_PRIO_REALTIME)
			{
				// Se atrasou
				if (ticks > running_thread->deadline)
				{
					raise_secondary_fault(running_thread->my_tid, ticks, 'L');
					if (running_thread->prio > SCHEDULER_PRIO_HIGHEST)
						running_thread->prio -= 1;
				}
				// Se adiantou
				else if (ticks < (running_thread->deadline - running_thread->start_time)/2)
				{
					raise_secondary_fault(running_thread->my_tid, ticks, 'E');
					if (running_thread->prio < SCHEDULER_PRIO_LOWEST)
						running_thread->prio += 1;
				}
			}	
			stop_running_thread(waiting_e);
		}
				
		// Verifica se alguma thread tem que ser ativada
		// Se esta na hora de uma das threads periodicas
		for (i = 0; i < N_THREADS; i++)
		{
			if (threads[i].periodic && (threads[i].status == waiting_e) && (ticks >= (threads[i].finish_time + threads[i].period)))
			{
				ready_thread(i);
			}
		}
		// Se chegou uma mensagem na uart, acorda o interpretador
		if ((scheduler_event.value.signals & 0x80) && (threads[interpreter_id].status == waiting_e))
		{
			ready_thread(interpreter_id);
		}
		
		// Escolhe a proxima thread
		highest_prio_ready = SCHEDULER_PRIO_LOWEST;
		highest_prio_ready_thread = NULL;
		for (i = 0; i < N_THREADS; i++)
		{
			if (threads[i].status != waiting_e)
			{
				if ((threads[i].prio < highest_prio_ready) || ((threads[i].prio == highest_prio_ready) && (threads[i].deadline < highest_prio_ready_thread->deadline)))
				{
					highest_prio_ready = threads[i].prio;
					highest_prio_ready_thread = &(threads[i]);
				}
			}
		}
		
		// Coloca a thread escolhida para rodar
 		if (highest_prio_ready_thread != NULL)
		{
			// Se esta interrompendo outra thread, para a outra
			if ((running_thread != NULL) && (running_thread != highest_prio_ready_thread))
			{
				stop_running_thread(ready_e);
			}
			
			run_thread(highest_prio_ready_thread);
		}
		
		// Roda a amostragem de threads
		sample_threads();
		
		// Guarda o tempo de ativacao do scheduler
		scheduler_last_activation = ticks;
	}
}

void wake_up_scheduler(const void* args)
{
	if (!(ticks % SCHEDULER_PERIOD_TICKS))
		osSignalSet(scheduler_id, 0x40);
	ticks += 1;
}

void raise_master_fault (uint8_t tid, uint32_t tick)
{
	print_master_fault(tid, tick);
	while (1)
		;
}

void raise_secondary_fault (uint8_t tid, uint32_t tick, char late_or_early)
{
	send_secondary_fault_display(tid, tick, late_or_early);
}

void ready_thread (uint8_t thread_id)
{
	threads[thread_id].status = ready_e;
	threads[thread_id].start_time = ticks;
	threads[thread_id].active_time = 0;
	threads[thread_id].deadline = ticks + threads[thread_id].deadline_len;
}

void stop_running_thread (thread_status_t new_status)
{
	gantt_enqueue(ticks*SCHEDULER_TIMER_PERIOD_MS, running_thread->my_tid, 'E');
	
	if (new_status == waiting_e)
	{
		running_thread->finish_time = ticks;
		running_thread->active_time = 0;
	}
	running_thread->status = new_status;
	osThreadSetPriority(running_thread->os_tid, osPriorityLow);
	
	running_thread = NULL;
}

void run_thread (tcb_t *thread)
{
	// Se ja estava na mesma thread, so precisa atualizar activation_time
	if (running_thread != thread)
	{
		running_thread = thread;
		osThreadSetPriority(running_thread->os_tid, osPriorityNormal);
		running_thread->status = running_e;
		
		// Se a thread esta comecando, envia um sinal
		if (running_thread->active_time == 0)
			osSignalSet(running_thread->os_tid, 1);
		
		gantt_enqueue(ticks*SCHEDULER_TIMER_PERIOD_MS, running_thread->my_tid, 'S');
	}
	running_thread->activation_time = ticks;
}

void add_thread_to_scheduler (osThreadId os_tid, uint8_t my_tid, int8_t prio, uint32_t deadline_ticks, bool periodic, uint32_t period_ticks)
{
	threads[my_tid].os_tid = os_tid;
	osThreadSetPriority(threads[gantt_id].os_tid, osPriorityLow);
	threads[my_tid].my_tid = my_tid;
	threads[my_tid].prio = prio;
	threads[my_tid].status = waiting_e;
	threads[my_tid].start_time = 0;
	threads[my_tid].finish_time = 0;
	threads[my_tid].activation_time = 0;
	threads[my_tid].active_time = 0;
	threads[my_tid].periodic = periodic;
	threads[my_tid].period = period_ticks;
	threads[my_tid].deadline = 0;
	threads[my_tid].deadline_len = deadline_ticks;
}

void sample_threads (void)
{
	#if SAMPLE_THREADS_METHOD == 0
	static uint32_t sample_threads_counter = 0;
	static uint32_t sample_threads_wait_ticks = 0;
	
	sample_threads_counter += ticks - scheduler_last_activation;
	
	if (sample_threads_counter > sample_threads_wait_ticks)
	{
		display_send_scheduler_status(ticks, threads);
		sample_threads_counter = 0;
		sample_threads_wait_ticks = rand() % DISPLAY_PERIOD_TICKS;
	}
	
	#elif SAMPLE_THREADS_METHOD == 1
	if ((rand() % 100) < 20)
		display_send_scheduler_status(ticks, threads);
	
	#elif SAMPLE_THREADS_METHOD == 2
	#define N_SAMPLES 1000
	static tcb_t threads_samples[N_SAMPLES][N_THREADS];
	static uint32_t ticks_samples[N_SAMPLES];
	static uint32_t sample_counter = 0;
	static uint32_t tick_display = 0;
	static bool display_starting = false;
	
	uint8_t i;
	uint8_t chosen_sample;
	
	// Faz uma amostra do estado do scheduler
	for (i = 0; i < N_THREADS; i++)
	{
		threads_samples[sample_counter][i] = threads[i];
	}
	ticks_samples[sample_counter] = ticks;
	
	if (ticks > tick_display && display_starting)
	{
		chosen_sample = rand() % sample_counter;
		display_send_scheduler_status(ticks_samples[chosen_sample], threads_samples[chosen_sample]);
		sample_counter = 0;
		display_starting = false;
	}
	else
	{
		sample_counter++;
	}
	
	// Se o display ficou ready escolhe uma amostra e manda
	if(ticks == threads[display_id].start_time)
	{
		tick_display = ticks;
		display_starting = true;
	}
	#endif
	
}
