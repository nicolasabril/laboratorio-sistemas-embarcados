/*============================================================================
 *                               Laboratório 3
 *                       Nicolas Abril e Lucca Rawlyk
 *---------------------------------------------------------------------------*
 *                    Prof. Andr� Schneider de Oliveira
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *===========================================================================*/
#include "cmsis_os.h"                   // ARM::CMSIS:RTOS:Keil RTX
#include "TM4C129.h"                    // Device header
#include <stdbool.h>
#include "driverlib/sysctl.h"
#include "servo.h"
#include "uart.h"
#include "display.h"

#include "scheduler.h"
#include "gantt.h"
#include "fibonacci.h"
#include "primos.h"
#include "manipulador.h"
#include "command_interpreter.h"

void init_threads (osThreadId scheduler_id, osMailQId cmd_mail_q_id, osMessageQId uart_msg_q_id);

osMessageQDef(uart_msg_q, 8, uint8_t);
osMailQDef(cmd_mail_q, 8, cmd_t);
osTimerDef(scheduler_timer, wake_up_scheduler);
osThreadId thread_ids[N_THREADS];

int main (void)
{
	uint32_t SysClock;
	osMessageQId uart_msg_q_id;
	osMailQId cmd_mail_q_id;
	osTimerId scheduler_timer_id;
	
	// Seta a frequencia para 120MHz
	SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), 120000000);
	
	// Cria as estruturas de ITC
	uart_msg_q_id = osMessageCreate(osMessageQ(uart_msg_q), NULL);
	cmd_mail_q_id = osMailCreate(osMailQ(cmd_mail_q), NULL);
	
	// Inicializa perifericos
	init_servo(SysClock);
	initUART(uart_msg_q_id, SysClock, osThreadGetId());
	init_display(SysClock);

	// Inicializa threads e configura para o scheduler
	init_threads(osThreadGetId(), cmd_mail_q_id, uart_msg_q_id);
	init_scheduler(thread_ids);
	
	// Cria o timer do scheduler
	scheduler_timer_id = osTimerCreate(osTimer(scheduler_timer), osTimerPeriodic, NULL);

	// Inicia o OS
	osKernelStart();
	
	// Inicia o timer do scheduler
	osTimerStart(scheduler_timer_id, SCHEDULER_TIMER_PERIOD_MS);
	
	// Main vira o scheduler
	scheduler();
}

void init_threads (osThreadId scheduler_id, osMailQId cmd_mail_q_id, osMessageQId uart_msg_q_id)
{
	thread_ids[gantt_id] = init_gantt_thread(scheduler_id);
	thread_ids[interpreter_id] = init_command_interpreter_thread(scheduler_id, uart_msg_q_id, cmd_mail_q_id);
	thread_ids[primos_id] = init_primos_thread(scheduler_id);
	thread_ids[fibonacci_id] = init_fibonacci_thread(scheduler_id);
	thread_ids[manipulador_id] = init_manipulador_thread(scheduler_id, cmd_mail_q_id);
	thread_ids[display_id] = init_display_thread(scheduler_id);
}
