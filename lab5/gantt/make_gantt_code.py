"""
Gerador de diagramas de Gantt
Para utilizar, coloque o arquivo da captura da UART com o nome capture.txt neste diretório
Depois, é só rodar este script e abrir index.htm
"""

from datetime import datetime

f = open('./capture.txt', 'r')

content = f.read()

lines = content.split('\n')

time_stamps = []
for line in lines:
    ts = {}
    ts['type'] = line[1]
    ts['thread'] = line[0]
    ts['time_ms'] = line[2:]
    time_stamps.append(ts)

for t in time_stamps:
    t['time_us'] = 100*(int(t['time_ms'],10))

threads = {'0': 'gantt', '1': 'interpreter', '2': 'primos', '3':'fibonacci', '4': 'manipulador', '5': 'display'}

output = 'var tasks = \n['
divisor_seconds = 4
for i,t in enumerate(time_stamps):
    if t['type'] == 'S':
        j = i+1
        found = False
        while j < len(time_stamps):
            if t['thread'] == time_stamps[j]['thread']:
                found = True
                output += '\n{\n'
                output += '    "startDate": new Date("'
                output += datetime.fromtimestamp(t['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
                output += '"),\n    "endDate": new Date("'
                output += datetime.fromtimestamp(time_stamps[j]['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
                output += '"),\n    "taskName": "' + threads[t['thread']] + '"\n},'
                break
            j += 1
        if not found:
            output += '\n{\n'
            output += '    "startDate": new Date("'
            output += datetime.fromtimestamp(t['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
            output += '"),\n    "endDate": new Date("'
            output += datetime.fromtimestamp(time_stamps[len(time_stamps)-1]['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
            output += '"),\n    "taskName": "' + threads[t['thread']] + '"\n},'
output = output[:-1]
output += '\n];'

output_file = open('gantt.js', 'w')
output_file.write(output)

ending = '''var taskNames = [ "gantt", "interpreter", "primos", "fibonacci", "manipulador", "display"];

var gantt = d3.gantt().taskTypes(taskNames);
gantt(tasks);
'''
output_file.write(ending)

f.close()
output_file.close()
