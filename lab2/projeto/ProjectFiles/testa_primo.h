/*----------------------------------------------------------------------------
 * Thread 3 'testa_primo': Verifica se a chave gerada e primo
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef TESTA_PRIMO_H
#define TESTA_PRIMO_H

#include "cmsis_os.h"   

void testa_primo (void const *argument);               // thread function

void init_testa_primo (void);

uint32_t my_sqrt(uint32_t a_nInput);

uint8_t testa_primalidade (uint32_t num);

#endif
