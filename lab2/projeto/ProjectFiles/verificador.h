/*----------------------------------------------------------------------------
 * Thread 7 'verificador': Verifica se todos os testes foram realizados e decide se continua ou para
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef VERIFICADOR_H
#define VERIFICADOR_H

#include "cmsis_os.h"

void init_verificador (osThreadId tid_main);

void verificador (const void *tid_main);               // thread function

#endif
