/*============================================================================
 *                               Laboratório 2
 *                       Nicolas Abril e Lucca Rawlyk
 *---------------------------------------------------------------------------*
 *                    Prof. Andr� Schneider de Oliveira
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *===========================================================================*/

#include "cmsis_os.h"
#include "TM4C129.h" // Device header
#include <stdbool.h>

#include "key_generator.h"
#include "verificador.h"
#include "testa_primo.h"
#include "testa_penultima_word.h"
#include "testa_ultima_word.h"
#include "printer.h"
#include "decodificador.h"
#include "recursos_compartilhados.h"
#include "display.h"

// Contexto do display
tContext sContext;

extern osThreadId tid_key_generator;
extern osThreadId tid_decodificador;
extern osThreadId tid_printer;
extern osThreadId tid_testa_penultima_word;
extern osThreadId tid_testa_primo;
extern osThreadId tid_testa_ultima_word;
extern osThreadId tid_verificador;

void init_key_generator(void);
void init_testa_primo(void);
void init_decodificador(void);
void init_testa_penultima_word(void);
void init_testa_ultima_word(void);
void init_printer(void);
void init_verificador(osThreadId tid_main);

void init_threads(void) // Inicializa todas as threads, salvando os tid em variaveis
{
	init_key_generator();
	init_decodificador();
	init_testa_primo();
	init_testa_penultima_word();
	init_testa_ultima_word();
	init_printer();
	init_verificador(osThreadGetId());
}

void terminate_threads(void) // Termina todas as threads alem da main
{
	osThreadTerminate(tid_key_generator);
	osThreadTerminate(tid_testa_primo);
	osThreadTerminate(tid_decodificador);
	osThreadTerminate(tid_testa_penultima_word);
	osThreadTerminate(tid_testa_ultima_word);
	osThreadTerminate(tid_printer);
	osThreadTerminate(tid_verificador);
}

// Variaveis criticas (tem um lock associado)
uint32_t mensagem_work[LEN_MSG];
uint32_t chave_teste = 0;
uint32_t primo_anterior = 0;
uint8_t e_primo = false;
uint8_t ultima_word_valida = false;
uint8_t penultima_word_valida = false;

uint8_t precisa_gerar_chave = true;
uint8_t testou_primo = false;
uint8_t	testou_ultima_word = false;
uint8_t testou_penultima_word = false;
uint8_t decodificou = false;
uint8_t printou = false;

// Locks de recurso
lock_t l_mensagem;
lock_t l_chave_teste;
lock_t l_primo_anterior;
lock_t l_e_primo;
lock_t l_ultima_word_valida;
lock_t l_penultima_word_valida;

// Locks de execução
lock_t l_precisa_gerar_chave;
lock_t l_testou_primo;
lock_t l_testou_ultima_word;
lock_t l_testou_penultima_word;
lock_t l_decodificou;
lock_t l_printou;

void create_locks(void) // Cria locks para todos os recursos compartilhados
{
	l_mensagem = create_lock(mensagem_work);
	l_chave_teste = create_lock(&chave_teste);
	l_primo_anterior = create_lock(&primo_anterior);
	l_e_primo = create_lock(&e_primo);
	l_ultima_word_valida = create_lock(&ultima_word_valida);
	l_penultima_word_valida = create_lock(&penultima_word_valida);

	l_precisa_gerar_chave = create_lock(&precisa_gerar_chave);
	l_testou_primo = create_lock(&testou_primo);
	l_decodificou = create_lock(&decodificou);
	l_testou_ultima_word = create_lock(&testou_ultima_word);
	l_testou_penultima_word = create_lock(&testou_penultima_word);
	l_printou = create_lock(&printou);
}

/*----------------------------------------------------------------------------
 *      Variáveis globais
 *---------------------------------------------------------------------------*/
// Mensagem conforme dada pelo professor
uint32_t const mensagem_original[LEN_MSG] =   // Mensagem 1
{
	0x0001995a, 0xfffe6766, 0x00019976, 0xfffe6768,
	0x0001997b, 0xfffe6761, 0x00019927, 0xfffe6726,
	0x00019927, 0xfffe674c, 0x00019968, 0xfffe6767,
	0x0001997b, 0xfffe675a, 0x00019975, 0xfffe675a,
	0x00019927, 0xfffe675f, 0x0001996c, 0xfffe675a,
	0x0001997b, 0xfffe6727, 0x00019927, 0xfffe674b,
	0x00019976, 0xfffe675b, 0x00019927, 0xfffe674d,
	0x0001996f, 0xfffe6768, 0x00019974, 0xfffe675a,
	0x0001997a, 0x0002658a, 0xfffebf8e
};
/*uint32_t const mensagem_original[LEN_MSG] =   // Mensagem 2
{
	0x8bae2088, 0x7451e033, 0x8bae20a8, 0x7451e02f, 
	0x8bae20bc, 0x7451dfdd, 0x8bae2085, 0x7451e02f, 
	0x8bae20a8, 0x7451e01e, 0x8bae20b7, 0x7451e025, 
	0x8bae2063, 0x7451e016, 0x8bae20b2, 0x7451e032, 
	0x8bae2063, 0x7451e011, 0x8bae20a4, 0x7451e028, 
	0x8bae20a8, 0x7451dfdd, 0x8bae2070, 0x7451dfdd, 
	0x8bae2097, 0x7451e025, 0x8bae20a8, 0x7451dfdd, 
	0x8bae2093, 0x7451e02c, 0x8bae20af, 0x7451e026, 
	0x8bae20a6, 0xd1853064, 0x7451dfbe
};*/

/*uint32_t const mensagem_original[LEN_MSG] = {  // Mensagem 3
	0x00006a07, 0xffff96b5, 0x00006a18, 0xffff966d,
	0x000069f7, 0xffff96bc, 0x00006a22, 0xffff96bf,
	0x00006a26, 0xffff966d, 0x000069e0, 0xffff966d,
	0x000069ff, 0xffff96b6, 0x00006a1a, 0xffff96b5,
	0x00006a27, 0xffff966d, 0x00006a00, 0xffff96c6,
	0x000069d3, 0xffff9693, 0x00006a1c, 0xffff96bf,
	0x00006a18, 0xffff966d, 0x000069d3, 0xffff966d,
	0x000069d3, 0xffff966d, 0x000069d3, 0xffff966d,
  0x000069d3, 0x00009e8c, 0x00000010
};*/

/*uint32_t const mensagem_original[LEN_MSG] = {  // Mensagem 4
	0x17d797c5, 0xe82868f7, 0x17d797d6, 0xe82868af,
  0x17d797b6, 0xe82868f0, 0x17d797d8, 0xe82868fb,
  0x17d797d6, 0xe8286902, 0x17d79791, 0xe82868bc,
  0x17d79791, 0xe82868d7, 0x17d797e0, 0xe8286903,
  0x17d797d6, 0xe82868fb, 0x17d79791, 0xe82868d2,
  0x17d797d2, 0xe82868fb, 0x17d797da, 0xe82868f5,
  0x17d797e0, 0xe8286901, 0x17d797df, 0xe82868f8,
  0x17d797d2, 0xe82868af, 0x17d79791, 0xe82868af,
  0x17d79791, 0x23c36329, 0xe828688f
};*/

/*uint32_t const mensagem_original[LEN_MSG] = {  // Mensagem 5
	0xad464fcd, 0x52b9b0e7, 0xad465000, 0x52b9b0d8,
  0xad464ff0, 0x52b9b095, 0xad464fde, 0x52b9b0e5,
  0xad464ffd, 0x52b9b0de, 0xad464ff9, 0x52b9b0dc,
  0xad464ffe, 0x52b9b0e9, 0xad464ff0, 0x52b9b0da,
  0xad464ff9, 0x52b9b095, 0xad464fb8, 0x52b9b095,
  0xad464fcd, 0x52b9b0e4, 0xad464ffd, 0x52b9b0e3,
  0xad464fab, 0x52b9b0e9, 0xad464ffa, 0x52b9b095,
  0xad464fdd, 0x52b9b0ea, 0xad464ff9, 0x52b9b095,
  0xad464fab, 0x03e97750, 0x52b9b075
};*/

uint32_t tempo_inicial;

/*----------------------------------------------------------------------------
 *      Main
 *---------------------------------------------------------------------------*/
int main (void) {
	osKernelInitialize();
	
	tempo_inicial = osKernelSysTick();
	
	#if SIMULACAO == 0 // Em modo de simulacao nao usa o display
		init_display();
	#endif //SIMULACAO

	create_locks();
	
	init_threads();
	
	osKernelStart();
	
	osSignalWait(0x0001, osWaitForever); // Espera terminar o programa

	// Elimina as outras threads, ja que nao vao ser mais usadas
	terminate_threads();
	osDelay(osWaitForever);
}
