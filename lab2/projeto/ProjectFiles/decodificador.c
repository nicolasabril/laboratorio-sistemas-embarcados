/*----------------------------------------------------------------------------
 * Thread 2 'decodificador': Decodifica a mensagem com a chave gerada
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "cmsis_os.h"
#include "decodificador.h"
#include "recursos_compartilhados.h"
#include <stdbool.h>

// Locks de recurso
extern lock_t l_mensagem;
extern lock_t l_chave_teste;

// Locks de execução
extern lock_t l_decodificou;

osThreadId tid_decodificador;                            // thread id
osThreadDef (decodificador, osPriorityNormal, 1, 0);     // thread object
 
void init_decodificador (void)
{
  tid_decodificador = osThreadCreate(osThread(decodificador), NULL);
}
 
void decodificador (void const *argument)
{
  uint32_t *msg;
  uint32_t ultima_chave = 0;
  uint32_t chave_atual;
  uint32_t i;
  while(1)
  {
    get_lock(&l_chave_teste);
    chave_atual = *((uint32_t*)l_chave_teste.data); // Salva uma versão local da chave para que as outras threads possam continuar trabalhando
                                                 // So funciona porque a unica thread que altera a chave é obrigatoriamente anterior
    release_lock(&l_chave_teste);

    if(chave_atual != ultima_chave) // Se ja foi gerada uma chave nova, executa o teste
    {
      ultima_chave = chave_atual; 
      
      get_lock(&l_mensagem);
      msg = (uint32_t*)l_mensagem.data;
      for(i=0; i<LEN_MSG-2; i++)
      {
        if(!(i % 2))
          msg[i] = mensagem_original[i] - chave_atual; // desfazendo a codificacao (tinha sido adicionado)
        else
          msg[i] = mensagem_original[i] + chave_atual; // desfazendo a codificacao (tinha sido subtraido)
      }
      if(!(i % 2))
      {
        msg[i] = mensagem_original[i] + chave_atual; // Por algum motivo as duas ultimas words estao trocadas
        msg[i+1] = mensagem_original[i] - chave_atual;
      }
      else
      {
        msg[i] = mensagem_original[i] - chave_atual;
        msg[i+1] = mensagem_original[i] + chave_atual;
      }
      release_lock(&l_mensagem);

      get_lock(&l_decodificou);
      *((uint8_t*)l_decodificou.data) = true;
      release_lock(&l_decodificou);
    }
    osThreadYield();
  }
}
