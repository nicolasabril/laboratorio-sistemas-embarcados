/*----------------------------------------------------------------------------
 * Thread 6 'printer': Imprime a chave e a mensagem na tela
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef PRINTER_H
#define PRINTER_H

#include "cmsis_os.h"

void printer (void const *argument); // thread function

void init_printer (void);

#endif //PRINTER_H
