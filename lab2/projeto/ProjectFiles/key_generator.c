/*----------------------------------------------------------------------------
 * Thread 1 'key_generator': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "cmsis_os.h" 
#include "key_generator.h"
#include "recursos_compartilhados.h"
#include "primo.h"
#include <stdbool.h>

// Locks de recurso
extern lock_t l_chave_teste;

// Locks de execução
extern lock_t l_precisa_gerar_chave;

osThreadId tid_key_generator;                            // thread id
osThreadDef (key_generator, osPriorityNormal, 1, 0);     // thread object
 
void init_key_generator (void)
{
  tid_key_generator = osThreadCreate(osThread(key_generator), NULL);
}
 
void key_generator (void const *argument)
{
  // Na primeira execução, inicia com valores predefinidos
  #if CHAVES_EM_ORDEM == 1
    #if CHUTE_INTELIGENTE == 1
      get_lock(&l_chave_teste);
      // Inicializa a chave, assumindo que msg[0] pode ser o maior caractere ascii possivel
      if (mensagem_original[0] % 2) // Chave tem que ser impar (para ser primo e ter primo anterior)
        *((uint32_t*)l_chave_teste.data) = mensagem_original[0] - 128;
      else
        *((uint32_t*)l_chave_teste.data) = mensagem_original[0] - 127;
      // Acha o primo anterior a 
      get_lock(&l_primo_anterior);
      *((uint32_t*)l_primo_anterior.data) = acha_primo_anterior(*((uint32_t*)l_chave_teste.data));
      release_lock(&l_primo_anterior);
      release_lock(&l_chave_teste);
    #else
      get_lock(&l_primo_anterior);
      *((uint32_t*)l_primo_anterior.data) = 2;
      release_lock(&l_primo_anterior);
      get_lock(&l_chave_teste);
      *((uint32_t*)l_chave_teste.data) = 3;
      release_lock(&l_chave_teste);
    #endif //CHUTE_INTELIGENTE
  #else
    // TODO: Geração aleatoria/fora de ordem de chaves
  #endif //CHAVES_EM_ORDEM

  get_lock(&l_precisa_gerar_chave);
  *((uint32_t*)l_precisa_gerar_chave.data) = false;
  release_lock(&l_precisa_gerar_chave);

  while(1)
  {
    get_lock(&l_precisa_gerar_chave);
    if(*((uint8_t*)l_precisa_gerar_chave.data) == true)
    {
      get_lock(&l_chave_teste);
      *((uint32_t*)l_chave_teste.data) = gera_prox_chave(*((uint32_t*)l_chave_teste.data));
      *((uint8_t*)l_precisa_gerar_chave.data) = false;
      release_lock(&l_chave_teste);
    }
    release_lock(&l_precisa_gerar_chave);
    osThreadYield();
  }
}

uint32_t gera_prox_chave (uint32_t chave_anterior)
{
  #if CHAVES_EM_ORDEM == 1
    return chave_anterior + 2;
  #else
    // TODO: geracao de chaves fora de ordem
  #endif //CHAVES_EM_ORDEM
}
