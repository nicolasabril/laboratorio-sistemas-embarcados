/*----------------------------------------------------------------------------
 * Thread 3 'testa_primo': Verifica se a chave gerada e primo
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "recursos_compartilhados.h"
#include "testa_primo.h"
#include <stdbool.h>
#include "primo.h"

// Locks de recurso
extern lock_t l_chave_teste;
extern lock_t l_e_primo;

// Locks de execução
extern lock_t l_testou_primo;

osThreadId tid_testa_primo; // thread id
osThreadDef(testa_primo, osPriorityNormal, 1, 0); // thread object
 
void init_testa_primo (void)
{
  tid_testa_primo = osThreadCreate(osThread(testa_primo), NULL);
}
 
void testa_primo (void const *argument)
{
  uint32_t ultima_chave = 0;
  uint32_t chave_atual;

  while(1)
  {
    // Salva uma versão local da chave para que as outras threads possam continuar trabalhando
    // So funciona porque a unica thread que altera a chave é obrigatoriamente anterior
    get_lock(&l_chave_teste);
    chave_atual = *((uint32_t*)l_chave_teste.data); 
    release_lock(&l_chave_teste);

    // So executa o teste se ja foi gerada uma chave nova
    if(chave_atual != ultima_chave) 
    {
      // Verifica se e primo
      ultima_chave = chave_atual;
      get_lock(&l_e_primo);
      *((uint8_t*)l_e_primo.data) = testa_primalidade(chave_atual);
      release_lock(&l_e_primo);

      // Indica que terminou
      get_lock(&l_testou_primo);
      *((uint8_t*)l_testou_primo.data) = true;
      release_lock(&l_testou_primo);
    }

    osThreadYield();
  }
}
