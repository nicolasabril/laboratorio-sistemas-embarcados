/*----------------------------------------------------------------------------
 * Thread 4 'testa_penultima_word': Verifica se a chave e valida com o teste da penultima word
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef TESTA_PENULTIMA_WORD_H
#define TESTA_PENULTIMA_WORD_H

#include "cmsis_os.h"

void testa_penultima_word (void const *argument);               // thread function

void init_testa_penultima_word (void);

#endif
