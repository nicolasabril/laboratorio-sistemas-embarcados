/*----------------------------------------------------------------------------
 * Thread 7 'verificador': Verifica se todos os testes foram realizados e decide se continua ou para
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "cmsis_os.h"   
#include <stdbool.h> 
#include "recursos_compartilhados.h"
#include "verificador.h"
#include "display.h"

// Locks de recurso
extern lock_t l_chave_teste;
extern lock_t l_primo_anterior;
extern lock_t l_e_primo;
extern lock_t l_ultima_word_valida;
extern lock_t l_penultima_word_valida;

// Locks de execução
extern lock_t l_precisa_gerar_chave;
extern lock_t l_testou_primo;
extern lock_t l_testou_ultima_word;
extern lock_t l_testou_penultima_word;
extern lock_t l_decodificou;
extern lock_t l_printou;

osThreadId tid_verificador; // thread id
osThreadDef(verificador, osPriorityNormal, 1, 0); // thread object
 
void init_verificador (osThreadId tid_main)
{ 
  tid_verificador = osThreadCreate (osThread(verificador), tid_main);
}
 
void verificador (const void *tid_main)
{ 
	extern uint32_t tempo_inicial;
  char tempo_decorrido_string [12];
	uint32_t tempo_decorrido;
	uint32_t tempo_1us;
  while (1)
  {
    // Verifica se as threads anteriores acabaram
    get_lock(&l_testou_primo);
    if (*((uint8_t*)l_testou_primo.data) == true)
    {
      get_lock(&l_printou);
      if (*((uint8_t*)l_printou.data) == true)
      {
        get_lock(&l_testou_penultima_word);
        if (*((uint8_t*)l_testou_penultima_word.data) == true)
        {
          get_lock(&l_testou_ultima_word);
          if (*((uint8_t*)l_testou_ultima_word.data) == true)
          {
            // Se passa em todos os testes, acabou
            get_lock(&l_e_primo);
            get_lock(&l_ultima_word_valida);
            get_lock(&l_penultima_word_valida);
            if(*((uint8_t*)l_e_primo.data) == true)
            {
              if(*((uint8_t*)l_ultima_word_valida.data) == true)
              {
                if(*((uint8_t*)l_penultima_word_valida.data) == true)
                {
                  #if SIMULACAO == 0
                    // Printa o tempo decorrido até o fim da execução do programa
                    GrStringDraw(&sContext, "Tempo decorrido us:", -1, 0, (sContext.psFont->ui8Height+2)*5, true);
										tempo_decorrido = osKernelSysTick();
										tempo_1us = osKernelSysTickMicroSec(1);
                    intToString(((tempo_decorrido - tempo_inicial)/(120)), tempo_decorrido_string, 12, 10, 10);
                    GrStringDraw(&sContext, tempo_decorrido_string, -1, 0, (sContext.psFont->ui8Height+2)*6, true);
                  #endif //SIMULACAO
                  // Acorda a main para matar as threads
                  osSignalSet((osThreadId)tid_main, 0x0001); 
                  osDelay(osWaitForever);
                }
              }
            }
            release_lock(&l_e_primo);
            release_lock(&l_ultima_word_valida);
            release_lock(&l_penultima_word_valida);

            // Se nao passou em todos os testes

            // Se a chave testada era primo, atualiza o primo anterior (so se as chaves sao geradas em ordem)
            #if CHAVES_EM_ORDEM == 1
              get_lock(&l_e_primo);
              if(*((uint8_t*)l_e_primo.data) == true) 
              {
                get_lock(&l_chave_teste);
                get_lock(&l_primo_anterior);
                *((uint32_t*)l_primo_anterior.data) = *((uint32_t*)l_chave_teste.data);
                release_lock(&l_primo_anterior);
                release_lock(&l_chave_teste);
              }
              release_lock(&l_e_primo);
            #endif //CHAVES_EM_ORDEM

            // Limpa as travas de execucao (já adquiriu as lock no começo)
            get_lock(&l_precisa_gerar_chave);
            *((uint8_t*)l_precisa_gerar_chave.data) = true;
            *((uint8_t*)l_testou_ultima_word.data) = false;
            *((uint8_t*)l_testou_penultima_word.data) = false;
            *((uint8_t*)l_printou.data) = false;
            *((uint8_t*)l_testou_primo.data) = false;
            *((uint8_t*)l_decodificou.data) = false;
            release_lock(&l_precisa_gerar_chave); // so solta quando limpou tudo, pra ter certeza que vai recomecar tudo zerado
          }
          release_lock(&l_testou_ultima_word);
        }
        release_lock(&l_testou_penultima_word);
      }
      release_lock(&l_printou);
    }
    release_lock(&l_testou_primo);
    osThreadYield();
  }
}
