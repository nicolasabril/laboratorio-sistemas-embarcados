/*----------------------------------------------------------------------------
 * Thread 1 'key_generator': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef KEY_GENERATOR_H
#define KEY_GENERATOR_H

#include "cmsis_os.h"

void key_generator (void const *argument); // thread function

osThreadId Init_key_generator (void);

uint32_t gera_prox_chave (uint32_t chave_anterior);

#endif
