/*----------------------------------------------------------------------------
 * Thread 4 'testa_penultima_word': Verifica se a chave e valida com o teste da penultima word
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "testa_penultima_word.h"
#include "recursos_compartilhados.h"
#include <stdbool.h>

 
// Locks de recurso
extern lock_t l_chave_teste;
extern lock_t l_penultima_word_valida;

// Locks de execução
extern lock_t l_testou_penultima_word;

osThreadId tid_testa_penultima_word; // thread id
osThreadDef(testa_penultima_word, osPriorityNormal, 1, 0); // thread object
 
void init_testa_penultima_word (void)
{
  tid_testa_penultima_word = osThreadCreate (osThread(testa_penultima_word), NULL);
}
 
void testa_penultima_word (void const *argument)
{
	uint32_t ultima_chave = 0;
  uint32_t chave_atual;
  uint32_t penultima_word;

  while(1)
  {
    // Salva uma versão local da chave para que as outras threads possam continuar trabalhando
    // So funciona porque a unica thread que altera a chave é obrigatoriamente anterior
    get_lock(&l_chave_teste);
    chave_atual = *((uint32_t*)l_chave_teste.data); 
    release_lock(&l_chave_teste);

    // So executa o teste se ja foi gerada uma chave nova
    if(chave_atual != ultima_chave)
    {
      ultima_chave = chave_atual;
      
      // Descodifica a word, ao contrario por algum motivo
      if((LEN_MSG-2) %2)
        penultima_word = mensagem_original[LEN_MSG-2] - chave_atual; 
      else
        penultima_word = mensagem_original[LEN_MSG-2] + chave_atual;

      // Verifica se a chave e valida por esse teste
      get_lock(&l_penultima_word_valida);
      if(chave_atual/2 == penultima_word)
        *((uint8_t*)l_penultima_word_valida.data) = true;
      else
        *((uint8_t*)l_penultima_word_valida.data) = false;
      release_lock(&l_penultima_word_valida);

      // Indica que terminou
      get_lock(&l_testou_penultima_word);
      *((uint8_t*)l_testou_penultima_word.data) = true;
      release_lock(&l_testou_penultima_word);
    }
    osThreadYield();
  }
}
