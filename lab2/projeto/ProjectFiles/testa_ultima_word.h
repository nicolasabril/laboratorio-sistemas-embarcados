/*----------------------------------------------------------------------------
 * Thread 5 'testa_ultima_word': fica se a chave e valida com o teste da ultima word
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef TESTA_ULTIMA_WORD_H
#define TESTA_ULTIMA_WORD_H

#include "cmsis_os.h"

void testa_ultima_word (void const *argument);               // thread function

void init_testa_ultima_word (void);

#endif
