/*----------------------------------------------------------------------------
 * Thread 2 'decodificador': Decodifica a mensagem com a chave gerada
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef DECODIFICADOR_H
#define DECODIFICADOR_H

#include "cmsis_os.h"

void decodificador (void const *argument);               // thread function

void init_decodificador (void);

#endif
