/*----------------------------------------------------------------------------
 * Thread 6 'printer': Imprime a chave e a mensagem na tela
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "cmsis_os.h"   
#include <stdbool.h>  
#include "printer.h"
#include "recursos_compartilhados.h"
#include "display.h"

// Locks de execução
extern lock_t l_decodificou;
extern lock_t l_printou;
 
osThreadId tid_printer;                            // thread id
osThreadDef (printer, osPriorityNormal, 1, 0);     // thread object
 
void init_printer (void)
{
  tid_printer = osThreadCreate(osThread(printer), NULL);
}

void printer (void const *argument)
{
	#if SIMULACAO == 0 // Só printa se nao estiver na simulacao
  char chave_string[10];
  int i;
  uint8_t caracter_mensagem;
  GrStringDraw(&sContext,"key: ", -1, 0, (sContext.psFont->ui8Height+2)*0, true);
	GrStringDraw(&sContext,"msg: ", -1, 0, (sContext.psFont->ui8Height+2)*1, true);
	#endif //SIMULACAO

  while (1)
  {
    get_lock(&l_decodificou);
    if(*((uint8_t*)l_decodificou.data) == true)
    {
      get_lock(&l_printou);
      if (*((uint8_t*)l_printou.data) == false)
      {
        #if SIMULACAO == 0 // Só printa se nao estiver na simulacao
          // Printa a chave
          get_lock(&l_chave_teste);
          intToString((*((uint32_t*)l_chave_teste.data)), chave_string, 10, 16, 8);
          GrStringDraw(&sContext, chave_string, -1, (sContext.psFont->ui8MaxWidth)*6, (sContext.psFont->ui8Height+2)*0, true);
          release_lock(&l_chave_teste);

          // Printa a mensagem decodificada
          get_lock(&l_mensagem);
          for (i=0; i < LEN_MSG; i++)
          {
            caracter_mensagem = ((uint32_t*)l_mensagem.data)[i];
            GrStringDraw(&sContext,
                         &caracter_mensagem,
                         1,
                         (sContext.psFont->ui8MaxWidth)*(i%21),
                         (sContext.psFont->ui8Height+2)*(2+i/21), // So cabem 21 caracteres em uma linha
                         true);
          }     
          release_lock(&l_mensagem);
        #endif //SIMULACAO
        *((uint8_t*)l_printou.data) = true;
      }
      release_lock(&l_printou);
    }
    release_lock(&l_decodificou);
    osThreadYield ();                             
  }
}
