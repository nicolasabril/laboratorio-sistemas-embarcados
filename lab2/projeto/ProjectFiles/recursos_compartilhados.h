/*---------------------------------------------------------------------------------
 *                          recursos_compartilhados.h
 *  Header com declaração dos recursos compartilhados e funções para utilizá-los
 *  Os recursos compartilhados (críticos) só devem ser acessados atraves das travas
 *---------------------------------------------------------------------------------*/

#ifndef RECURSOS_COMPARTILHADOS_H
#define RECURSOS_COMPARTILHADOS_H

#include "cmsis_os.h"

/* ----------------------------------------- *
 *            Modos de compilacao
 * ----------------------------------------- */
#define CHAVES_EM_ORDEM 1   // 1 para chaves criadas em ordem, 0 para chaves criadas fora de ordem
#define SIMULACAO 0        // 1 para modo simulacao (nao usa o display)
#define CHUTE_INTELIGENTE 1 // 1 para restringir as possibilidades de chave, 0 para testar todos os impares

/* ----------------------------------------- *
 *  Definicao de lock e funções pra usa-las  
 * ----------------------------------------- */
#define LOCK_STATUS_OK 0 // Operação executada com sucesso
#define LOCK_STATUS_NULL 1 // Lock vazia (não existia ou não tinha recurso)
#define LOCK_STATUS_ALREADY_UNLOCKED 2 // Tentou liberar uma chave que ja estava aberta
#define LOCK_STATUS_ALREADY_LOCKED 3 // Tentou trancar uma chave já trancada pela mesma thread ou destrancar chave de outra thread

typedef struct 
{
    osThreadId tid; // id da thread que está com a lock.
    void *data;
} lock_t;

__inline lock_t create_lock(void *resource)
{
    lock_t new_lock;
    new_lock.tid = NULL;
    new_lock.data = resource;
    return new_lock;
}
__inline uint8_t get_lock(lock_t *lock)
{
    osThreadId current_tid = osThreadGetId(); // tid da task atual, que esta pedindo o lock
    if(lock == NULL) // Se a lock nao existe
        return LOCK_STATUS_NULL;
    if(lock->tid == current_tid) // Se a lock já é da thread
        return LOCK_STATUS_ALREADY_LOCKED;
    
    while(lock->tid != NULL) // Enquanto a thread está ocupada, espera
    {
        osThreadYield();
    }
    lock->tid = current_tid; // Pega a lock
    return LOCK_STATUS_OK;
}
__inline uint8_t release_lock(lock_t *lock)
{
    osThreadId current_tid = osThreadGetId(); // tid da task atual, que esta soltando o lock
    if(lock == NULL) // se o lock nao existe
        return LOCK_STATUS_NULL;
    if(lock->tid == NULL) // se o lock ja esta detravado
        return LOCK_STATUS_ALREADY_UNLOCKED;
    if(lock->tid != current_tid) // se o lock e de outra thread
        return LOCK_STATUS_ALREADY_LOCKED;

    lock->tid = NULL;
    return LOCK_STATUS_OK;
}

/* ----------------------------------------- *
 *                  Mensagem
 * ----------------------------------------- */
#define LEN_MSG 35 // precisa para criar uma copia da mensagem
extern uint32_t const mensagem_original[];

/* ----------------------------------------- *
 *            Declaração das locks
 * ----------------------------------------- */
// Locks de recurso
extern lock_t l_mensagem;
extern lock_t l_chave_teste;
extern lock_t l_primo_anterior;
extern lock_t l_e_primo;
extern lock_t l_ultima_word_valida;
extern lock_t l_penultima_word_valida;

// Locks de execução
extern lock_t l_precisa_gerar_chave;
extern lock_t l_testou_primo;
extern lock_t l_testou_ultima_word;
extern lock_t l_testou_penultima_word;
extern lock_t l_printou;


#endif
