/*----------------------------------------------------------------------------
 * Thread 5 'testa_ultima_word': fica se a chave e valida com o teste da ultima word
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "cmsis_os.h"     
#include <stdbool.h>
#include "testa_ultima_word.h"
#include "recursos_compartilhados.h"
#include "primo.h"

// Locks de recurso
extern lock_t l_chave_teste;
extern lock_t l_primo_anterior;
extern lock_t l_ultima_word_valida;

// Locks de execução
extern lock_t l_testou_ultima_word;

osThreadId tid_testa_ultima_word;                            // thread id
osThreadDef (testa_ultima_word, osPriorityNormal, 1, 0);     // thread object
 
void init_testa_ultima_word (void)
{
  tid_testa_ultima_word = osThreadCreate (osThread(testa_ultima_word), NULL);
}
 
void testa_ultima_word (void const *argument)
{
	uint32_t ultima_chave = 0;
  uint32_t chave_atual;
  uint32_t ultima_word;
  uint32_t primo_anterior;

  while(1)
  {
    // Salva uma versão local da chave para que as outras threads possam continuar trabalhando
    // So funciona porque a unica thread que altera a chave é obrigatoriamente anterior
    get_lock(&l_chave_teste);
    chave_atual = *((uint32_t*)l_chave_teste.data); 
    release_lock(&l_chave_teste);

    // So executa o teste se ja foi gerada uma chave nova
    if(chave_atual != ultima_chave) 
    {
      ultima_chave = chave_atual;
      
      // Decodifica a word, ao contrario por algum motivo
      if((LEN_MSG-1) %2)
        ultima_word = mensagem_original[LEN_MSG-1] - chave_atual; 
      else
        ultima_word = mensagem_original[LEN_MSG-1] + chave_atual;
      
      // Se as chaves sao geradas em ordem, os primos sao armazenados pelo verificados
      // Assim é possivel reaproveitar o teste do testa_primo
      #if CHAVES_EM_ORDEM == 1
        get_lock(&l_primo_anterior);
        primo_anterior = *((uint32_t*)l_primo_anterior.data);
        release_lock(&l_primo_anterior);
      // Se sao geradas fora de ordem e preciso calcular o primo anterior aqui
      #else
        primo_anterior = acha_primo_anterior(chave_atual);
      #endif

      // Realiza o teste
      if(((chave_atual*chave_atual)/primo_anterior) == ultima_word)
      {
        get_lock(&l_ultima_word_valida);
				*((uint8_t*)l_ultima_word_valida.data) = true;
        release_lock(&l_ultima_word_valida);
      }

      // Indica que terminou
      get_lock(&l_testou_ultima_word);
      *((uint8_t*)l_testou_ultima_word.data) = true;
      release_lock(&l_testou_ultima_word);
    }
    osThreadYield();
  }
}
