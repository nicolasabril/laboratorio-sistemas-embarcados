/*----------------------------------------------------------------------------
 * Thread 1 'pacman': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/
 
#include "pacman.h"

ator_t pacman = {
	NONE, //direcao
	{PACMAN_POS_X_START, //pos_x
	PACMAN_POS_Y_START}, //pos_y
	false, //andou
	INATIVO, //estado
	VALOR_PACMAN //tipo
};

osThreadId tid_pacman_thread;                            // thread id
osThreadDef (pacman_thread, osPriorityNormal, 1, 0);     // thread object
 
void init_pacman_thread (void)
{
  tid_pacman_thread = osThreadCreate(osThread(pacman_thread), NULL);
}
 
void pacman_thread (void const *argument)
{
  while(osSignalWait(0x0001, osWaitForever).status == osEventSignal)
  {
    if(estado_jogo == DEMO)
    {
			pacman.estado = INATIVO;
		}
    else
    {
      if (estado_jogo == COMECA_JOGO)
      {
				reseta_pacman();
			}
      else if (estado_jogo == RENASCE)
      {
        reseta_pacman();
      }
      else if (estado_jogo == JOGO)
      {
				// Se acabou a fase
				if (n_pilulas == 0)
				{
					reseta_pacman();
				}
				else
				{
					// Atualiza estado do pacman se comecou ou acabou power pill
					if (cont_power_pill > 0)
						pacman.estado = SUPER;
					else if (pacman.estado == SUPER)
						pacman.estado = NORMAL;						
					
					if (direcao_joystick != NONE && pode_ir(pacman, direcao_joystick))
					{
						pacman.direcao = direcao_joystick;
					}
					if (pode_ir(pacman, pacman.direcao))
					{
						ator_anda(&pacman, pacman.direcao);
					}
					else
					{
						pacman.direcao = NONE;
					}
				}
      }
      else if (estado_jogo == MORRE)
      {;}
      else if (estado_jogo == GAME_OVER)
      {
				pacman.estado = INATIVO;
			}
      else
      {
        while(1)
          ;
      }
    }
		osSignalSet(tid_pilulas_thread, 0x0001);
  }
}

void reseta_pacman ()
{
	pacman.pos.x = PACMAN_POS_X_START;
	pacman.pos.y = PACMAN_POS_Y_START;
	pacman.andou = false;
	pacman.direcao = NONE;
	pacman.estado = NORMAL;
}

