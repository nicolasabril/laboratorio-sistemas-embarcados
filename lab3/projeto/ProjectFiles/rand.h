#ifndef rand_h
#define rand_h

#include "cmsis_os.h"

#define RAND_MAX 65535
uint16_t rand (void);

#endif
