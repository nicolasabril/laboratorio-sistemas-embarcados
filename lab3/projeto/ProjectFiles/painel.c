/*----------------------------------------------------------------------------
 * Thread 1 'painel': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "painel.h"

osThreadId tid_painel_thread;                            // thread id
osThreadDef (painel_thread, osPriorityNormal, 1, 0);     // thread object
 
void init_painel_thread (void)
{
  tid_painel_thread = osThreadCreate(osThread(painel_thread), NULL);
}
 
void painel_thread (void const *argument)
{
  while(osSignalWait(0x0001, osWaitForever).status == osEventSignal)
  {
		if(btn_cor_pb == true && btn_cor_pb_old == false)
		{
			display_colorido = !display_colorido;
			color_flip = true;
		}
		btn_cor_pb_old = btn_cor_pb;

		if (btn_rst)
			comeca = true;
		
		if (estado_jogo == DEMO)
		{
			if (btn_joy)
				comeca = true;
		}
		else
		{
			if (estado_jogo == COMECA_JOGO)
			{
				vidas = 3;
				pontos = 0;
				nivel_jogo = 0;
			}
			else if (estado_jogo == RENASCE)
			{
				
			}
			else if (estado_jogo == JOGO)
			{
				if (n_pilulas == 0)
				{
					if (vidas < MAX_VIDAS)
						vidas++;
					nivel_jogo++;
				}
			}
			else if (estado_jogo == MORRE)
			{
				vidas--;
			}
			else if (estado_jogo == GAME_OVER)
			{

			}
			else
			{
				while(1)
					;
			}
		}
		osSignalSet(tid_interacao_saidas, 0x0001);
  }
}
