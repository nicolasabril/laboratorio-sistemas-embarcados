#ifndef recursos_compartilhados_h
#define recursos_compartilhados_h

#include <stdbool.h>
#include "cmsis_os.h"

#define SIMULACAO 0

#define VALOR_PAREDE_FISICA 1<<0
#define VALOR_PAREDE_SPRITE 1<<1
#define VALOR_PILULA 1<<2
#define VALOR_POWER_PILL 1<<3
#define VALOR_VITAMINA 1<<4
#define VALOR_PACMAN 1<<5
#define VALOR_FANTASMA 1<<6
#define VALOR_PAREDE_FANTASMA 1<<7
#define MAPA_HEIGHT 84
#define MAPA_WIDTH 128
extern uint8_t mapa[MAPA_HEIGHT][MAPA_WIDTH];
extern uint8_t mapa_default[MAPA_HEIGHT][MAPA_WIDTH];

typedef enum {DEMO, COMECA_JOGO, RENASCE, JOGO, MORRE, GAME_OVER} estado_jogo_t;
extern estado_jogo_t estado_jogo;
extern uint8_t nivel_jogo;
extern int8_t vidas;
extern uint16_t pontos;
extern uint16_t n_pilulas;
extern uint16_t cont_power_pill;
extern bool comeca;
extern bool comeu;

#define MAX_VIDAS 5
#define N_FANTASMAS 3
#define DURACAO_POWER_PILL 200

typedef enum {DIREITA=0, ESQUERDA, BAIXO, CIMA, NONE} direcao_t;
typedef enum {PILULA, POWER_PILL, VITAMINA} comida_tipo_t;
typedef enum {SOM_PILULA=0, SOM_SUPER, SOM_MORTE, NENHUM_SOM} tipo_som_t;
extern tipo_som_t som_a_tocar;

extern direcao_t direcao_joystick;
extern bool btn_rst;
extern bool btn_joy;
extern bool btn_cor_pb;
extern bool btn_cor_pb_old;
extern bool display_colorido;
extern bool color_flip;

extern osMutexId mutex_som_id;

extern osThreadId tid_pacman_thread;
extern osThreadId tid_pilulas_thread;
extern osThreadId tid_fantasmas_thread;
extern osThreadId tid_tocador_som;
extern osThreadId tid_painel_thread;
extern osThreadId tid_interacao_entradas;
extern osThreadId tid_interacao_saidas;

#endif //recursos_compartilhados_h
