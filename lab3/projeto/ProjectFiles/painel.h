/*----------------------------------------------------------------------------
 * Thread 1 'painel': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef painel_H
#define painel_H

#include "cmsis_os.h"
#include <stdbool.h>
#include "recursos_compartilhados.h"

void painel_thread (void const *argument); // thread function
void init_painel_thread (void);

#endif
