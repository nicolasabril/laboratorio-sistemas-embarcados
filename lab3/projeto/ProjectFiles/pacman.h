/*----------------------------------------------------------------------------
 * Thread 1 'pacman': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef pacman_H
#define pacman_H

#include "cmsis_os.h"
#include <stdbool.h>
#include "recursos_compartilhados.h"
#include "ator.h"

#define PACMAN_POS_X_START 60
#define PACMAN_POS_Y_START 53

void pacman_thread (void const *argument); // thread function
void init_pacman_thread (void);
void reseta_pacman (void);
#endif
