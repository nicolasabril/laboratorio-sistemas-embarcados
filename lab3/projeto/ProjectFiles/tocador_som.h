/*----------------------------------------------------------------------------
 * Thread 1 'tocador_som': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef tocador_som_H
#define tocador_som_H

#include "cmsis_os.h"
#include "buzzer.h"
#include "recursos_compartilhados.h"

#define N_SONS 3
#define LEN_SOM_PILULA 3
#define LEN_SOM_SUPER 2
#define LEN_SOM_MORTE 20
#define LEN_SOM_MAX 20

void tocador_som (void const *argument); // thread function
void init_tocador_som (void);

extern osThreadId tid_tocador_som;

#endif
