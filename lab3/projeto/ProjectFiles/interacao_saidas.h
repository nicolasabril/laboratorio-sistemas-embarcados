/*----------------------------------------------------------------------------
 * Thread 1 'interacao_saidas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef interacao_saidas_H
#define interacao_saidas_H

#include "cmsis_os.h"
#include <stdbool.h>
#include "recursos_compartilhados.h"
#include "grlib/grlib.h"
#include "cfaf128x128x16.h"
#include "ator.h"

#define COR_PAREDE 						ClrDarkOrange
#define COR_FUNDO 						ClrBlue
#define COR_PACMAN 						ClrYellow
#define COR_FANTASMA_1 				ClrRed
#define COR_FANTASMA_2 				ClrPurple
#define COR_FANTASMA_3 				ClrPink
#define COR_FANTASMA_FUGINDO 	ClrTeal
#define COR_PILULA 						ClrDarkOrange
#define COR_POWER_PILL 				ClrLime
#define COR_VITAMINA_INTERNO 	ClrOrange
#define COR_VITAMINA_EXTERNO 	ClrDarkOrange
#define COR_PAINEL 						ClrDarkGreen
#define COR_TEXTO							ClrBlack
#define COR_VIDA 							COR_PAINEL

#define CUTSCENE_RENASCE 0
#define CUTSCENE_MORTE 1

#define PAINEL_POS_Y MAPA_HEIGHT + 5
#define PAINEL_HEIGHT 20
#define VIDAS_POS_Y PAINEL_POS_Y + PAINEL_HEIGHT + 5
#define VIDAS_POS_X_BASE 10
#define VIDAS_SIZE 5
#define PONTOS_POS_X_BASE 80
#define PONTOS_POS_Y PAINEL_POS_Y + 2
#define TEXTO_DEMO_POS_Y PONTOS_POS_Y

extern ator_t pacman;
extern ator_t fantasma[N_FANTASMAS];

void interacao_saidas (void const *argument); // thread function
void init_interacao_saidas (void);

void limpa_modificacoes(void);
void limpa_movimento_ator(ator_t const ator);
void desenha_labirinto(void);
void desenha_pilulas(void);
void desenha_ator(ator_t const ator, uint8_t const sprite[ATOR_HEIGHT][ATOR_WIDTH], uint32_t const cor);
void desenha_pacman(void);
void desenha_fantasmas(void);
void desenha_painel(void);
void atualiza_painel(void);
void decide_qual_som_tocar(void);
void play_cutscene(uint8_t const nome_cutscene);
void intToString(int64_t value, char * pBuf, uint32_t len, uint32_t base, uint8_t zeros);
void limpa_campo (void);
void redraw (void);

#endif
