/*----------------------------------------------------------------------------
 * Thread 1 'interacao_entradas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef interacao_entradas_H
#define interacao_entradas_H

#include "cmsis_os.h"
#include <stdbool.h>
#include "recursos_compartilhados.h"
#include "joy.h"
#include "buttons.h"

void interacao_entradas (void const *argument); // thread function
void init_interacao_entradas (void);

#endif
