#ifndef ator_h
#define ator_h

#include <stdbool.h>
#include "cmsis_os.h"
#include "recursos_compartilhados.h"
#include <stdlib.h>

#define ATOR_HEIGHT 8
#define ATOR_WIDTH 8

typedef struct
{
	uint8_t x;
	uint8_t y;
} pos_t;

typedef enum {NORMAL, SUPER, MORTO, INATIVO} ator_estado_t;

typedef struct
{
	direcao_t direcao;
	pos_t pos;
	bool andou;
	ator_estado_t estado;
	uint8_t tipo;
} ator_t;

// True se existe um ponto de valor tipo_obj no mapa dentro de seu corpo
// O corpo tem dimensoes (ATOR_WIDTH, ATOR_HEIGHT)
bool ator_colide (uint8_t const tipo_obj, ator_t const ator, pos_t *pos_colisao);

// True se os dois atores colidem, false caso contrario
bool atores_colidem (ator_t const ator1, ator_t const ator2);

// Anda o ator em uma direcao INCONDICIONALMENTE
void ator_anda (ator_t *ator, direcao_t const direcao);

// True se o ator pode andar na direcao, false caso contrario
bool pode_ir (ator_t const ator, direcao_t const direcao);

// Diz quais posicoes o ator pode andar
// Retorna um byte, onde um bit k � 1(um) se a direcao (direcao_t)k for um movimento possivel
uint8_t movimentos_possiveis (ator_t const ator);

// Anda o ator em direcao a posicao especificada
void vai_a(pos_t const pos, ator_t *ator);

// Retorna a direcao contraria a dir (CIMA->BAIXO, ESQUERDA->DIREITA)
direcao_t direcao_contraria (direcao_t const dir);

#endif //ator_h
