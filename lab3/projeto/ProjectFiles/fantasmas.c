/*----------------------------------------------------------------------------
 * Thread 1 'fantasmas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "fantasmas.h"

ator_t fantasma[3] = {
	{
		DIREITA,
		{FANTASMA_POS_X_START,
		FANTASMA_POS_Y_START},
		false,
		NORMAL,
		VALOR_FANTASMA
	},
	{
		DIREITA,
		{FANTASMA_POS_X_START,
		FANTASMA_POS_Y_START},
		false,
		NORMAL,
		VALOR_FANTASMA
	},
	{
		DIREITA,
		{FANTASMA_POS_X_START,
		FANTASMA_POS_Y_START},
		false,
		NORMAL,
		VALOR_FANTASMA
	}
};

pos_t const pos_inicial_fantasmas = {FANTASMA_POS_X_START, FANTASMA_POS_Y_START};

osThreadId tid_fantasmas_thread;                            // thread id
osThreadDef (fantasmas_thread, osPriorityNormal, 1, 0);     // thread object
 
void init_fantasmas_thread (void)
{
  tid_fantasmas_thread = osThreadCreate(osThread(fantasmas_thread), NULL);
}
 
void fantasmas_thread (void const *argument)
{
	uint8_t i;
	uint8_t cont_atraso_fantasmas = nivel_jogo;
	reseta_fantasmas();
  while(osSignalWait(0x0001, osWaitForever).status == osEventSignal)
  {
    if(estado_jogo == DEMO)
    {
			move_fantasmas();
    }
    else if (estado_jogo == COMECA_JOGO)
		{
			reseta_fantasmas();
		}
		else if (estado_jogo == RENASCE)
		{
			reseta_fantasmas();
		}
		else if (estado_jogo == JOGO)
		{
			// Se acabou o level
			if (n_pilulas == 0)
			{
				reseta_fantasmas();
			}
			else
			{
				// Atualiza estado dos fantasmas
				for (i=0; i<N_FANTASMAS; i++)
				{
					if (fantasma[i].estado == NORMAL && cont_power_pill > 0)
						fantasma[i].estado = SUPER;
					else if (fantasma[i].estado == SUPER && cont_power_pill == 0)
						fantasma[i].estado = NORMAL;
					else if (fantasma[i].estado == MORTO && fantasma[i].pos.x == FANTASMA_POS_X_START && fantasma[i].pos.y == FANTASMA_POS_Y_START)
					{
						if (cont_power_pill > 0)
							fantasma[i].estado = SUPER;
						else
							fantasma[i].estado = NORMAL;
					}
				}
				
				if (cont_atraso_fantasmas == FANTASMA_ATRASO)
				{
					cont_atraso_fantasmas = nivel_jogo;
				}
				else
				{
					cont_atraso_fantasmas++;
					move_fantasmas();
				}
				// Se o pacman encostou em um fantasma
				for (i=0; i<N_FANTASMAS; i++)
				{
					if(atores_colidem(pacman, fantasma[i]))
					{
						if (fantasma[i].estado == NORMAL)
							pacman.estado = MORTO;
						else if (fantasma[i].estado == SUPER)
						{
							fantasma[i].estado = MORTO;
							pontos += 40;
						}
					}
				}
			}
		}
		else if (estado_jogo == MORRE)
		{;}
		else if (estado_jogo == GAME_OVER)
		{
			move_fantasmas();
		}
		else
		{
			while(1)
				;
		}
		osSignalSet(tid_painel_thread, 0x0001);
  }
}

void move_fantasmas (void)
{
	uint8_t i;
	int num_aleatorio;
	pos_t pos_aleatoria;
	
	// Move cada um dos fantasmas
	for (i = 0; i < N_FANTASMAS; i++)
	{
		num_aleatorio = rand();
		//FANTASMA_PROB_ERRAR por cento de chance de ir a um lugar aleatorio
		// Se esta fugindo do pacman, em DEMO ou GAME_OVER, tambem anda aleatoriamente
		if ((fantasma[i].estado == NORMAL && num_aleatorio%100 < FANTASMA_PROB_ERRAR) ||
				 fantasma[i].estado == SUPER ||
				 estado_jogo == DEMO ||
				 estado_jogo == GAME_OVER)
		{
			pos_aleatoria.x = num_aleatorio % MAPA_WIDTH;
			pos_aleatoria.y = num_aleatorio % MAPA_HEIGHT;
			vai_a(pos_aleatoria, &fantasma[i]);
		}
		// Vai atras do pacman
		else if (fantasma[i].estado == NORMAL)
		{
			vai_a(pacman.pos, &fantasma[i]);			
		}			
		// Se ta morto, vai ate o comeco para renascer
		else if (fantasma[i].estado == MORTO)
		{
			vai_a(pos_inicial_fantasmas, &fantasma[i]);
		}
	}
}

void reseta_fantasmas (void)
{
	int i;
	for(i = 0; i < N_FANTASMAS; i++)
	{
		fantasma[i].pos = pos_inicial_fantasmas;
		fantasma[i].estado = NORMAL;
		fantasma[i].andou = false;
		fantasma[i].direcao = DIREITA;
	}
}

