/*----------------------------------------------------------------------------
 * Thread 1 'pilulas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "pilulas.h"

osThreadId tid_pilulas_thread;                            // thread id
osThreadDef (pilulas_thread, osPriorityNormal, 1, 0);     // thread object
 
void init_pilulas_thread (void)
{
  tid_pilulas_thread = osThreadCreate(osThread(pilulas_thread), NULL);
}
 
void pilulas_thread (void const *argument)
{
	pos_t pos_colisao;
  coloca_pilulas();
  while(osSignalWait(0x0001, osWaitForever).status == osEventSignal)
  {
    if(estado_jogo == DEMO)
    {;}
		else if (estado_jogo == COMECA_JOGO)
		{
			coloca_pilulas();
		}
		else if (estado_jogo == RENASCE)
		{;}
		else if (estado_jogo == JOGO)
		{
			comeu = false;
			
			if (n_pilulas == 0)
			{
				coloca_pilulas();
				cont_power_pill = 0;
			}
			if (cont_power_pill > 0)
				cont_power_pill--;
			
			if (ator_colide(VALOR_PILULA|VALOR_POWER_PILL|VALOR_VITAMINA, pacman, &pos_colisao))
			{
				come_pilula(pos_colisao);
			}
			if (rand()%1000 < VITAMINA_PROB_BASE*nivel_jogo)
			{
				coloca_vitamina();
			}
		}
		else if (estado_jogo == MORRE)
		{;}
		else if (estado_jogo == GAME_OVER)
		{;}
		else
		{
			while(1)
				;
		}
		osSignalSet(tid_fantasmas_thread, 0x0001);
  }
}

void coloca_vitamina (void)
{
	mapa[VITAMINA_POS_Y][VITAMINA_POS_X] |= VALOR_VITAMINA;
}

void coloca_pilulas (void)
{
	uint8_t i, j;
	n_pilulas = 0;
	for (i = 0; i < MAPA_HEIGHT; i++)
	{
		for (j = 0; j < MAPA_WIDTH; j++)
		{
			if (mapa_default[i][j] & VALOR_PILULA)
			{
				mapa[i][j] |= VALOR_PILULA;
				n_pilulas++;
			}
			else if (mapa_default[i][j] & VALOR_POWER_PILL)
			{
				mapa[i][j] |= VALOR_POWER_PILL;
				n_pilulas++;
			}
		}
	}
}

void come_pilula (pos_t const pos)
{
	if (mapa[pos.y][pos.x] & VALOR_PILULA)
	{
		mapa[pos.y][pos.x] &= ~((uint8_t)VALOR_PILULA);
		pontos += 10;
		n_pilulas -= 1;
	}
	if (mapa[pos.y][pos.x] & VALOR_POWER_PILL)
	{
		mapa[pos.y][pos.x] &= ~((uint8_t)VALOR_POWER_PILL);
		n_pilulas -= 1;
		cont_power_pill = DURACAO_POWER_PILL;
	}
	if (mapa[pos.y][pos.x] & VALOR_VITAMINA)
	{
		mapa[pos.y][pos.x] &= ~((uint8_t)VALOR_VITAMINA);
		pontos += 100;
	}
	comeu = true;
}
