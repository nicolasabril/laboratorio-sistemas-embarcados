/*----------------------------------------------------------------------------
 * Thread 1 'interacao_saidas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "interacao_saidas.h"

extern tContext sContext;

osThreadId tid_interacao_saidas;                            // thread id
osThreadDef (interacao_saidas, osPriorityNormal, 1, 0);     // thread object
 
 uint8_t const sprites_pacman[1][ATOR_HEIGHT][ATOR_WIDTH] = 
{
	{
		{ 0, 0, 1, 1, 1, 1, 0, 0 },
		{ 0, 1, 1, 1, 0, 1, 1, 0 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 0, 0, 0, 0 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 0, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 0, 1, 1, 1, 1, 0, 0 }
	}
};

 uint8_t const sprites_fantasma[2][ATOR_HEIGHT][ATOR_WIDTH] = 
{
	{
		{ 0, 0, 1, 1, 1, 1, 0, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 0 },
		{ 1, 0, 1, 1, 1, 0, 1, 1 },
		{ 1, 0, 0, 1, 1, 0, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0 }
	},
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 1, 1, 1, 0, 0, 1, 1, 1 },
		{ 1, 0, 1, 0, 0, 1, 0, 1 },
		{ 1, 1, 1, 0, 0, 1, 1, 1 },
		{ 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0 }
	}
};
 
uint16_t pontos_old = 0xFFFF;
uint16_t vidas_old = 0xFF;

uint32_t const cor_fantasmas [3] = {COR_FANTASMA_1, COR_FANTASMA_2, COR_FANTASMA_3};

void init_interacao_saidas (void)
{
  tid_interacao_saidas = osThreadCreate(osThread(interacao_saidas), NULL);
}
 
void interacao_saidas (void const *argument)
{
	bool terminou_cutscene = true;
		
	#if SIMULACAO == 0
	desenha_labirinto();
	desenha_painel();
	#endif
	
  while (osSignalWait(0x0001, osWaitForever).status == osEventSignal)
  {
		#if SIMULACAO == 0
		if (color_flip)
		{
			redraw();
			color_flip = false;
		}
		
		limpa_modificacoes();
    desenha_pilulas();
    desenha_pacman();
    desenha_fantasmas();
    atualiza_painel();
    decide_qual_som_tocar(); 
		
		// Atualiza o estado do jogo
		if (comeca)
		{
			limpa_campo();
			estado_jogo = COMECA_JOGO;
			comeca = false;
		}
		else
		{
			if (estado_jogo == DEMO)
			{;}
			else if (estado_jogo == COMECA_JOGO)
			{
				estado_jogo = RENASCE;
			}
			else if (estado_jogo == RENASCE)
			{
				if (terminou_cutscene)
				{
					estado_jogo = JOGO;
				}
				else
				{
					play_cutscene(CUTSCENE_RENASCE);
				}
			}
			else if (estado_jogo == JOGO)
			{
				if (n_pilulas == 0)
					redraw();
				else if (pacman.estado == MORTO)
				{
					estado_jogo = MORRE;
				}
			}
			else if (estado_jogo == MORRE)
			{
				play_cutscene(CUTSCENE_MORTE);
				if (vidas < 0)
				{
					estado_jogo = GAME_OVER;
				}
				else
				{
					estado_jogo = RENASCE;
				}
				limpa_campo();
			}
			else if (estado_jogo == GAME_OVER)
			{
			}
		}
		#endif
  }
}

void desenha_labirinto(void)
{
  uint8_t i,j;
	int16_t x0, x1;
	bool draw_wall;
	bool trocou;
  for (i=0; i < MAPA_HEIGHT; i++)
  {
		// Comeca cada linha assumindo que o primeiro pixel e parede
		draw_wall = true;
		trocou = false;
		x0 = 0; // Limite inferior do bloco a ser printada
		x1 = -1; // Limite superior do bloco a ser printada
		
    for (j=0; j < MAPA_WIDTH; j++)
    {
			// Se o pixel do mapa tem sprite de parede
      if (mapa[i][j] & VALOR_PAREDE_SPRITE)
      {
				// Se o mapa trocou de fundo para parede
				if (draw_wall == false)
				{
					draw_wall = true;
					trocou = true;
					// Seta a cor para o que tem que ser printado
					if (display_colorido)
						GrContextForegroundSet(&sContext, COR_FUNDO);
					else
						GrContextForegroundSet(&sContext, ClrBlack);
				}
      }
			// Se nao tem sprite de parede
      else
      {
				// Se o mapa trocou de parede para fundo
				if (draw_wall == true)
				{
					draw_wall = false;
					trocou = true;
					if (display_colorido)
						GrContextForegroundSet(&sContext, COR_PAREDE);
					else
						GrContextForegroundSet(&sContext, ClrWhite);
				}
      }
			
			// Se trocou de parede para fundo ou o contrario, printa do bloco encontrada
			if (trocou)
			{
				if (x1 >= x0)
					GrLineDrawH(&sContext, x0, x1, i);
				trocou = false;
				x0 = j;
				x1 = j-1;
			}
			x1++;
    }
		// Printa o ultimo bloco
		if (draw_wall == true)
		{
			if (display_colorido)
				GrContextForegroundSet(&sContext, COR_PAREDE);
			else
				GrContextForegroundSet(&sContext, ClrWhite);
		}
		else
		{
			if (display_colorido)
				GrContextForegroundSet(&sContext, COR_FUNDO);
			else
				GrContextForegroundSet(&sContext, ClrBlack);
		}
		GrLineDrawH(&sContext, x0, x1, i);
  }
}
void desenha_pilulas(void)
{
	int i, j;
	for (i = 0; i < MAPA_HEIGHT; i++)
	{
		for (j=0; j < MAPA_WIDTH; j++)
		{
			if (mapa[i][j] & VALOR_PILULA)
			{
				if (display_colorido)
					GrContextForegroundSet(&sContext, COR_PILULA);
				else
					GrContextForegroundSet(&sContext, ClrWhite);
				GrLineDrawH(&sContext, j-1, j+2, i);
			}
			else if (mapa[i][j] & VALOR_POWER_PILL)
			{
				if (display_colorido)
					GrContextForegroundSet(&sContext, COR_POWER_PILL);
				else
					GrContextForegroundSet(&sContext, ClrWhite);
				GrLineDrawH(&sContext, j-1, j+2, i-1);
				GrLineDrawH(&sContext, j-1, j+2, i);
				GrLineDrawH(&sContext, j-1, j+2, i+1);
				GrLineDrawH(&sContext, j-1, j+2, i+2);
			}
			else if (mapa[i][j] & VALOR_VITAMINA)
			{
				// Parte de fora da vitamina
				if (display_colorido)
					GrContextForegroundSet(&sContext, COR_VITAMINA_EXTERNO);
				else
					GrContextForegroundSet(&sContext, ClrWhite);
				GrLineDrawH(&sContext, j-3, j+4, i-2);
				GrLineDrawH(&sContext, j-3, j+4, i-1);
				GrLineDrawH(&sContext, j-3, j+4, i);
				GrLineDrawH(&sContext, j-3, j+4, i+1);
				GrLineDrawH(&sContext, j-3, j+4, i+2);
				GrLineDrawH(&sContext, j-3, j+4, i+3);
				// Parte de dentro
				if (display_colorido)
					GrContextForegroundSet(&sContext, COR_VITAMINA_INTERNO);
				else
					GrContextForegroundSet(&sContext, ClrWhite);
				GrLineDrawH(&sContext, j-1, j+2, i);
				GrLineDrawH(&sContext, j-1, j+2, i+1);
			}
		}
	}
}
void desenha_ator(ator_t const ator, uint8_t const sprite[ATOR_HEIGHT][ATOR_WIDTH], uint32_t const cor)
{
	uint8_t i,j;
	int16_t x0, x1;
	bool desenhando_ator;
	bool trocou;
	uint32_t cor_do_ator;
	uint32_t cor_do_fundo = display_colorido ? COR_FUNDO : ClrBlack;
	if (display_colorido)
		cor_do_ator = cor;
	else
	{
		if (cor == COR_FUNDO)
			cor_do_ator = ClrBlack;
		else
			cor_do_ator = ClrWhite;
	}
	GrContextForegroundSet(&sContext, cor);
	for (i=0; i < ATOR_HEIGHT; i++)
  {
		x0 = 0; // Limite inferior do bloco a ser printada
		x1 = -1; // Limite superior do bloco a ser printada
		desenhando_ator = false;
		trocou = false;
		
    for (j=0; j < ATOR_WIDTH; j++)
    {
      if (sprite[i][j])
      {
				if (!desenhando_ator)
				{
					trocou = true;
				}
      }
      else
      {
				if (desenhando_ator)
				{
					trocou = true;
				}
      }
			if (trocou)
			{
				if (x1 >= x0)
					if(ator.pos.y + i < MAPA_HEIGHT)
					{
						if (desenhando_ator)
						{
							GrContextForegroundSet(&sContext, cor_do_ator);
						}
						else
						{
							GrContextForegroundSet(&sContext, cor_do_fundo);
						}
						GrLineDrawH(&sContext, ator.pos.x + x0, ator.pos.x + x1, ator.pos.y + i);
					}
				trocou = false;
				x0 = j;
				x1 = j-1;
				desenhando_ator = sprite[i][j]? true : false;
			}
			x1++;
    }
		if(ator.pos.y + i < MAPA_HEIGHT)
		{
			if (desenhando_ator)
			{
				GrContextForegroundSet(&sContext, cor_do_ator);
			}
			else
			{
				GrContextForegroundSet(&sContext, cor_do_fundo);
			}
			GrLineDrawH(&sContext, ator.pos.x + x0, ator.pos.x + x1, ator.pos.y + i);
		}
  }
}

void desenha_pacman(void)
{
	if (pacman.estado == MORTO)
		desenha_ator(pacman, sprites_pacman[0], COR_FUNDO);
	else if (pacman.estado == NORMAL || pacman.estado == SUPER)
		desenha_ator(pacman, sprites_pacman[0], COR_PACMAN);
}

void desenha_fantasmas(void)
{
	uint8_t i;
	for (i=0; i<N_FANTASMAS; i++)
	{
		if (fantasma[i].estado == NORMAL)
			desenha_ator(fantasma[i], sprites_fantasma[0], cor_fantasmas[i]); 
		else if (fantasma[i].estado == SUPER)
			desenha_ator(fantasma[i], sprites_fantasma[0], COR_FANTASMA_FUGINDO); 
		else if (fantasma[i].estado == MORTO)
			desenha_ator(fantasma[i], sprites_fantasma[1], COR_FANTASMA_FUGINDO);
	}
}

void desenha_painel(void)
{
	tRectangle rect;
	rect.i16XMax = MAPA_WIDTH;
	rect.i16XMin = 0;
	rect.i16YMin = PAINEL_POS_Y;
	rect.i16YMax = PAINEL_POS_Y + PAINEL_HEIGHT;
	if (display_colorido)
		GrContextForegroundSet(&sContext, COR_PAINEL);
	else
		GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &rect);
}

void atualiza_painel(void)
{
	char buffer_pontos[6];
	tRectangle aux_rect;
	uint8_t i;
	uint32_t cor_do_painel = display_colorido ? COR_PAINEL : ClrWhite;
	if (estado_jogo != DEMO)
	{
		// Se mudou os pontos, atualiza
		if (pontos != pontos_old)
		{
			pontos_old = pontos;
			aux_rect.i16XMin = PONTOS_POS_X_BASE;
			aux_rect.i16XMax = aux_rect.i16XMin + (sContext.psFont->ui8MaxWidth)*5;
			aux_rect.i16YMin = PONTOS_POS_Y;
			aux_rect.i16YMax = aux_rect.i16YMin + (sContext.psFont->ui8Height);
			GrContextForegroundSet(&sContext, cor_do_painel);
			GrRectFill(&sContext, &aux_rect);
			intToString(pontos, buffer_pontos, 5, 10, 0);
			GrContextBackgroundSet(&sContext, cor_do_painel);
			GrContextForegroundSet(&sContext, COR_TEXTO);
			GrStringDraw(&sContext, buffer_pontos, -1, PONTOS_POS_X_BASE, PONTOS_POS_Y, 0);
		}
		// Se mudaram as vidas, atualiza
		if (vidas != vidas_old)
		{
			vidas_old = vidas;
			for (i=1; i<MAX_VIDAS; i++)
			{
				aux_rect.i16XMin = VIDAS_POS_X_BASE + 2*VIDAS_SIZE*i;
				aux_rect.i16XMax = aux_rect.i16XMin + VIDAS_SIZE;
				aux_rect.i16YMin = VIDAS_POS_Y;
				aux_rect.i16YMax = aux_rect.i16YMin + VIDAS_SIZE;
				if(i <= vidas)
				{
					if (display_colorido)
						GrContextForegroundSet(&sContext, COR_VIDA);
					else
						GrContextForegroundSet(&sContext, ClrWhite);
				}
				else
					GrContextForegroundSet(&sContext, ClrBlack);
				GrRectFill(&sContext, &aux_rect);
			}
		}
	}
}

void decide_qual_som_tocar(void)
{
	osMutexWait(mutex_som_id, osWaitForever);
	if(pacman.estado == MORTO)
		som_a_tocar = SOM_MORTE;
	else if (comeu)
		som_a_tocar = SOM_PILULA;
	else if (cont_power_pill > 0)
		som_a_tocar = SOM_SUPER;
	osMutexRelease(mutex_som_id);
}
void play_cutscene(uint8_t const nome_cutscene)
{
	
}

void limpa_modificacoes (void)
{
	uint8_t i;
	limpa_movimento_ator(pacman);
	for (i = 0; i < N_FANTASMAS; i++)
		limpa_movimento_ator(fantasma[i]);
}

void limpa_movimento_ator(ator_t const ator)
{
	uint32_t cor_do_fundo = display_colorido ? COR_FUNDO : ClrBlack;
	uint32_t cor_da_parede = display_colorido ? COR_PAREDE : ClrWhite;
	switch (ator.direcao)
	{
		case CIMA:
			// Caso atravesse de cima para baixo, vai sumindo aos poucos de cima
			if(ator.pos.y > MAPA_HEIGHT-ATOR_HEIGHT-1)
			{
				GrContextForegroundSet(&sContext, cor_do_fundo);
				GrLineDrawH(&sContext, ator.pos.x, ator.pos.x+ATOR_WIDTH-1, ator.pos.y-MAPA_HEIGHT+ATOR_HEIGHT);
			}
			else
			{
				if (mapa[ator.pos.y+ATOR_HEIGHT][ator.pos.x] & VALOR_PAREDE_SPRITE)
					GrContextForegroundSet(&sContext, cor_da_parede);
				else
					GrContextForegroundSet(&sContext, cor_do_fundo);
				GrLineDrawH(&sContext, ator.pos.x, ator.pos.x+ATOR_WIDTH-1, ator.pos.y+ATOR_HEIGHT);
			}
			break;
		case BAIXO:
			if (ator.pos.y > 0)
			{
				if (mapa[ator.pos.y-1][ator.pos.x] & VALOR_PAREDE_SPRITE)
					GrContextForegroundSet(&sContext, cor_da_parede);
				else
					GrContextForegroundSet(&sContext, cor_do_fundo);
				GrLineDrawH(&sContext, ator.pos.x, ator.pos.x+ATOR_WIDTH-1, ator.pos.y-1);
			}
			else
			{
				GrContextForegroundSet(&sContext, cor_do_fundo);
				GrLineDrawH(&sContext, ator.pos.x, ator.pos.x+ATOR_WIDTH-1, MAPA_HEIGHT-1);
			}
			break;
		case ESQUERDA:
			if (mapa[ator.pos.y][ator.pos.x+ATOR_WIDTH] & VALOR_PAREDE_SPRITE)
				GrContextForegroundSet(&sContext, cor_da_parede);
			else
				GrContextForegroundSet(&sContext, cor_do_fundo);
			GrLineDrawV(&sContext, ator.pos.x+ATOR_WIDTH, ator.pos.y, ator.pos.y+ATOR_HEIGHT-1);
			break;
		case DIREITA:
			if (mapa[ator.pos.y][ator.pos.x-1] & VALOR_PAREDE_SPRITE)
				GrContextForegroundSet(&sContext, cor_da_parede);
			else
				GrContextForegroundSet(&sContext, cor_do_fundo);
			GrLineDrawV(&sContext, ator.pos.x-1, ator.pos.y, ator.pos.y+ATOR_HEIGHT-1);
			break;
		default:
			break;
	}
}

void intToString(int64_t value, char * pBuf, uint32_t len, uint32_t base, uint8_t zeros)
{
	static const char* pAscii = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	bool n = false;
	int pos = 0, d = 0;
	int64_t tmpValue = value;

	// the buffer must not be null and at least have a length of 2 to handle one
	// digit and null-terminator
	if (pBuf == NULL || len < 2)
			return;
	// a valid base cannot be less than 2 or larger than 36
	// a base value of 2 means binary representation. A value of 1 would mean only zeros
	// a base larger than 36 can only be used if a larger alphabet were used.
	if (base < 2 || base > 36)
			return;
	if (zeros > len)
		return;
	// negative value
	if (value < 0)
	{
			tmpValue = -tmpValue;
			value    = -value;
			pBuf[pos++] = '-';
			n = true;
	}
	// calculate the required length of the buffer
	do {
			pos++;
			tmpValue /= base;
	} while(tmpValue > 0);
	if (pos > len)
			// the len parameter is invalid.
			return;
	if(zeros > pos){
		pBuf[zeros] = '\0';
		do{
			pBuf[d++ + (n ? 1 : 0)] = pAscii[0]; 
		}
		while(zeros > d + pos);
	}
	else
		pBuf[pos] = '\0';
	pos += d;
	do {
		pBuf[--pos] = pAscii[value % base];
		value /= base;
	} while(value > 0);
}

void limpa_campo (void)
{
	uint8_t i;
	desenha_ator(pacman, sprites_pacman[0], COR_FUNDO);
	for (i=0; i<N_FANTASMAS; i++)
	{
		desenha_ator(fantasma[i], sprites_fantasma[0], COR_FUNDO);
	}
}

void redraw (void)
{
	desenha_labirinto();
	desenha_painel();
	desenha_pilulas();
	vidas_old = MAX_VIDAS + 1;
	pontos_old = 0xFFFE;
	atualiza_painel();
	desenha_pacman();
	desenha_fantasmas();
}
