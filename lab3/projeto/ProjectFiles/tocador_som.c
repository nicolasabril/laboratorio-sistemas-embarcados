/*----------------------------------------------------------------------------
 * Thread 1 'tocador_som': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "tocador_som.h"

// Quantidade de frames de som com 'sound_refresh_period_ms' de duracao que dura cada um dos sons
uint16_t const len_som[N_SONS] = {LEN_SOM_PILULA, LEN_SOM_SUPER, LEN_SOM_MORTE};

// Sons como sequencia de periodos do pwm
uint16_t const som[N_SONS][LEN_SOM_MAX] = 
{
	{10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000},
	{3000, 3000, 0, 0, 0, 0, 0, 0, 0, 0},
	{1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000}
};

tipo_som_t som_tocando = NENHUM_SOM;
tipo_som_t som_a_tocar = NENHUM_SOM;
uint8_t som_tocando_index = 0;

osThreadId tid_tocador_som;                            // thread id
osThreadDef (tocador_som, osPriorityNormal, 1, 0);     // thread object
 
void init_tocador_som (void)
{
  tid_tocador_som = osThreadCreate(osThread(tocador_som), NULL);
}
 
void tocador_som (void const *argument)
{
	#if SIMULACAO == 0
	buzzer_vol_set(0x8000);
	buzzer_write(false);
	#endif
	
	while (osSignalWait(0x0001, osWaitForever).status == osEventSignal)
	{
		#if SIMULACAO == 0
		osMutexWait(mutex_som_id, osWaitForever);
		if (som_a_tocar != NENHUM_SOM)
		{
			som_tocando = som_a_tocar;
			som_tocando_index = 0;
			som_a_tocar = NENHUM_SOM;
		}
		osMutexRelease(mutex_som_id);
		
		if (som_tocando_index < len_som[som_tocando] && som_tocando != NENHUM_SOM)
		{
			if (som_tocando_index == 0 || (som_tocando_index > 0 && som[som_tocando][som_tocando_index-1] != som[som_tocando][som_tocando_index]))
			{
				buzzer_per_set(som[som_tocando][som_tocando_index]);
				buzzer_write(true);
			}
			som_tocando_index++;
		}
		else
		{
			som_tocando = NENHUM_SOM;
			buzzer_write(false);
		}
		#endif
	}
}
