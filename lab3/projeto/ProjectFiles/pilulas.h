/*----------------------------------------------------------------------------
 * Thread 1 'pilulas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef pilulas_H
#define pilulas_H

#include "cmsis_os.h"
#include <stdbool.h>
#include "recursos_compartilhados.h"
#include <stdlib.h>
#include "ator.h"

#define VITAMINA_POS_Y 46
#define VITAMINA_POS_X 63
#define VITAMINA_PROB_BASE 10

extern ator_t pacman;

void pilulas_thread (void const *argument); // thread function
void init_pilulas_thread (void);
void coloca_vitamina (void);
void come_pilula (pos_t const pos);
void coloca_pilulas (void);

#endif
