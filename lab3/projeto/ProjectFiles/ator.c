#include "ator.h"

bool ator_colide (uint8_t const tipo_obj, ator_t const ator, pos_t *pos_colisao)
{
	uint8_t i,j;
	for (i=0; i<ATOR_HEIGHT; i++)
	{
		for (j=0; j<ATOR_WIDTH; j++)
		{
			if (mapa[ator.pos.y+i][ator.pos.x+j] & tipo_obj)
			{
				pos_colisao->x = ator.pos.x+j;
				pos_colisao->y = ator.pos.y+i;
				return true;
			}
		}
	}
	return false;
}

bool atores_colidem (ator_t const ator1, ator_t const ator2)
{
	if (ator1.pos.x < ator2.pos.x + ATOR_WIDTH &&
			ator1.pos.x + ATOR_WIDTH > ator2.pos.x &&
			ator1.pos.y < ator2.pos.y + ATOR_HEIGHT &&
			ator1.pos.y + ATOR_HEIGHT > ator2.pos.y)
		return true;
	return false;
}

void ator_anda (ator_t *ator, direcao_t const direcao)
{
	switch (direcao)
	{
		case DIREITA:
			if (ator->pos.x < MAPA_WIDTH-1)
				ator->pos.x++;
			break;
		case ESQUERDA:
			if (ator->pos.x > 0)
				ator->pos.x--;
			break;
		case CIMA:
			if (ator->pos.y > 0)
				ator->pos.y--;
			else
				ator->pos.y = MAPA_HEIGHT-1;
			break;
		case BAIXO:
			if (ator->pos.y < MAPA_HEIGHT-1)
				ator->pos.y++;
			else
				ator->pos.y = 0;
			break;
		case NONE:
			break;
	}
	ator->direcao = direcao;
}

bool pode_ir (ator_t const ator, direcao_t const direcao)
{
	bool tem_parede;
	switch (direcao)
	{
		case DIREITA:
			if (ator.pos.x >= MAPA_WIDTH-1)
				tem_parede = true;
			else
			{
				if (mapa[ator.pos.y][ator.pos.x+1] & VALOR_PAREDE_FISICA)
				{
					// Se quer movimentar um fantasma, tem que ver se nao e a parede do meio que eles atavessam
					if (ator.tipo == VALOR_FANTASMA && (mapa[ator.pos.y][ator.pos.x+1] & VALOR_PAREDE_FANTASMA))
						tem_parede = false;
					else
						tem_parede = true;
				}
				else
					tem_parede = false;
			}
			break;
			
		case ESQUERDA:
			if (ator.pos.x <= 0)
				tem_parede = true;
			else
			{
				if (mapa[ator.pos.y][ator.pos.x-1] & VALOR_PAREDE_FISICA)
				{
					// Se quer movimentar um fantasma, tem que ver se nao e a parede do meio que eles atavessam
					if (ator.tipo == VALOR_FANTASMA && (mapa[ator.pos.y][ator.pos.x-1] & VALOR_PAREDE_FANTASMA) && ator.estado == MORTO)
						tem_parede = false;
					else
						tem_parede = true;
				}
				else
					tem_parede = false;
			}
			break;
			
		case CIMA:
			if (ator.pos.y <= 0)
			{
				if (mapa[MAPA_HEIGHT-1][ator.pos.x] & VALOR_PAREDE_FISICA)
					tem_parede = true;
				else
					tem_parede = false;
			}
			else
			{
				if (mapa[ator.pos.y-1][ator.pos.x] & VALOR_PAREDE_FISICA)
					tem_parede = true;
				else
					tem_parede = false;
			}
			break;
			
		case BAIXO:
			if (ator.pos.y <= 0)
			{
				if (mapa[0][ator.pos.x] & VALOR_PAREDE_FISICA)
					tem_parede = true;
				else
					tem_parede = false;
			}
			else
			{
				if (mapa[ator.pos.y+1][ator.pos.x] & VALOR_PAREDE_FISICA)
					tem_parede = true;
				else
					tem_parede = false;
			}
			break;
			
		case NONE:
			tem_parede = false;
			break;
	}
	return !tem_parede;
}

uint8_t movimentos_possiveis (ator_t const ator)
{
	uint8_t ret = 0;
	if (pode_ir(ator, DIREITA))
		ret |= 1<<(int)DIREITA;
	if (pode_ir(ator, ESQUERDA))
		ret |= 1<<(int)ESQUERDA;
	if (pode_ir(ator, CIMA))
		ret |= 1<<(int)CIMA;
	if (pode_ir(ator, BAIXO))
		ret |= 1<<(int)BAIXO;
	return ret;
}

void vai_a(pos_t const pos, ator_t *ator)
{
	uint8_t mov_possiveis = movimentos_possiveis(*ator);
	uint8_t cont_cruzamento = 0;
	bool cruzamento;
	uint8_t mov_aleatorio;
	
	// Descobre se o ator ta em um cruzamento
	// Se pode ir para a direita
	if (mov_possiveis & 1<<(int)DIREITA)
	{
		cont_cruzamento++;
	}
	if (mov_possiveis & 1<<(int)ESQUERDA)
	{
		cont_cruzamento++;
	}
	if (mov_possiveis & 1<<(int)BAIXO)
	{
		cont_cruzamento++;
	}
	if (mov_possiveis & 1<<(int)CIMA)
	{
		cont_cruzamento++;
	}
	
	if (cont_cruzamento > 2)
	{
		cruzamento = true;
	}
	
	// Se e cruzamento, escolhe um caminho a seguir, mas sem voltar atras
	if (cruzamento == true)
	{
		// Se o objetivo esta a direita e e possivel andar para a direita
		if (pos.x > ator->pos.x && mov_possiveis & 1<<(int)DIREITA && ator->direcao != ESQUERDA)
				ator_anda(ator, DIREITA);
		else if (pos.x < ator->pos.x && mov_possiveis & 1<<(int)ESQUERDA && ator->direcao != DIREITA)
			ator_anda(ator, ESQUERDA);
		else if (pos.y < ator->pos.y && mov_possiveis & 1<<(int)CIMA && ator->direcao != BAIXO)
			ator_anda(ator, CIMA);
		else if (pos.y > ator->pos.y && mov_possiveis & 1<<(int)BAIXO && ator->direcao != CIMA)
			ator_anda(ator, BAIXO);
		// Se nao tem nenhuma direcao disponivel que leve ao objetivo
		else
		{
			// Se ta no beco dos fantasmas, sai
			if (mov_possiveis == 1<<(int)DIREITA)
			{
				ator_anda(ator, DIREITA);
			}
			// vai para uma direcao aleatoria (sem voltar)
			else
			{
				do
				{
					mov_aleatorio = rand()%4;
				} while (!((mov_possiveis & 1<<mov_aleatorio) && (ator->direcao != direcao_contraria((direcao_t)mov_aleatorio))));
				ator_anda(ator, (direcao_t)mov_aleatorio);
			}
		}
	}
	// Se nao e cruzamento, segue o caminho
	else
	{
		// Se o caminho e em linha reta
		if (mov_possiveis & 1<<(int)ator->direcao)
		{
			ator_anda(ator, ator->direcao);
		}
		// Se e uma quina
		else
		{
			if (mov_possiveis & 1<<(int)DIREITA && ator->direcao != ESQUERDA)
				ator_anda(ator, DIREITA);
			else if (mov_possiveis & 1<<(int)ESQUERDA && ator->direcao != DIREITA)
				ator_anda(ator, ESQUERDA);
			else if (mov_possiveis & 1<<(int)CIMA && ator->direcao != BAIXO)
				ator_anda(ator, CIMA);
			else if (mov_possiveis & 1<<(int)BAIXO && ator->direcao != CIMA)
				ator_anda(ator, BAIXO);
		}
	}
}

direcao_t direcao_contraria (direcao_t const dir)
{
	switch (dir)
	{
		case DIREITA:
			return ESQUERDA;
		case ESQUERDA:
			return DIREITA;
		case CIMA:
			return BAIXO;
		case BAIXO:
			return CIMA;
		default:
			return NONE;
	}
}
