/*----------------------------------------------------------------------------
 * Thread 1 'interacao_entradas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#include "interacao_entradas.h"

osThreadId tid_interacao_entradas;                            // thread id
osThreadDef (interacao_entradas, osPriorityNormal, 1, 0);     // thread object
 
void init_interacao_entradas (void)
{
  tid_interacao_entradas = osThreadCreate(osThread(interacao_entradas), NULL);
}
 
void interacao_entradas (void const *argument)
{
  uint16_t joy_x, joy_y;

  while(osSignalWait(0x0001, osWaitForever).status == osEventSignal)
  {
		#if SIMULACAO == 0
    joy_x = joy_read_x();
    joy_y = joy_read_y();
    if(joy_x > 0xD00)
      direcao_joystick = DIREITA;
    else if(joy_y > 0xD00)
      direcao_joystick = CIMA;
    else if(joy_x < 0x300)
      direcao_joystick = ESQUERDA;
    else if(joy_y < 0x300)
      direcao_joystick = BAIXO;
    else
      direcao_joystick = NONE;
    
    btn_joy = joy_read_center();
    btn_rst = button_read_s1();
    btn_cor_pb = button_read_s2();
		#else
		joy_x = 0x800;
		joy_y = 0x800;
		btn_joy = false;
		btn_rst = false;
		btn_cor_pb = false;
		#endif
    osSignalSet(tid_pacman_thread, 0x0001);
  }
}

