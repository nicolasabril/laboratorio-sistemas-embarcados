/*----------------------------------------------------------------------------
 * Thread 1 'fantasmas': Gera chaves para tentar decodificar a mensagem
 * Autores: Nicolas Abril, Lucca Rawlyk
 *---------------------------------------------------------------------------*/

#ifndef fantasmas_H
#define fantasmas_H

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdlib.h>
#include "recursos_compartilhados.h"
#include "ator.h"

#define FANTASMA_POS_X_START 60
#define FANTASMA_POS_Y_START 33
#define FANTASMA_PROB_ERRAR 40
#define FANTASMA_ATRASO 3 // A cada FANTASMA_ATRASO passos, o fantasma nao vai andar
extern ator_t pacman;

void fantasmas_thread (void const *argument); // thread function
void init_fantasmas_thread (void);
void reseta_fantasmas (void);
void move_fantasmas (void);

#endif
