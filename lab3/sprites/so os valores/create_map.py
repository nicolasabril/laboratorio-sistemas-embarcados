def create (namemapsprite, namemapphysics, nameghostwalls, namepill, namepowerpill):
    filemapsprite = open(namemapsprite, 'r')
    filemapphysics = open(namemapphysics, 'r')
    fileghostwalls = open(nameghostwalls, 'r')
    filepill = open(namepill, 'r')
    filepowerpill = open(namepowerpill, 'r')

    conteudo = []
    conteudo.append(filemapphysics.readlines())
    conteudo.append(filemapsprite.readlines())
    conteudo.append(fileghostwalls.readlines())
    conteudo.append(filepill.readlines())
    conteudo.append(filepowerpill.readlines())
    
    in_data = ['','','','','']
    for i in range(len(conteudo[0])):
      in_data[0] += conteudo[0][i].splitlines()[0]
      in_data[1] += conteudo[1][i].splitlines()[0]
      in_data[2] += conteudo[2][i].splitlines()[0]
      in_data[3] += conteudo[3][i].splitlines()[0]
      in_data[4] += conteudo[4][i].splitlines()[0]
    
    maze_data = []
    for i in range(len(in_data[0])):
        maze_data.append( str( int(in_data[0][i]) + 2*int(in_data[1][i]) + 128*int(in_data[2][i]) + 4*int(in_data[3][i]) + 8*int(in_data[4][i]) ) )
    
    output_file = open("map.c", 'w')
    output_file.write("const uint8_t maze[84][128] = {")
    i = 0
    for num in maze_data:
        if i==0:
            output_file.write("\n\t{ " + num + ', ')
        elif (i%128)==0:
            output_file.write("},\n\t{ " + num + ', ')
        elif (i%128)==127:
            output_file.write(num + ' ')
        else:
            output_file.write(num + ', ')
        i += 1
        
    output_file.write("}\n};")

    filemapsprite.close()
    filemapphysics.close()
    fileghostwalls.close()
    filepill.close()
    filepowerpill.close()
    output_file.close() 
