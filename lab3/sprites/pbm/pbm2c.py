# ppm2c.py - Converte uma imagem ppm com dados em binario para uma váriavel const uint8_t[] de C

def convert_pbm (filename):
    pbm_file = open(filename, 'r')
    split_name = filename.split('.')
    
    conteudo = pbm_file.readlines()
    (width, height) = conteudo[1].split()
    data = ""
    for i in range(2, len(conteudo)):
      data += conteudo[i].splitlines()[0]

    print(data)
    
    output_file = open(split_name[0] + ".c", 'w')
    output_file.write("const uint8_t img_" + split_name[0])
    output_file.write("[" + height + "][" + width + "]")
    output_file.write(" = {\n") 
    i = 0
    for char in data:
        if (i % int(width)) == 0:
            output_file.write("{ " + char + ", ")
        elif int(width)-1 > (i % int(width)) > 0:
            output_file.write(char + ", ")
        elif i < len(data)-1:
            output_file.write(char + " },\n")
        else:
            output_file.write(char + " }\n")
        i += 1
    output_file.write("};")

    pbm_file.close()
    output_file.close() 
