#ifndef display_h
#define display_h

#include "my_types.h"

#define DISPLAY_WAVE_X1 15
#define DISPLAY_WAVE_X2 114
#define DISPLAY_WAVE_Y1 31
#define DISPLAY_WAVE_Y2 120
#define DISPLAY_WAVE_WIDTH (DISPLAY_WAVE_X2 - DISPLAY_WAVE_X1 + 1)
#define DISPLAY_WAVE_HEIGHT (DISPLAY_WAVE_Y2 - DISPLAY_WAVE_Y1 + 1)
#define DISPLAY_WAVE_GAP 20

// Inicializa o perif�rico do display
void initDisplay (void);

// Inicializa a thread do display
// shared_resources - Ponteiro para os recursos compartilhados, criado pela thread Control
void init_display_thread(shared_resources_t *shared_resources);

// Fun��o chamada pelo Timer do display
// Somente manda sinal para a Thread
void display_tick (const void * arg);

// Fun��o executada pela Thread do Display
// arg - Ponteiro para os recursos compartilhados
void display_thread (const void *arg);

// Desenha os elementos estaticos do display
void drawMenu(void);

// Limpa toda a regiao onde e desenhada a onda
// current_px_pos - Posicao atual para desenhar a onda. Fica zerada no fim da execucao da funcao
// drawn_pixel_lines - Posicao dos pixels pintados da onda. Fica zerada no fim da execucao da funcao
void clearWave(uint8_t *current_px_pos, uint8_t drawn_pixel_lines[DISPLAY_WAVE_WIDTH]);

// Desenha o valor da resolucao da onda mostrada no display
// resolution_us - Resolucao que sera escrita
void drawResolutionText(uint32_t resolution_us);

// Desenha o nome do formato de onda atual
// shape - Formato de onda que sera escrito
void drawShapeText(wave_shape_t shape);

// Desenha o valor da amplitude da onda
// amplitude - Amplitude da onda, em volts, de 0 a 3.3
void drawAmplitudeText(float amplitude);

// Desenha o valor da frequencia da onda
// period_us - Periodo da onda que sera convertido em frequencia e desenhado
void drawFrequencyText(uint32_t period_us);

// Desenha o proximo passo da onda
// output_normalized - Valor de 0 a 1 correspondente a amplitude de onda que sera desenhada
// current_px_pos  - Coluna em que sera desenhada o passo da onda
// drawn_pixel_lines - Vetor com as linhas em que os pixels da onda estao desenhados atualmente
// wave_amplitude - Valor da amplitude maxima da onda, de 0 a 3.3
void updateWave (float output_normalized, uint8_t current_px_pos, uint8_t drawn_pixel_lines[DISPLAY_WAVE_WIDTH], float wave_amplitude);

#endif //display_h
