/*============================================================================
 *                               Laboratório 3
 *                       Nicolas Abril e Lucca Rawlyk
 *---------------------------------------------------------------------------*
 *                    Prof. Andr� Schneider de Oliveira
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *===========================================================================*/
#include "cmsis_os.h"                   // ARM::CMSIS:RTOS:Keil RTX
#include "TM4C129.h"                    // Device header
#include <stdbool.h>
#include "driverlib/sysctl.h"
#include "uart.h"
#include "wave.h"
#include "pwm.h"
#include "display.h"
#include "control.h"
#include "gantt.h"
#include "my_types.h"

uint32_t g_ui32SysClock;

int main (void)
{
	
	shared_resources_t *shared_resources;
	
  g_ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), 120000000);
	
	
	osKernelInitialize();

	shared_resources = init_control_thread();
	init_gantt_thread(shared_resources);
	initUART(shared_resources->uart_msg_q_id);
	initPWM();
	init_display_thread(shared_resources);
	
	send_instructions();
	
	osKernelStart(); 
	
	// Gantt de inicio da main
	if (!(shared_resources->status->mute))
	{
		// INICIO DA SECAO CRITICA		
		osMutexWait(shared_resources->status_mutex_id, osWaitForever);
		gantt_enqueue (shared_resources->gantt_mail_q_id, "main", gantt_start_e);
		osMutexRelease(shared_resources->status_mutex_id);
		// FIM DA SECAO CRITICA
	}	
	
	init_wave(shared_resources);
	
	// Gantt de fim da main
	if (!(shared_resources->status->mute))
	{
		// INICIO DA SECAO CRITICA
		osMutexWait(shared_resources->status_mutex_id, osWaitForever);
		gantt_enqueue (shared_resources->gantt_mail_q_id, "main", gantt_end_e);
		osMutexRelease(shared_resources->status_mutex_id);
		// FIM DA SECAO CRITICA
	}
	
	osDelay(osWaitForever);
}
