#include "uart.h"

// Macro para acessar registradores pelos seus endereços
#define HWREG(x) (*((volatile uint32_t *)(x)))

#define SYSCTL_RCGCUART_R 		0x400FE618 // UART Run Mode Clock Gating Control 
#define SYSCTL_RCGCUART_R0 		0x00000001 // UART Module 0 Run Mode Clock Gating Control

#define SYSCTL_RCGCGPIO_R 		0x400FE608 // GPIO Run Mode Clock Gating Control 
#define SYSCTL_RCGCGPIO_R0 		0x00000001 // GPIO Port A Run Mode Clock Gating Control

#define SYSCTL_PRUART_R 		0x400FEA18 // UART Peripheral Ready
#define SYSCTL_PRUART_R0 		0x00000001 // UART Module 0 Peripheral Ready

#define SYSCTL_PRGPIO_R 		0x400FEa08 // GPIO Peripheral Ready
#define SYSCTL_PRGPIO_R0 		0x00000001 // GPIO Port A Peripheral Ready

#define GPIO_PIN_0				0x00000001 // GPIO Pin 0
#define GPIO_PIN_1				0x00000002 // GPIO Pin 1
#define GPIO_PORTA_AFSEL_R 		0x40058420 // GPIO Port A Alternate Function Select 
#define GPIO_PORTA_DEN_R 		0x4005851C // GPIO Port A Digital Enable
#define GPIO_PORTA_AMSEL_R 		0x40058528 // GPIO Port A Analog Mode Select
#define GPIO_PORTA_DR2R_R		0x40058500 // GPIO Port A 2-mA Drive Select
#define GPIO_PORTA_SLR_R 		0x40058518 // GPIO Port A Slew Rate Control Select

#define GPIO_PORTA_PCTL_R 		0x4005852C // GPIO Port A Port Control
#define GPIO_PA0_U0RX           0x00000001 // PA0 com funcao U0Rx
#define GPIO_PA1_U0TX           0x00000010 // PA1 com funcao U0Tx

#define UART0_CTL_R 			0x4000C030 // UART Module 0 Control
#define UART_CTL_UARTEN			0x00000001 // UART Enable
#define UART_CTL_HSE 			0x00000020 // High-speed Enable

#define UART0_LCRH_R 			0x4000C02C  // UART Module 0 Line Control, High Byte register
#define UART_LCRH_FEN			0x00000010  // UART Enable FIFOs
#define UART_LCRH_WLEN_8    	0x00000060  // UART Word Length = 8 bits
#define UART_LCRH_STP2   		0x00000008  // UART Two Stop Bits Select
#define UART_LCRH_PEN   		0x00000002  // UART Parity Enable

#define UART0_CC_R 				0x4000cfc8 // UART Module 0 Clock Configuration 
#define UART_CC_SYSCLK 			0x00000000 // System clock as UART Baud Clock Source

#define UART0_IBRD_R 			0x4000C024 // UART Module 0 Integer Baud-Rate Divisor 
#define UART0_FBRD_R 			0x4000C028 // UART Fractional Baud-Rate Divisor

#define UART0_IM_R				0x4000c038 // UART Module 0 Interrupt Mask 
#define UART_IM_RXIM			0x00000010 // UART Receive Interrupt Mask
#define UART_IM_RTIM			0x00000040 // UART Receive Time-Out Interrupt Mask

#define NVIC_PRI1_R           	0xE000E404  // Interrupt 4-7 Priority 
#define NVIC_PRI1_INT5_M		0x0000E000  // Interrupt 5 Priority Mask

#define NVIC_EN0_R              0xE000E100  // Interrupt 0-31 Set Enable
#define NVIC_EN0_INT5           0x00000020  // Interrupt 5 enable

#define UART0_FR_R              0x4000C018 // UART Module 0 Flag
#define UART_FR_RXFF            0x00000040 // UART Receive FIFO Full
#define UART_FR_TXFF            0x00000020 // UART Transmit FIFO Full
#define UART_FR_RXFE            0x00000010 // UART Receive FIFO Empty

#define UART0_DR_R              0x4000C000 // UART Module 0 Data

#define UART0_ICR_R             0x4000C044 // UART Module 0 Interrupt Clear
#define UART0_RIS_R             0x4000C03C // UART Module 0 Raw Interrupt Status

// MailQueue para enviar os dados recebidos na UART
// Configurado por initUART()
osMessageQId handler_msg_q_id;

void initUART (osMessageQId msg_q_id)
{
	uint32_t brdi, brdf;
	
	// Seta o MailQ que vai ser usado
	handler_msg_q_id = msg_q_id;

	// Habilita o clock da UART0
	HWREG(SYSCTL_RCGCUART_R) |= SYSCTL_RCGCUART_R0;

  // Habilita a porta A
	HWREG(SYSCTL_RCGCGPIO_R) |= SYSCTL_RCGCGPIO_R0;

	// Habilita funcao alternativa nos pinos da UART
	HWREG(GPIO_PORTA_AFSEL_R)|= (GPIO_PIN_0 | GPIO_PIN_1);

	// Habilita IO digital nos pinos da UART
	HWREG(GPIO_PORTA_DEN_R) |= (GPIO_PIN_0 | GPIO_PIN_1);

	// Desabilita modo analogico
	HWREG(GPIO_PORTA_AMSEL_R) &= ~(GPIO_PIN_0 | GPIO_PIN_1);
	
	// Habilita drive de 2mA
	HWREG(GPIO_PORTA_DR2R_R) |= (GPIO_PIN_0 | GPIO_PIN_1);

	// Desabilita controle de slew rate
	HWREG(GPIO_PORTA_SLR_R) &= ~(GPIO_PIN_0 | GPIO_PIN_1);
	
	// Conecta os pinos as funcoes alternativas corretas (UART RX e TX)
	HWREG(GPIO_PORTA_PCTL_R) = (HWREG(GPIO_PORTA_PCTL_R) & ~(0xff)) | (GPIO_PA0_U0RX | GPIO_PA1_U0TX);
	
	// Desabilita a UART para poder configurar
	HWREG(UART0_CTL_R) &= ~UART_CTL_UARTEN;
	
    //Configura o baud rate.
	// BRD = BRDI + BRDF = UARTSysClk / (ClkDiv * Baud Rate)
	brdi = UART_CLK / (16 * UART_BAUD);
	brdf = (uint32_t)(((UART_CLK*64) / (16 * UART_BAUD))%64 + 0.5);
    HWREG(UART0_IBRD_R) = brdi; // Parte inteira
    HWREG(UART0_FBRD_R) = brdf; // Parte fracionaria
	
	// Configura modo de funcionamento da UART (sem FIFO, words de 8bits, 1 stop bit, nenhum bit de paridade
	HWREG(UART0_LCRH_R) = ((HWREG(UART0_LCRH_R) & ~(0xff)) & ~(UART_LCRH_PEN | UART_LCRH_STP2 | UART_LCRH_FEN)) | (UART_LCRH_WLEN_8);
	
	// Escolhe fonte de clock como clock do sistema
	HWREG(UART0_CC_R) = (HWREG(UART0_CC_R) & ~0xf) | UART_CC_SYSCLK;
	
	// Desabilita modo High speed (divide clock por 16 em vez de 8)
	HWREG(UART0_CTL_R) &= ~UART_CTL_HSE;
			
	// Habilita interrupcao em RX e RX timeout
	HWREG(UART0_IM_R) |= (UART_IM_RXIM|UART_IM_RTIM);
	
	// Habilita a UART0
	HWREG(UART0_CTL_R) |= UART_CTL_UARTEN;
	
	// UART0 com prioridade 2 de interrupcao
	HWREG(NVIC_PRI1_R) = (NVIC_PRI1_R & ~NVIC_PRI1_INT5_M) | (2 << 13);
	
	// Habilita a interrupcao 5 (UART0)
	HWREG(NVIC_EN0_R) = NVIC_EN0_INT5;
	
	// Habilita interrupcoes no processador
	__asm("cpsie i\n");
		
	// Envia uma mensagem pela uart avisando que foi inicializado com sucesso
	//writeUART((uint8_t*)"UART inicializada", 17);
}

void writeUART (char *data, uint32_t len)
{
    uint32_t i;
	
	// Se o comprimento e 0, considera que a string termina em \0
	if (len == 0)
	{
		// Envia a mensagem pela UART byte a byte, adivinhando o fim por um \0
		for(i=0; data[i] != '\0'; i++)
		{
			writeByteUART(data[i]);
		}
	}
	else
	{
		// Envia a mensagem pela UART byte a byte
		for (i=0; i<len; i++)
		{
			writeByteUART(data[i]);
		}
	}
}

void writeByteUART (char data)
{
    // Wait until space is available.
    while(HWREG(UART0_FR_R) & UART_FR_TXFF)
        ;
    // Send the char.
    HWREG(UART0_DR_R) = data;
}

void UART0_Handler (void)
{
    // Variavel para guardar a mensagem que chegou na UART
    uint8_t rcv_msg;
		osStatus msg_put_status;
	
    // Limpa as interrupcoes ocorridas
    HWREG(UART0_ICR_R) = HWREG(UART0_RIS_R);

    // Espera um caracter estar disponivel
    while((HWREG(UART0_FR_R) & UART_FR_RXFE))
		;

		// Pega o byte recebido
			rcv_msg = (uint8_t) HWREG(UART0_DR_R);
		
		#ifdef UART_ECHO
			// Eco do byte recebido
			writeByteUART(rcv_msg);
		#endif // UART_ECHO
		// Envia a mensagem ao tratador
		msg_put_status = osMessagePut(handler_msg_q_id, rcv_msg, 0);
		if (msg_put_status != osOK)
		{
			writeUART("Erro UART message queue put", 0);
		}
}
