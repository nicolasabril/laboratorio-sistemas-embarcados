#include "control.h"
#include "gantt.h"
#include "uart.h"
#include "my_types.h"
#include "stdbool.h"

osMessageQDef(uart_msg_q, UART_MSG_Q_LEN, uint8_t);
osMessageQId uart_msg_q_id;


osPoolDef(status_pool, 1, status_t);
osPoolId status_pool_id;

osMutexDef(status_mutex);
osMutexId status_mutex_id;

osPoolId shared_resources_id;
osPoolDef(shared_resources_pool, 1, shared_resources_t);
osThreadDef(control_thread, osPriorityAboveNormal, 1, 0);

shared_resources_t* init_control_thread (void)
{
	status_t *status;
	shared_resources_t *shared_resources;
	
	// Cria a fila de mensagens da UART
	uart_msg_q_id = osMessageCreate(osMessageQ(uart_msg_q), NULL);
	
	// Cria a variavel compartilhada de status
	status_pool_id = osPoolCreate(osPool(status_pool));
	status = (status_t *)osPoolAlloc(status_pool_id);
	status->shape = sine_e;
	status->wave_amplitude_v = 3.0;
	status->resolution_us = 20000;
	status->period_us = 10000;
	status->mute = true;
	
	// Cria o mutex para acessar o status
	status_mutex_id = osMutexCreate(osMutex(status_mutex));
	
	// Cria a estrutura que contem todos os recursos compartilhados
	shared_resources_id = osPoolCreate(osPool(shared_resources_pool));
	shared_resources = (shared_resources_t *)osPoolAlloc(shared_resources_id);
	shared_resources->status = status;
	shared_resources->status_mutex_id = status_mutex_id;
	shared_resources->uart_msg_q_id = uart_msg_q_id;
	
	// Cria a thread de controle 
	osThreadCreate(osThread(control_thread), (void*)shared_resources);
		
	return shared_resources;
}

void control_thread (const void * arg)
{
	status_t *status = ((shared_resources_t*)arg)->status;
	osMutexId status_mutex_id = ((shared_resources_t*)arg)->status_mutex_id;
	osMessageQId uart_msg_q_id = ((shared_resources_t*)arg)->uart_msg_q_id;
	osMailQId gantt_mail_q_id = ((shared_resources_t*)arg)->gantt_mail_q_id;

	osEvent msg_evt;
	
	float new_freq;
	float new_amp;
	uint32_t new_res;
	
	while (1)
	{
		msg_evt = osMessageGet(uart_msg_q_id, osWaitForever);
		if (msg_evt.status != osEventMessage)
		{
			writeUART("\n\tFalha na mensagem da UART!!!\n\t\tO programa sera terminado...", 0);
			break;
		}

		// INICIO DA SECAO CRITICA		
		osMutexWait(status_mutex_id, osWaitForever);
		
		if (!(status->mute))
			gantt_enqueue(gantt_mail_q_id, "control", gantt_start_e);
		
		switch((uint8_t)(msg_evt.value.v))
		{
			case 'q':
				new_freq = ((float)1.0/((float)status->period_us/(float)1000000.0)) - (float)10.0;
				if (new_freq < 10)
					new_freq = (float)10.0;
				status->period_us = (float)1000000.0/new_freq;
				break;
				
			case 'w':
				new_freq = ((float)1.0/((float)status->period_us/(float)1000000.0)) + (float)10.0;
				if (new_freq > 200)
					new_freq = (float)200.0;
				status->period_us = (float)1000000.0/new_freq;
				break;
				
			case 'a':
				new_amp = status->wave_amplitude_v - (float)0.1;
				if (new_amp < (float)0.1)
					new_amp = (float)0.1;
				status->wave_amplitude_v = new_amp;
				break;
				
			case 's':
				new_amp = status->wave_amplitude_v + (float)0.1;
				if (new_amp > (float)3.3)
					new_amp = (float)3.3;
				status->wave_amplitude_v = new_amp;
				break;
				
			case 'e':
				new_res = status->resolution_us - 5000;
				if (new_res < 10000)
					new_res = 10000;
				status->resolution_us = new_res;
				break;
				
			case 'r':
				new_res = status->resolution_us + 5000;
				if (new_res > 200000)
					new_res = 200000;
				status->resolution_us = new_res;
				break;
				
			case 'd':
				switch (status->shape)
				{
					case sine_e:
						status->shape = square_e;
						break;
					case square_e:
						status->shape = triangle_e;
						break;
					case triangle_e:
						status->shape = sawtooth_e;
						break;
					case sawtooth_e:
						status->shape = trapezoid_e;
						break;
					case trapezoid_e:
						status->shape = sine_e;
						break;
				}
				break;
			case 'f':
				switch (status->shape)
				{
					case sine_e:
						status->shape = trapezoid_e;
						break;
					case square_e:
						status->shape = sine_e;
						break;
					case triangle_e:
						status->shape = square_e;
						break;
					case sawtooth_e:
						status->shape = triangle_e;
						break;
					case trapezoid_e:
						status->shape = sawtooth_e;
						break;
				}
				break;
			case 'm':
				status->mute = !(status->mute);
				break;
		}
		
		if (!(status->mute))
			gantt_enqueue(gantt_mail_q_id, "control", gantt_end_e);
		
		osMutexRelease(status_mutex_id);
		// FIM DA SECAO CRITICA
	}
}

void send_instructions(void)
{
	// Envia a mensagem de como usar
	writeUART("Q < Frequencia > W \r\n", 0);
	writeUART("E < Resolucao > R \r\n", 0);
	writeUART("A < Amplitude > S \r\n", 0);
	writeUART("D < Forma > F \r\n", 0);
	writeUART("M Ativar/Desativar Gantt \r\n", 0);
}

