#include "display.h"
#include "cfaf128x128x16.h"
#include "grlib/grlib.h"
#include "gantt.h"
#include "cmsis_os.h"
#include "num2string.h"

osTimerDef(timer_display, display_tick);
osTimerId timer_display_id;

osThreadDef(display_thread, osPriorityNormal, 1, 0);
osThreadId display_thread_id;

//Context to print on the screen
tContext sContext;

void initDisplay (void)
{
	cfaf128x128x16Init();
	GrContextInit(&sContext, &g_sCfaf128x128x16);
	GrContextBackgroundSet(&sContext, ClrWhite);
	GrFlush(&sContext);
	GrContextFontSet(&sContext, &g_sFontFixed6x8);
	drawMenu();
}

void init_display_thread(shared_resources_t *shared_resources)
{
	initDisplay();
	
	timer_display_id = osTimerCreate(osTimer(timer_display), osTimerPeriodic, NULL);
	display_thread_id = osThreadCreate(osThread(display_thread), (void*)shared_resources);
}

void display_tick (const void * arg)
{
	osSignalSet(display_thread_id, 0x0001);
}

void display_thread (const void *arg)
{
	osMutexId status_mutex_id = ((shared_resources_t*)arg)->status_mutex_id;
	osMailQId gantt_mail_q_id = ((shared_resources_t*)arg)->gantt_mail_q_id;
	status_t *pstatus = ((shared_resources_t*)arg)->status;
	status_t status = *pstatus;
	status_t last_status = status;
	uint8_t drawn_pixel_lines[DISPLAY_WAVE_WIDTH] = {0}; // Linha em que os pixels da onda estao desenhados, para saber onde apagar
	uint8_t current_px_pos = 0;
	
	drawFrequencyText(status.period_us);
	drawShapeText(status.shape);
	drawAmplitudeText(status.wave_amplitude_v);
	drawResolutionText(status.resolution_us);
	
	// 1 tick = 100 us
	osTimerStart(timer_display_id, status.resolution_us/(100*DISPLAY_WAVE_WIDTH));
	
	//F 500Hz   R 001ms 
	//A 3.3V    S TRAPEZ
	
	while(osSignalWait(0x0001, osWaitForever).status == osEventSignal)
	{
		
		// INICIA SECAO CRITICA
		osMutexWait(status_mutex_id, osWaitForever);
		status = *pstatus;
		osMutexRelease(status_mutex_id);
		// FIM DA SECAO CRITICA
		
		if(!(status.mute))
			gantt_enqueue(gantt_mail_q_id, "display", gantt_start_e);
		
		// Se o periodo mudou, rescreve o texto
		if (last_status.period_us != status.period_us)
		{
			drawFrequencyText(status.period_us);
		}
		
		// Se o formato mudou, rescreve o texto
		if (last_status.shape != status.shape)
		{		
			drawShapeText(status.shape);
		}
		
		// Se a amplitude mudou, rescreve o texto
		if (last_status.wave_amplitude_v != status.wave_amplitude_v)
		{
			drawAmplitudeText(status.wave_amplitude_v);
		}
		
		// Se a resolucao mudou, precisa atualizar o timer, refazer a onda e reescrever o texto
		if (last_status.resolution_us != status.resolution_us)
		{
			osTimerStop(timer_display_id);
			osTimerStart(timer_display_id, status.resolution_us/(100*DISPLAY_WAVE_WIDTH));
			drawResolutionText(status.resolution_us);
			clearWave(&current_px_pos, drawn_pixel_lines);
		}
		last_status = status;
		
		updateWave(status.output_normalized, current_px_pos, drawn_pixel_lines, status.wave_amplitude_v);
		
		current_px_pos = (current_px_pos + 1) % (DISPLAY_WAVE_WIDTH);

		if(!(status.mute))
			gantt_enqueue(gantt_mail_q_id, "display", gantt_end_e);
	}
}

void drawMenu(void)
{
	tRectangle wave_border;
	tRectangle screen;
	
	screen.i16XMin = 0;
	screen.i16XMax = 127;
	screen.i16YMin = 0;
	screen.i16YMax = 127;
	
	wave_border.i16XMin = DISPLAY_WAVE_X1-1;
	wave_border.i16XMax = DISPLAY_WAVE_X2+1;
	wave_border.i16YMin = DISPLAY_WAVE_Y1-1;
	wave_border.i16YMax = DISPLAY_WAVE_Y2+2;
	
	// INICIA SECAO CRITICA
	GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &screen);
	
	GrContextForegroundSet(&sContext, ClrBlack);
	GrStringDraw(&sContext, "F", -1, 2, 2, true);
	GrStringDraw(&sContext, "A", -1, 2, (sContext.psFont->ui8Height) + 4, true);
	GrStringDraw(&sContext, "R", -1, (sContext.psFont->ui8MaxWidth)*10 + 2, 2, true);
	GrStringDraw(&sContext, "S", -1, (sContext.psFont->ui8MaxWidth)*10 + 2, (sContext.psFont->ui8Height) + 4, true);
	
	GrContextForegroundSet(&sContext, ClrRed);
	GrRectDraw(&sContext, &wave_border);
	// FIM DA SECAO CRITICA
	
}

void drawFrequencyText(uint32_t period_us)
{
	char freq_buf[8];
	tRectangle text_fill;
	
	intToString(1000000/period_us, freq_buf, 3, 10, 3);
	freq_buf[3] = ' ';
	freq_buf[4] = 'H';
	freq_buf[5] = 'z';
	freq_buf[6] = '\0';
	
	text_fill.i16XMin = (sContext.psFont->ui8MaxWidth)*2 + 2;
	text_fill.i16XMax = (sContext.psFont->ui8MaxWidth)*(2+7) + 2;
	text_fill.i16YMin = 2;
	text_fill.i16YMax = (sContext.psFont->ui8Height) + 2;
	
	// INICIA SECAO CRITICA
	GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &text_fill);
	GrContextForegroundSet(&sContext, ClrBlack);
	GrStringDraw(&sContext, freq_buf, -1, text_fill.i16XMin, text_fill.i16YMin, true);
	// FIM DA SECAO CRITICA
}

void drawAmplitudeText(float amplitude)
{
	char amp_buf[8];
	tRectangle text_fill;
	
	
	floatToString(amplitude, amp_buf, 7, 10, 0, 2);
	amp_buf[4] = ' ';
	amp_buf[5] = 'V';
	amp_buf[6] = '\0';
	
	text_fill.i16XMin = (sContext.psFont->ui8MaxWidth)*2 + 2;
	text_fill.i16XMax = (sContext.psFont->ui8MaxWidth)*(2+7) + 2;
	text_fill.i16YMin = sContext.psFont->ui8Height + 4;
	text_fill.i16YMax = (sContext.psFont->ui8Height)*2 + 4;
	
	// INICIA SECAO CRITICA
	GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &text_fill);
	GrContextForegroundSet(&sContext, ClrBlack);
	GrStringDraw(&sContext, amp_buf, -1, text_fill.i16XMin, text_fill.i16YMin, true);
	// FIM DA SECAO CRITICA
}

void drawShapeText(wave_shape_t shape)
{
	const char shape_buf[5][8] = {"SINE", "SQUARE", "TRIANG", "SAW", "TRAPEZ" };
	tRectangle text_fill;
	
	text_fill.i16XMin = (sContext.psFont->ui8MaxWidth)*12 + 2;
	text_fill.i16XMax = (sContext.psFont->ui8MaxWidth)*(12+7) + 2;
	text_fill.i16YMin = (sContext.psFont->ui8Height) + 4;
	text_fill.i16YMax = (sContext.psFont->ui8Height)*2 + 4;
	
	// INICIA SECAO CRITICA
	GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &text_fill);
	GrContextForegroundSet(&sContext, ClrBlack);
	GrStringDraw(&sContext, shape_buf[shape], -1, text_fill.i16XMin, text_fill.i16YMin, true);
	// FIM DA SECAO CRITICA
}

void drawResolutionText(uint32_t resolution_us)
{
	char res_buf[8];
	tRectangle text_fill;
	
	intToString(resolution_us/1000, res_buf, 7, 10, 3);
	res_buf[3] = ' ';
	res_buf[4] = 'm';
	res_buf[5] = 's';
	res_buf[6] = '\0';
	
	text_fill.i16XMin = (sContext.psFont->ui8MaxWidth)*12 + 2;
	text_fill.i16XMax = (sContext.psFont->ui8MaxWidth)*(12+7) + 2;
	text_fill.i16YMin = 2;
	text_fill.i16YMax = (sContext.psFont->ui8Height) + 2;
	
	// INICIA SECAO CRITICA
	GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &text_fill);
	
	GrContextForegroundSet(&sContext, ClrBlack);
	GrStringDraw(&sContext, res_buf, -1, text_fill.i16XMin, text_fill.i16YMin, true);
	// FIM DA SECAO CRITICA
}

void clearWave(uint8_t *current_px_pos, uint8_t drawn_pixel_lines[DISPLAY_WAVE_WIDTH])
{
	int i;
	const tRectangle wave_area = {DISPLAY_WAVE_X1, DISPLAY_WAVE_Y1, DISPLAY_WAVE_X2, DISPLAY_WAVE_Y2+1};
	
	// INICIA SECAO CRITICA
	GrContextForegroundSet(&sContext, ClrWhite);
	GrRectFill(&sContext, &wave_area);
	// FIM DA SECAO CRITICA
	
	*current_px_pos = 0;
	for (i=0; i < DISPLAY_WAVE_WIDTH; i++)
	{
		// INICIA SECAO CRITICA
		//GrContextForegroundSet(&sContext, ClrWhite);
		//GrPixelDraw(&sContext, DISPLAY_WAVE_X1+i, drawn_pixel_lines[i]);
		// FIM DA SECAO CRITICA
		drawn_pixel_lines[i] = 0;
	}
}

void updateWave (float output_normalized, uint8_t current_px_pos, uint8_t drawn_pixel_lines[DISPLAY_WAVE_WIDTH], float wave_amplitude)
{
	uint8_t px_to_clear = (current_px_pos + DISPLAY_WAVE_GAP) % DISPLAY_WAVE_WIDTH;
	uint8_t output_height;
	uint8_t output_offset;
	
	// INICIA SECAO CRITICA
	// Limpa valor antigo do px um pouco a frente
	GrContextForegroundSet(&sContext, ClrWhite);
	if (drawn_pixel_lines[px_to_clear] >= DISPLAY_WAVE_Y1)
	{
		GrPixelDraw(&sContext, DISPLAY_WAVE_X1+px_to_clear, drawn_pixel_lines[px_to_clear]);
	}
	
	// Pinta o pixel atual com o novo valor
	output_height = ((float)1.0 - output_normalized) * (float)DISPLAY_WAVE_HEIGHT * wave_amplitude/(float)3.3;
	output_offset = DISPLAY_WAVE_Y1 + ((((float)3.3 - wave_amplitude) / (float)6.6) * DISPLAY_WAVE_HEIGHT);
	drawn_pixel_lines[current_px_pos] = output_height + output_offset;
	if (drawn_pixel_lines[current_px_pos] >= DISPLAY_WAVE_Y1)
	{
		GrContextForegroundSet(&sContext, ClrBlack);
		GrPixelDraw(&sContext, DISPLAY_WAVE_X1+current_px_pos, drawn_pixel_lines[current_px_pos]);
	}
	// FIM DA SECAO CRITICA
}

