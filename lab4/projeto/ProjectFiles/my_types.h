#ifndef my_types_h
#define my_types_h

#include "stdint.h"
#include "cmsis_os.h"
#include "stdbool.h"

// Tipo do formato da onda gerada
typedef enum {sine_e=0, square_e, triangle_e, sawtooth_e, trapezoid_e} wave_shape_t;

// Tipo que representa o estado atual da execucao
typedef struct
{
	wave_shape_t shape;
	uint32_t period_us;
	uint32_t resolution_us;
	float wave_amplitude_v;
	float output_normalized;
	bool mute;
} status_t;

// Tipo que indica se a mensagem de gantt indica o fim ou o comeco de uma execucao
typedef enum {gantt_start_e='y', gantt_end_e='n'} gantt_type_t;

// Tipo enviado pela Mail Queue da thread que manda o Gantt
typedef struct 
{
    char thread_name[8];
	uint32_t tick;
	gantt_type_t msg_type;
} gantt_mail_t;

// Conjunto de recursos compartilhados pelas Threads
typedef struct
{
	osMessageQId uart_msg_q_id;
	status_t *status;
	osMutexId status_mutex_id;
	osMailQId gantt_mail_q_id;
} shared_resources_t;

#endif //my_types_h
