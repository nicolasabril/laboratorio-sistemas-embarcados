#include "wave.h"
#include "pwm.h"
#include "math.h"
#include "gantt.h"

#define M_2PI (float)(6.283185307179586)

osTimerDef(wave_timer, wave_tick);
osTimerId wave_timer_id;

osMailQId gantt_mail_q_id;

osMutexId status_mutex; // Mutex para acessar status
status_t *pstatus; // Ponteiro para o status compartilhado
status_t status; // Copia local de status
status_t last_status;

uint32_t n_ticks_in_a_period;
uint32_t time_step_count = 0;
float time_step_normalized = 0.0;

float wave_amplitude_normalized = 0.5;
float last_wave_amplitude_normalized = 0.0;

void init_wave(shared_resources_t * shared_resources)
{	
	status_mutex = shared_resources->status_mutex_id;
	
	gantt_mail_q_id = shared_resources->gantt_mail_q_id;
	
	pstatus = shared_resources->status;
	status = *pstatus;
	
	last_status = status;
	last_status.period_us = 1; // Para atualizar na primeira execucao
		
	wave_timer_id = osTimerCreate(osTimer(wave_timer), osTimerPeriodic, NULL);
	
	// 100 us
	osTimerStart(wave_timer_id, 1);
}

void wave_tick (const void * arg)
{
	

	// INICIA SECAO CRITICA
	// Atualiza o pstatus local
	status = *pstatus;
	// FIM DA SECAO CRITICA
	
	/*
	if(!(status.mute))
		gantt_enqueue(gantt_mail_q_id, "wave", gantt_start_e);
	*/
	
	// Se o status mudou, precisa recomecar a onda
	if ((last_status.period_us != status.period_us) || (last_status.shape != status.shape) || (last_status.wave_amplitude_v != status.wave_amplitude_v))
	{
		time_step_count = 0;
		n_ticks_in_a_period = (status.period_us) / 100;
	}
	last_status = status;
	
	// Calcula a amplitude normalizada (de 0 a 1) do proximo tick da onda
	switch (status.shape)
	{
		case sine_e:
			time_step_normalized = M_2PI * (float)time_step_count / (float)n_ticks_in_a_period;
			#ifdef USE_MY_SINE
			wave_amplitude_normalized = my_sin(time_step_normalized)/2 + (float)0.5;
			#else
			wave_amplitude_normalized = sin(time_step_normalized)/2 + (float)0.5;
			#endif
			break;
		case square_e:
			if (time_step_count >= n_ticks_in_a_period/2)
				wave_amplitude_normalized = (float)1.0;
			else
				wave_amplitude_normalized = (float)0.0;
			break;
		case triangle_e:
			if (time_step_count <= n_ticks_in_a_period/2)
				wave_amplitude_normalized = (float)time_step_count / (float)(n_ticks_in_a_period/2);
			else
				wave_amplitude_normalized = (float)1.0 - ((float)(time_step_count-(n_ticks_in_a_period/2)) / (float)(n_ticks_in_a_period/2));
			break;
		case sawtooth_e:
				wave_amplitude_normalized = (float)time_step_count / (float)(n_ticks_in_a_period);
			break;
		case trapezoid_e:
			if (time_step_count < n_ticks_in_a_period/4)
				wave_amplitude_normalized = (float)time_step_count / (float)(n_ticks_in_a_period/4);
			else if ((time_step_count >= n_ticks_in_a_period/4) && (time_step_count <= 3*n_ticks_in_a_period/4))
				wave_amplitude_normalized = (float)1.0;
			else 
				wave_amplitude_normalized = (float)1.0 - ((float)(time_step_count-(3*n_ticks_in_a_period/4)) / (float)(n_ticks_in_a_period/4));
			break;
	}
	
	// Envia o nivel de onda calculada ao PWM
	if(last_wave_amplitude_normalized != wave_amplitude_normalized)
	{
		last_wave_amplitude_normalized = wave_amplitude_normalized;
		
		// INICIA SECAO CRITICA
		sendPWM(wave_amplitude_normalized*(status.wave_amplitude_v/(float)3.3));
		// FIM DA SECAO CRITICA
		
		// INICIA SECAO CRITICA
		pstatus->output_normalized = wave_amplitude_normalized;
		// FIM DA SECAO CRITICA
	}
	
	time_step_count++;
	if(time_step_count >= n_ticks_in_a_period)
		time_step_count = 0;

	/*
	if(!(status.mute))
		gantt_enqueue(gantt_mail_q_id, "wave", gantt_end_e);
	*/
}

float my_sin(float x)
{
	float ret = x;
	ret += -x*x*x/6;
	ret += x*x*x*x*x/120;
	ret += -x*x*x*x*x*x*x/5040;
	return ret;
}
