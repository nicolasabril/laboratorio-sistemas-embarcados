#ifndef gantt_h
#define gantt_h

#include "my_types.h"
#include "cmsis_os.h"

// Se definido, envia o diagrama de gantt pela uart
#define SEND_GANTT

#define GANTT_MAIL_Q_LEN 16

// Inicializa a thread que envia o diagrama de gantt
// shared_resources - Conjunto de recursos compartilhados por todas as threads (vide my_types.h)
void init_gantt_thread (shared_resources_t *shared_resources);

// Função executada pela thread que envia o diagrama de gantt
// arg - shared_resources, mas em formato (void*)
void gantt_thread (const void * arg);

// Cria o pedido para o inicio/fim de uma entry do diagrama de gantt
// gantt_mail_q_id - id da mail queue onde vai ser colocado o pedido de gantt
// thread_name - Nome da thread. Ex: "gantt" e "display"
// msg_type - Indica se a mensagem e o inicio ou fim de uma entry
void gantt_enqueue (osMailQId gantt_mail_q_id, char thread_name[8], gantt_type_t msg_type);

// Envia o inicio/fim de uma entry do diagrama de gantt
void send_gantt (gantt_mail_t mail);

#endif // gantt_h
