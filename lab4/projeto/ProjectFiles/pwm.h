#ifndef pwm_h
#define pwm_h

#include <stdint.h>

#define PWM_IN_USE 2
#define PWM_ENABLE

extern uint32_t g_ui32SysClock;

// (12MHz/2)/PWM_PERIOD = 100kHz
// -1 porque usa modo down count
#define PWM_PERIOD (uint32_t) (g_ui32SysClock/(2*1000000) - 1)

// Inicializa o PWM
void initPWM (void);

// Manda o PWM ficar com um certo duty cycle
void sendPWM (float duty_cycle);

#endif //pwm_h
