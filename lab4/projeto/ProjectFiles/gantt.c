#include "gantt.h"
#include "uart.h"
#include "my_types.h"
#include "num2string.h"
#include "cmsis_os.h"

osMailQDef(gantt_mail_q, GANTT_MAIL_Q_LEN, gantt_mail_t);
osThreadDef(gantt_thread, osPriorityLow, 1, 0);

void init_gantt_thread (shared_resources_t *shared_resources)
{
    osMailQId gantt_mail_q_id;
	
    // Cria a mail queue por onde são enviados os pedidos de gantt
    gantt_mail_q_id = osMailCreate(osMailQ(gantt_mail_q), NULL);
	
    // Distribui a mail queue entre as outras threads
    shared_resources->gantt_mail_q_id = gantt_mail_q_id;
	
    // Cria a thread que envia o gantt
    osThreadCreate(osThread(gantt_thread), (void*)shared_resources);
}
void gantt_thread (const void * arg)
{
    osMailQId gantt_mail_q_id = ((shared_resources_t*)arg)->gantt_mail_q_id;
    gantt_mail_t local_gantt;
    osEvent rcv_event;
	const char thread_name[8] = "gantt";
	uint8_t i;
	
	for(i=0; i<8; i++)
		local_gantt.thread_name[i] = thread_name[i];

    while (1)
    {
        // Espera haver um pedido de gantt
        rcv_event = osMailGet(gantt_mail_q_id, osWaitForever);

        // Envia o inicio do gantt local
        local_gantt.tick = osKernelSysTick();
        local_gantt.msg_type = gantt_start_e;
        send_gantt(local_gantt);

        // Enquanto tem pedidos prontos na fila, envia eles
        do
        {
            send_gantt(*((gantt_mail_t*)(rcv_event.value.p)));
            osMailFree(gantt_mail_q_id, (void*)rcv_event.value.p);
            // Pega um pedido que esteja pronto na fila
            rcv_event = osMailGet(gantt_mail_q_id, 0);
        } while (rcv_event.status == osEventMail);
        
        // Envia o fim do gantt local
        local_gantt.tick = osKernelSysTick();
        local_gantt.msg_type = gantt_end_e;
        send_gantt(local_gantt);
    }
}
void gantt_enqueue (osMailQId gantt_mail_q_id, char thread_name[8], gantt_type_t msg_type)
{
    #ifdef SEND_GANTT
		uint8_t i;
        const uint32_t tick = osKernelSysTick();
        // Cria o pedido de gantt
        gantt_mail_t *gant_mail = osMailAlloc(gantt_mail_q_id, osWaitForever);
		for(i=0; i<8; i++)
			gant_mail->thread_name[i] = thread_name[i];
        gant_mail->tick = tick;
        gant_mail->msg_type = msg_type;
        // Coloca o pedido na fila
        osMailPut(gantt_mail_q_id, gant_mail);
    #endif // SEND_GANT
}

void send_gantt (gantt_mail_t mail)
{
    #ifdef SEND_GANTT
			char tick_buf[10];
			// Converte o tick em hexa
			intToString(mail.tick, tick_buf, 9, 16, 0);

			// INICIA SECAO CRITICA
			// Envia se e comeco ou fim de uma entry
			if(mail.msg_type == gantt_start_e)
				writeByteUART('y');
			else
				writeByteUART('n');
			writeByteUART(' ');
			// Envia o nome da thread
			writeUART(mail.thread_name, 0);
			writeByteUART(' ');
			// Envia o tick de inicio/fim da entry
			writeUART(tick_buf, 0);
			// Pula linha paraa proxima entry
			writeByteUART('\n');
			// FIM SECAO CRITICA
    #endif // SEND_GANT
}
