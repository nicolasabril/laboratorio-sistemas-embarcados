#ifndef wave_h
#define wave_h

#include "my_types.h"

//#define USE_MY_SINE

void wave_tick (const void * arg);

void init_wave(shared_resources_t * shared_resources);

float my_sin(float x);

#endif //wave_h
