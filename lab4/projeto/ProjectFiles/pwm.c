#include "pwm.h"

#define HWREG(x) (*((volatile uint32_t *)(x)))

#define PWM_ENABLE_R           0x40028008 // PWM Output Enable
#define PWM_ENABLE_PWM0EN       0x00000001 // M0PWM0 Output Enable
#define PWM_ENABLE_PWM1EN       0x00000002 // M0PWM1 Output Enable
#define PWM_ENABLE_PWM2EN		0x00000004 // M0PWM2 Output Enable

#define PWM_0_CTL_R				0x40028040 // PWM0 Control (Gen0)
#define PWM_1_CTL_R             0x40028040 // PWM1 Control (Gen0)
#define PWM_2_CTL_R             0x40028080 // PWM2 Control (Gen1)
#define PWM_CTL_M				0x0007ffff // PWM Control Mask
#define PWM_CTL_ENABLE    	    0x00000001 // PWM Block Enable

#define PWM_0_LOAD_R         	0x40028050 // PWM0 Load (Gen0)
#define PWM_1_LOAD_R        	0x40028050 // PWM1 Load (Gen0)
#define PWM_2_LOAD_R       	    0x40028090 // PWM2 Load (Gen1)
#define PWM_LOAD_M				0x0000ffff // PWM Load Mask

#define PWM_0_CMP_R         	0x40028058 // PWM0 Compare (Gen0A)
#define PWM_1_CMP_R           	0x4002805c // PWM1 Compare (Gen0B)
#define PWM_2_CMP_R         	0x40028098 // PWM2 Compare (Gen1A)
#define PWM_CMP_M				0x0000ffff // PWM Compare Mask

#define PWM_0_GEN_R           	0x40028060 // PWM0 Generator Control (Gen0A)
#define PWM_1_GEN_R           	0x40028064 // PWM1 Generator Control (Gen0B)
#define PWM_2_GEN_R           	0x400280a0 // PWM2 Generator Control (Gen1A)

#define PWM_GEN_M				0x000007ff // PWM Generator Mask
#define PWM_GEN_ACTCMPAD_ONE 	0x000000C0 // Action for Comparator A Down = Drive pwm High
#define PWM_GEN_ACTCMPBD_ONE	0x00000C00 // Action for Comparator B Down = Drive pwm High
#define PWM_GEN_ACTLOAD_ZERO	0x00000008 // Action for Counter=LOAD = Drive pwm Low


#define PWM_CC_R               	0x40028FC8 // PWM Clock Configuration 
#define PWM_CC_USEPWM           0x00000100 // Use PWM Clock Divisor
#define PWM_CC_PWMDIV_M         0x00000007 // PWM Clock Divider Mask
#define PWM_CC_PWMDIV_2         0x00000000 // PWM Clock = SysClk/2

#define GPIO_PIN_0				0x00000001 // GPIO Pin 0
#define GPIO_PIN_1				0x00000002 // GPIO Pin 1
#define GPIO_PIN_2				0x00000004 // GPIO Pin 2
#define GPIO_PORTF_AFSEL_R      0x4005D420 // GPIO Port F Alternate Function Select 
#define GPIO_PORTF_DEN_R        0x4005D51C // GPIO Port F Digital Enable
#define GPIO_PORTF_AMSEL_R      0x4005D528 // GPIO Port F Analog Mode Select

#define GPIO_PORTF_PCTL_R       0x4005D52C // GPIO Port F Control 
#define GPIO_PCTL_PMC0_M		0x0000000F // Port Mux Control 0
#define GPIO_PCTL_PMC1_M		0x000000F0 // Port Mux Control 1
#define GPIO_PCTL_PMC2_M		0x00000F00 // Port Mux Control 2
#define GPIO_PCTL_M0PWM0		0x00000006 // Motion Control Module 0 PWM 0 
#define GPIO_PCTL_M0PWM1		0x00000060 // Motion Control Module 0 PWM 1 
#define GPIO_PCTL_M0PWM2		0x00000600 // Motion Control Module 0 PWM 2

#define SYSCTL_RCGCGPIO_R       0x400FE608 // GPIO Run Mode Clock Gating Control
#define SYSCTL_RCGCGPIO_R5      0x00000020 // GPIO Port F Run Mode Clock Gating Control
                                            
#define SYSCTL_RCGCPWM_R        0x400FE640 // PWM Run Mode Clock Gating Control
#define SYSCTL_RCGCPWM_R0       0x00000001 // PWM Module 0 Run Mode Clock Gating Control
                                            
#define SYSCTL_PRGPIO_R         0x400FEA08 // GPIO Peripheral Ready
#define SYSCTL_PRGPIO_R5        0x00000020 // GPIO Port F Peripheral Ready

#define SYSCTL_PRPWM_R          0x400FEA40 // PWM Peripheral Ready 
#define SYSCTL_PRPWM_R0         0x00000001 // PWM Module 0 Peripheral Ready

#if PWM_IN_USE == 0
	#define PWM_ENABLE_PWMXEN PWM_ENABLE_PWM0EN
	#define PWM_X_CTL_R PWM_0_CTL_R
	#define PWM_X_LOAD_R PWM_0_LOAD_R
	#define PWM_X_CMP_R PWM_0_CMP_R
	#define PWM_X_GEN_R PWM_0_GEN_R
	#define PWM_GEN_ACTCMPXD_ONE PWM_GEN_ACTCMPAD_ONE
	#define GPIO_PIN_X GPIO_PIN_0
	#define GPIO_PCTL_PMCX_M GPIO_PCTL_PMC0_M
	#define GPIO_PCTL_M0PWMX GPIO_PCTL_M0PWM0
#elif PWM_IN_USE == 1
	#define PWM_ENABLE_PWMXEN PWM_ENABLE_PWM1EN
	#define PWM_X_CTL_R PWM_1_CTL_R
	#define PWM_X_LOAD_R PWM_1_LOAD_R
	#define PWM_X_CMP_R PWM_1_CMP_R
	#define PWM_X_GEN_R PWM_1_GEN_R
	#define PWM_GEN_ACTCMPXD_ONE PWM_GEN_ACTCMPBD_ONE
	#define GPIO_PIN_X GPIO_PIN_1
	#define GPIO_PCTL_PMCX_M GPIO_PCTL_PMC1_M
	#define GPIO_PCTL_M0PWMX GPIO_PCTL_M0PWM1
#elif PWM_IN_USE == 2
	#define PWM_ENABLE_PWMXEN PWM_ENABLE_PWM2EN
	#define PWM_X_CTL_R PWM_2_CTL_R
	#define PWM_X_LOAD_R PWM_2_LOAD_R
	#define PWM_X_CMP_R PWM_2_CMP_R
	#define PWM_X_GEN_R PWM_2_GEN_R
	#define PWM_GEN_ACTCMPXD_ONE PWM_GEN_ACTCMPAD_ONE
	#define GPIO_PIN_X GPIO_PIN_2
	#define GPIO_PCTL_PMCX_M GPIO_PCTL_PMC2_M
	#define GPIO_PCTL_M0PWMX GPIO_PCTL_M0PWM2
#endif



void initPWM (void)
{
     // Habilita o modulo 0 de PWM
    HWREG(SYSCTL_RCGCPWM_R) |= SYSCTL_RCGCPWM_R0;
	
    // Habilita a porta F
    HWREG(SYSCTL_RCGCGPIO_R) |= SYSCTL_RCGCGPIO_R5;
	
	// Habilita Funcao alternativa do pino do PWM
	HWREG(GPIO_PORTF_AFSEL_R) |= (GPIO_PIN_X);
	    
	// Habilita IO digital nos pinos da UART
	HWREG(GPIO_PORTF_DEN_R) |= (GPIO_PIN_X);

	// Desabilita modo analogico
	HWREG(GPIO_PORTF_AMSEL_R) &= ~(GPIO_PIN_X);
	
	// Escolhe PWM como funcao alternativa
	HWREG(GPIO_PORTF_PCTL_R) = (HWREG(GPIO_PORTF_PCTL_R) & ~GPIO_PCTL_PMCX_M) | GPIO_PCTL_M0PWMX;

    // Define clock do PWM como sysclk / 2
    HWREG(PWM_CC_R) = (HWREG(PWM_CC_R) & ~(PWM_CC_PWMDIV_M)) | (PWM_CC_USEPWM | PWM_CC_PWMDIV_2);

    // Configura para usar o gerador 0 em modo down count com atualizacao imediata
    HWREG(PWM_X_CTL_R) = (HWREG(PWM_X_CTL_R) & ~PWM_CTL_M);
	
    // Set the individual PWM generator controls.
    // Set the signal low on load and high on count comparison
    HWREG(PWM_X_GEN_R) = (HWREG(PWM_X_GEN_R) & ~(PWM_GEN_M)) | (PWM_GEN_ACTLOAD_ZERO | PWM_GEN_ACTCMPXD_ONE);

    // Seta o periodo como PWM_PERIOD clocks do PWM
    HWREG(PWM_X_LOAD_R) = (HWREG(PWM_X_LOAD_R) & ~(PWM_LOAD_M)) | PWM_PERIOD;
    
	// Inicia o PWM com duty cycle = 50%
	sendPWM(0.5);
	
    // Habilita a saida do PWM
    HWREG(PWM_X_CTL_R) |= PWM_CTL_ENABLE;
    
    // Habilita a contagem do gerador 0
    #ifdef PWM_ENABLE
		HWREG(PWM_ENABLE_R) |= PWM_ENABLE_PWMXEN;
		#endif
}

void sendPWM (float duty_cycle)
{
	uint32_t compare_value = PWM_PERIOD*duty_cycle;
	
    // Atualiza o valor de comparacao
    HWREG(PWM_X_CMP_R) = (HWREG(PWM_X_CMP_R) & ~(PWM_CMP_M)) | compare_value;
}
