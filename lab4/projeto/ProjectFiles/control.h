#ifndef control_h
#define control_h

#include "my_types.h"
#include "cmsis_os.h"

#define UART_MSG_Q_LEN 16

// Se true, so envia o gantt quando receber o comando 'm'
#define INITIAL_MUTE_VALUE true

// Inicializa a thread que interpreta os comandos
// Retorna ponteiro para os recursos compartilhados
shared_resources_t* init_control_thread (void);

// Fun��o executada pela thread que interpreta os comandos
// arg - Ponteiro para os recursos compartilhados
void control_thread (const void * arg);

// Envia instrucoes de como usar o gerador pela UART
void send_instructions(void);

#endif //control_h
