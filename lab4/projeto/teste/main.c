/*============================================================================
 *                               Laboratório 3
 *                       Nicolas Abril e Lucca Rawlyk
 *---------------------------------------------------------------------------*
 *                    Prof. Andr� Schneider de Oliveira
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *===========================================================================*/

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "inc/hw_gpio.h"
#include "driverlib/rom.h"
#include "driverlib/debug.h"
#include "driverlib/pwm.h"
#include "buzzer.h"

void UARTSend(const uint8_t *pui8Buffer, uint32_t ui32Count)
{
    //
    // Loop while there are more characters to send.
    //
    while(ui32Count--)
    {
        // Write the next character to the UART.
        UARTCharPut(UART0_BASE, *pui8Buffer++);
    }
}

/*
int main (void)
{
	
	uint32_t g_ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                             SYSCTL_OSC_MAIN |
                                             SYSCTL_USE_PLL |
                                             SYSCTL_CFG_VCO_480), 120000000);
	// Enable the PWM0 peripheral
	SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0));
	// Wait for the PWM0 module to be ready
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF));
  // Configure PIN for use by the PWM peripheral
	GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1);
  PWMClockSet(PWM0_BASE, PWM_SYSCLK_DIV_64);
	// Configures the alternate function of a GPIO pin
	// PF1_M0PWM1 --> piezo buzzer
	GPIOPinConfigure(GPIO_PF1_M0PWM1);
  // Configures a PWM generator.
	PWMGenConfigure(PWM0_BASE, PWM_GEN_0, (PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC)); //PWM_GEN_0	
	//buzzer_per_set(0xFFFF);
	PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, false); // set output
	PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 0xffff);
	//buzzer_vol_set(0x7FFF); 
	PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 0x7FFF*PWMGenPeriodGet(PWM0_BASE, PWM_GEN_0)/0xFFFF);
	//buzzer_write(false);
	PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, false);
	PWMOutputInvert(PWM0_BASE, PWM_OUT_1_BIT, false);
	PWMGenEnable(PWM0_BASE, PWM_GEN_0);
	buzzer_per_set(0x2000);
	buzzer_vol_set(0x0100);
	buzzer_write(true);
	
	while(true)
		;
}
*/

int main (void)
{
    uint8_t uart_char;
	  uint32_t g_ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                             SYSCTL_OSC_MAIN |
                                             SYSCTL_USE_PLL |
                                             SYSCTL_CFG_VCO_480), 120000000);
    //PERIPHERAL CONFIGURATION
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0); //PWM PERIPHERAL ENABLE
		while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0))
				;
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF); //GPIO FOR PWM0
		while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF))
				;
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1); //GPIO PD0 FOR PWM0
    //SET PWM CLOCK AS SYSTEM CLOCK DIVIDED BY 64
    PWMClockSet(PWM0_BASE, PWM_SYSCLK_DIV_64);
    GPIOPinConfigure(GPIO_PF1_M0PWM1);  //PD0 AS M1PWM0
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, (PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC));
    //SET PWM GENERATOR WITH MODEOF OPERATION AS COUNTING
		
	PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, false);
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0,0xffff);  //SET PERIOD OF PWM GENERATOR
		
	PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 0x7fff*PWMGenPeriodGet(PWM0_BASE, PWM_GEN_0)/0xffff);

	PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, false);  //ENABLE BIT2 OUTPUT
	
	PWMOutputInvert(PWM0_BASE, PWM_OUT_1_BIT, false);

	PWMGenEnable(PWM0_BASE, PWM_GEN_0);  //ENABLE PWM_GEN_0 GENERATOR
	
	PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0,0x2000);  //SET PERIOD OF PWM GENERATOR
	PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 0x7fff*PWMGenPeriodGet(PWM0_BASE, PWM_GEN_0)/0xffff);
	PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, true);  //ENABLE BIT2 OUTPUT
    
		
    // ENABLE PERIPHERAL UART 0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    // ENABLE GPIO PORT A,FOR UART
    GPIOPinConfigure(GPIO_PA0_U0RX); // PA0 IS CONFIGURED TO UART RX
    GPIOPinConfigure(GPIO_PA1_U0TX); // PA1 IS CONFIGURED TO UART TX
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    // CONFIGURE UART, BAUD RATE 115200, DATA BITS 8, STOP BIT 1, PARITY NONE
    UARTConfigSetExpClk(UART0_BASE, g_ui32SysClock, 115200, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
    
		UARTSend((uint8_t *)"\033[2JEnter text: ", 16);
		
		while (1)
    {
        //UART ECHO - what is received is transmitted back //
        if (UARTCharsAvail(UART0_BASE))
        {
            uart_char = UARTCharGet(UART0_BASE);
            UARTCharPut(UART0_BASE, uart_char);
            PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, (uart_char<<4)*PWMGenPeriodGet(PWM0_BASE, PWM_GEN_0)/0xFFFF);
		}
				//PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, 0x7fff);
    }
}
