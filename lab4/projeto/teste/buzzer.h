//..............................................................................
// Piezo buzzer header for using buzzer driver functions.
//
// Copyright (c) 2017
//
// Allan Patrick de Souza - <allansouza@alunos.utfpr.edu.br>
// Guilherme Jacichen     - <jacichen@alunos.utfpr.edu.br>
// Jessica Isoton Sampaio - <jessicasampaio@alunos.utfpr.edu.br>
// Mariana Carri�o        - <mcarriao@alunos.utfpr.edu.br>
//
// All rights reserved.
// Software License Agreement
//..............................................................................

#ifndef __BUZZER_H__
#define __BUZZER_H__

#include <stdbool.h>
#include <stdint.h>


typedef enum {FREQ_0, FREQ_1, FREQ_2, FREQ_3, FREQ_4, FREQ_5, FREQ_6} 
freq_t;

extern void buzzer_init(void);
extern void buzzer_write(bool);
extern void buzzer_vol_set(uint16_t); 
extern void buzzer_per_set(uint16_t);
void buzzer_div_set(freq_t div_freq);
#endif //__BUZZER_H
