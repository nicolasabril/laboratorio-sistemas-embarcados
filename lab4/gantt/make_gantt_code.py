from datetime import datetime

f = open('capture.txt', 'r')

content = f.read()

lines = content.split('\n')

time_stamps = []
for line in lines:
    l = line.split(' ')
    if len(l) != 3:
        continue
    ts = {}
    ts['type'] = l[0]
    ts['thread'] = l[1]
    ts['hex_time'] = l[2]
    time_stamps.append(ts)

start_time_us = 0
for t in time_stamps:
    if t['thread'] == 'main' and t['type'] == 'y':
        start_time_us = int(t['hex_time'],16)
        break

for t in time_stamps:
    t['time_us'] = 100*(int(t['hex_time'],16) - start_time_us)

output = 'var tasks = \n['
divisor_seconds = 8333333
for i,t in enumerate(time_stamps):
    if t['type'] == 'y':
        j = i+1
        found = False
        while j < len(time_stamps):
            if t['thread'] == time_stamps[j]['thread']:
                found = True
                output += '\n{\n'
                output += '    "startDate": new Date("'
                output += datetime.fromtimestamp(t['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
                output += '"),\n    "endDate": new Date("'
                output += datetime.fromtimestamp(time_stamps[j]['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
                output += '"),\n    "taskName": "' + t['thread'] + '"\n},'
                break
            j += 1
        if not found:
            output += '\n{\n'
            output += '    "startDate": new Date("'
            output += datetime.fromtimestamp(t['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
            output += '"),\n    "endDate": new Date("'
            output += datetime.fromtimestamp(time_stamps[len(time_stamps)-1]['time_us']/divisor_seconds).strftime("%a %b %d %H:%M:%S EST %Y")
            output += '"),\n    "taskName": "' + t['thread'] + '"\n},'
output = output[:-1]
output += '\n];'

output_file = open('var_tasks.txt', 'w')
output_file.write(output)

f.close()
output_file.close()
